#!/bin/bash

if [ $# -eq 0 ]; then
	echo "pass the version to refresh as argument. e.g.: " $0 "0.2.5";
else
	echo yes | conan remove commonlibs/$1@jmmut/testing
	conan export ./ commonlibs/$1@jmmut/testing 
	conan install commonlibs/$1@jmmut/testing --build commonlibs
fi

