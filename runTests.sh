#!/bin/bash

# advice: run as `./runTests.sh 2> /dev/null | grep TestSuite` to view only summaries

cd bin/

TESTS="test_log
	test_math_stats
	test_utils_exception_StackTracedException
	test_utils_graph
	"

for TEST in $TESTS
do
	./$TEST 2> /dev/null | grep "TestSuite: "
done

