rm CMakeCache.txt
rm cmake_install.cmake
rm .ninja_deps
rm .ninja_log
rm conaninfo.txt
rm conanbuildinfo.cmake
rm rules.ninja
rm -rf CMakeFiles/
rm build.ninja
rm -rf bin/
rm Makefile

rm -rf lib
rm conanfile.pyc
