from conans import ConanFile, CMake
from conans.tools import os_info, SystemPackageTool

class MyProjectWithConan(ConanFile):
    name = "commonlibs"
    author = "randomize"
    version = "0.2.9"
    url = "https://bitbucket.org/mutcoll/common-libs"
    license = "https://www.gnu.org/licenses/lgpl.txt"
    description = "Small library with utilities for developing games"
    #exports = "CMakeLists.txt"
    #exports = "*"
    settings = "os", "compiler", "build_type", "arch"
    requires = "LodePNG/master@jmmut/testing", "sdl2/2.0.8@bincrafters/stable"
    generators = "cmake"
    options = {"shared": [True, False]} # Values can be True or False (number or string value is also possible)
    default_options = ("shared=False", # Default value for shared is False (static)
                       #"sdl2:alsa=False",
                       #"sdl2:jack=False",
                       #"sdl2:pulse=False",
                       #"sdl2:nas=False",
                       #"sdl2:esd=False",
                       #"sdl2:arts=False",
                       #"sdl2:x11=False",
                       #"sdl2:xcursor=False",
                       #"sdl2:xinerama=False",
                       #"sdl2:xinput=False",
                       #"sdl2:xrandr=False",
                       #"sdl2:xscrnsaver=False",
                       #"sdl2:xshape=False",
                       #"sdl2:xvm=False",
                       #"sdl2:wayland=False",
                       #"sdl2:mir=False",
                       #"sdl2:directfb=False"
                       )


    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()


    def package(self):
        self.copy("*.h", dst="include", src="src")
        if self.settings.os == "Windows":
            self.copy(pattern="lib/*.lib", dst="lib", keep_path=False)
            self.copy(pattern="lib/*.dll", dst="bin", keep_path=False)
        else:
            #self.run("cd src && files=`find . | grep \\\\\.h` && echo $files > /tmp/files.txt && cp $files ../include --parents")
            self.copy("*.a", dst="lib", src="lib")

    def package_info(self):
        if self.settings.os == "Windows":
            self.cpp_info.libs.extend(["commonlibs", "OpenGL32"])
        else:
            self.cpp_info.libs.extend(["commonlibs", "GL"])

            #print("package info: cpp_info.libs: ", self.cpp_info.libs)
            #self.cpp_info.sharedlinkflags = ["-lGL", "GL", "libGL", "libGL.so", "-llibGL.so"]  # linker flags
            #self.cpp_info.libs = ["libGL.so", "-llibGL.soo", "-lGL", "GL", "libGL"]


    def source(self):
        self.run("git init && git remote add origin https://bitbucket.org/mutcoll/common-libs && git fetch origin -t && git checkout -f release/" + self.version )

    #def system_requirements(self):
        #if os_info.is_linux:
            #pack_name = None
            #if os_info.linux_distro == "fedora" or os_info.linux_distro == "centos":
                #pack_name = "mesa-libGL-devel"
            #else:
                #pack_name = "libgl1-mesa-dev"
#
            #if pack_name:
                #installer = SystemPackageTool()
                #installer.install(pack_name) # Install the package, will update the package database if pack_name isn't already installed
