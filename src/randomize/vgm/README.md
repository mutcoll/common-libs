**Warning: this project is under construction.**

Ventana is a little event-oriented template wrapper to SDL and openGL shaders.
It is thougth to avoid reimplementing the base code in each project.

It is designed to be used from another class 'Manager', which inherits from Ventana and overrides
its event methods (e.g. Ondisplay, or OnMouseMove).


Goals:

* Start a new project directly in the new stuff.
* Avoid remembering and/or searching those functions like SDL_CreateWindow() you only use once in every project.
* Recycle a basic makefile and project structure, and resolve typical compilation issues faster.
