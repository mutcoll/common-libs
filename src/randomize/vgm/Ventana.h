/**
 * @file Ventana.h
 * @author Jose Miguel Mut, 2013-now
 *
 * Simple class to handle a window in a game loop.
 *
 * Hides most of the hardware control.
 *
 * references:
 * * (C) 2003 by Alberto García Serrano Programación de videojuegos con SDL
 * * Sam Lantinga and SDL developers team
 * * Nehe tutorials.
 ***************************************/
#ifndef _VENTANA_H_
#define _VENTANA_H_

#include "randomize/platform/port.h"
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <fstream>

#include "SDL.h"
#include "SDL_opengl.h"
#include "randomize/log/log.h"


using namespace std;
#include "randomize/utils/semaforo/Semaforo.h"

/**
 * Creates a window and runs the game loop.
 *
 * The proper way to use this is inherit it and override the events methods.
 */
class Ventana
{
	public:
		/**
		 * Basic SDL setup.
		 */
		Ventana(int w, int h, Uint32 flags);
		Ventana():Ventana(WIDTH, HEIGHT, FLAGS){}
		Ventana(int w, int h):Ventana(w, h, FLAGS){}
		Ventana(Uint32 flags):Ventana(WIDTH, HEIGHT, flags){}
		virtual ~Ventana();

		/**
		 * Iterates periodically (based on setFPS()) sending the events to
		 * its corresponding method, which should be overrided in the child class
		 */
		void mainLoop();

		/**
		 * This will try to sleep \f$ \frac{1}{fps} \, \mbox{ms} \f$ between loop iteration. note that loop contains
		 * events listeners and onDisplay and onStep methods, so setting 2 FPS will result in
		 * a laggy keyboard feel, for example.
		 * If you want to display once every 10 iterations, set high FPS and put this in your OnStep method:
		 * t++; if (t%10 == 0) semaforoDisplay.sumar();
		 */
		void setFps(int fps);
		int getFps();
		bool isPressed(char c);
		bool isPressed(SDL_Scancode);

		typedef struct{
			SDL_Scancode scancode;
			Uint32 sym;
			Uint32 num;
			Uint32 time;
			//Uint32 pressed_time;  //¿TODO?
		} Pulsado;

	protected:
		SDL_Window *window;
		SDL_GLContext context;
		int mspf;	// miliseconds per frame
		unsigned int instanteInicial;	// to compute mean fps
		unsigned int ciclos;	// number of iterations del main loop
		float dt;	// seconds to simulate in OnStep()

		//////////////// Events
		/** Your overrided method will be called only if semaforoDisplay allows it */
		virtual void onDisplay(){}
		/** Your overrided method will be called only if semaforoStep allows it */
		virtual void onStep(float dt){};
		/*TODO comment a block of functions
		 * You can override this methods to handle the events
		 */
		virtual void onKeyPressed(SDL_Event &e){}
		virtual void onPressed(const Pulsado &p){}
		virtual void onMouseButton(SDL_Event &e){}
		virtual void onMouseMotion(SDL_Event &e){}
		virtual void onResize(int w, int h){}
		virtual void onEvent(SDL_Event &e){}

		void completarCiclo();
		void salir();

		//////////////// auxiliar to opengl
		/** You can call this after Ventana constructor if you don't need a
		 * special opengl setup. You can also make your own initGL taking this as example.
		 */
		virtual void initGL();

		void perspectiveGL (GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar);

		// Auxliliares
		void SDLcin(int &entero);	// TODO reemplazar por SDL_input
		void SDLcin(char* &cadena, int longitud);


		////////////////// private attributes
	protected:
		Semaforo semaforoDisplay, semaforoStep;	// control acces to OnDisplay and OnStep
	private:
		static const int WIDTH = 640;
		static const int HEIGHT = 480;
		static const int FLAGS = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;
		char keycodePulsado[256];
		char scancodePulsado[SDL_NUM_SCANCODES];
		bool dentro;
		Uint32 tiempoAnt;

		int steps;
		int displays;
		long int sumEvent;
		long int sumStep;
		long int sumDisplay;
		long int sumDelay;
		long int sumSleeps;
};

#endif
