#ifndef _SHADER_MANAGER_H
#define _SHADER_MANAGER_H
// TODO aprender a comentar varias funciones conjuntamente en doxygen

#include <fstream>

#include "SDL.h"
#include "SDL_opengl.h"

typedef struct {
    GLhandleARB program;
    GLhandleARB vertShader;
    GLhandleARB fragShader;
    const char *vertSource;
    const char *fragSource;
} ShaderData;

class ShaderManager {
	public:
		ShaderManager(){}
		~ShaderManager(){quitShaders();}
		
		
		//////////////// shaders meta handlers
		/*
		 * handle the shaders at "high" level.
		 */
		bool initShaders();
		void quitShaders();
		static bool loadFile(const char path[], char* &program, int &length);
		bool loadShaders(const char vspath[], const char fspath[]);
		bool compileShader(GLhandleARB, const char*);
		bool compileShaderProgram(ShaderData*);
		void destroyShaderProgram(ShaderData*);
		bool selectShader(int n);
		void unselectShader();

		void setUniform(const char varName[], float varValue);

		//////////////// shaders handlers
		/*
		 * handle the shaders at low level. this methods are called from 
		 * shaders meta handlers (above "high level methods"), but if you don't like 
		 * the above functions you can call these directly.
		 */ 
		PFNGLATTACHOBJECTARBPROC glAttachObjectARB = 0;
		PFNGLCOMPILESHADERARBPROC glCompileShaderARB = 0;
		PFNGLCREATEPROGRAMOBJECTARBPROC glCreateProgramObjectARB = 0;
		PFNGLCREATESHADEROBJECTARBPROC glCreateShaderObjectARB = 0;
		PFNGLDELETEOBJECTARBPROC glDeleteObjectARB = 0;
		PFNGLGETINFOLOGARBPROC glGetInfoLogARB = 0;
		PFNGLGETOBJECTPARAMETERIVARBPROC glGetObjectParameterivARB = 0;
		PFNGLLINKPROGRAMARBPROC glLinkProgramARB = 0;
		PFNGLSHADERSOURCEARBPROC glShaderSourceARB = 0;
		PFNGLUSEPROGRAMOBJECTARBPROC glUseProgramObjectARB = 0;


		/////////////// shader variables
		/**
		 * to change the value of a shader variable, you must first get the 
		 * location, and later set it.
		 * Shaders have three types of variables: uniforms, attributes and varyings; 
		 * but you can only set uniforms and attributes from outside.
		 */
		PFNGLGETUNIFORMLOCATIONARBPROC glGetUniformLocationARB = 0;	// get uniform location

		PFNGLUNIFORM1IARBPROC glUniform1iARB = 0;	//set uniforms
		PFNGLUNIFORM1FARBPROC glUniform1fARB = 0;
		PFNGLUNIFORM2FVARBPROC glUniform2fvARB = 0;
		PFNGLUNIFORM3FVARBPROC glUniform3fvARB = 0;

		PFNGLGETATTRIBLOCATIONARBPROC glGetAttribLocationARB = 0;	// get attributes location

		PFNGLVERTEXATTRIB1FARBPROC glVertexAttrib1fARB = 0;	// set attributes
		PFNGLVERTEXATTRIB2FVARBPROC glVertexAttrib2fvARB = 0;
		PFNGLVERTEXATTRIB3FVARBPROC glVertexAttrib3fvARB = 0;
		PFNGLVERTEXATTRIB1DARBPROC glVertexAttrib1dARB = 0;
		PFNGLVERTEXATTRIB2DVARBPROC glVertexAttrib2dvARB = 0;

	protected:
		static const int NUM_SHADERS = 9;
		ShaderData shaders[NUM_SHADERS];
		int numShaders = 0;
		int currentShader = -1;
		bool shadersSupported = false;
};


#endif /* _SHADER_MANAGER_H */
