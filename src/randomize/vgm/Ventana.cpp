#include "Ventana.h"


Ventana::Ventana(int w, int h, Uint32 flags):context(NULL)
{
	mspf = 1000/60;
	dt = mspf/1000.0;
	ciclos = 0;
	steps = 0;
	displays = 0;
	sumEvent = 0;
	sumStep = 0;
	sumDisplay = 0;
	sumDelay = 0;
	sumSleeps = 0;
	memset(keycodePulsado, -1, sizeof(keycodePulsado));
	memset(scancodePulsado, -1, sizeof(scancodePulsado));

	//Initialize SDL for video output
	if (!SDL_WasInit(SDL_INIT_VIDEO)) {
		if ( SDL_Init(SDL_INIT_VIDEO) < 0 ) {
			LOG_ERROR("Unable to initialize SDL: %s\n", SDL_GetError());
			exit(1);
		}
	}

	instanteInicial = SDL_GetTicks();
	//SDL_Log("inicio: %u", instanteInicial);

	// Create a 640x480 OpenGL screen
	window = SDL_CreateWindow( "Ventana Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, w, h, flags);
	if ( !window ) {
		LOG_ERROR("Unable to create window: %s\n", SDL_GetError());
		SDL_Quit();
		exit(2);
	}
}

Ventana::~Ventana()
{
	unsigned int instanteFinal = SDL_GetTicks();
	float tiempo_ejecucion = (instanteFinal - instanteInicial)/1000.0;
	//SDL_Log("final: %u", instanteFinal);
	//SDL_Log("ciclos: %u", ciclos);
	LOG_DEBUG("num ciclos = %d\n", ciclos);	// DEPURACION
	LOG_DEBUG("num steps = %d\n", steps);	// DEPURACION
	LOG_DEBUG("num displays = %d\n", displays);	// DEPURACION
	LOG_DEBUG("sumEvent = %ld ms\n", sumEvent);	// DEPURACION
	LOG_DEBUG("sumStep = %ld ms\n", sumStep);	// DEPURACION
	LOG_DEBUG("sumDisplay = %ld ms\n", sumDisplay);	// DEPURACION
	LOG_DEBUG("sumDelay = %ld ms\n", sumDelay);	// DEPURACION
	LOG_DEBUG("sumSleeps = %ld ms\n", sumSleeps);	// DEPURACION
	LOG_INFO("execution time (s): %f, loop iterations / time: %f", tiempo_ejecucion, ciclos/tiempo_ejecucion);

	SDL_Quit();
}
/*
   int Ventana::PintarRect(Sint16 x, Sint16 y, Uint16 w, Uint16 h, Uint32 colorARGB)
   {
   static SDL_Rect r;
   r.x = x;
   r.y = y;
   r.w = w;
   r.h = h;
   return SDL_FillRect(screen, &r,colorARGB);
   }
   */


/*
 * Reescala el valor en un marco logaritmico.
 * En otras palabras devuelve lo que se veria en un plot con ejes
 * pseudologaritmicos: 2^2, 2^1, 2^0, 0, -2^0, -2^1, -2^2, etc.
 *
 int Lg (int x)
 {
 int res(0);
 bool negativo(x<0);

 if (x == 0)
 return 0;

 if (negativo)
 x *= -1;

 while (x > 0)
 {
 res ++;
 x >>= 1;
 }

 if (negativo)
 res *= -1;

 return res;
 }
 */

bool Ventana::isPressed(char c)
{
	return keycodePulsado[(unsigned int)c] != -1;
}
bool Ventana::isPressed(SDL_Scancode sc)
{
	if (sc >= 0 && sc < SDL_NUM_SCANCODES)
		return scancodePulsado[sc] != -1;
	return false;
}

void Ventana::mainLoop()
{
	SDL_Event evento;
	static const int MAX_PRESS_KEY = 20;	// number of keys pressed at the same time
	Pulsado pulsadosV[MAX_PRESS_KEY];
	int pulsadosN = 0;
	int aux;
	dentro = true;
	tiempoAnt = SDL_GetTicks();
	int startChrono = tiempoAnt;
	int stopChrono;
	while(dentro)
	{
		while ( SDL_PollEvent(&evento) )	// anadir la recogida de otros eventos
		{
			onEvent(evento);
			switch (evento.type)
			{
				case SDL_QUIT:
					dentro = false;
					break;
				case SDL_KEYUP:
					onKeyPressed(evento);
					if((aux = scancodePulsado[evento.key.keysym.scancode]) != -1){
						scancodePulsado[evento.key.keysym.scancode] = -1;
						if(pulsadosV[aux].sym <= 255)
							keycodePulsado[pulsadosV[aux].sym] = -1;
						pulsadosN--;
						if(pulsadosN > 0 && aux != pulsadosN){
							pulsadosV[aux] = pulsadosV[pulsadosN];
							if(pulsadosV[aux].sym <= 255)
								keycodePulsado[pulsadosV[aux].sym] = aux;
							scancodePulsado[pulsadosV[aux].scancode] = aux;
						}
					}
					break;
				case SDL_KEYDOWN:
					if (evento.key.keysym.sym == SDLK_ESCAPE)
						dentro = false;
					else
						onKeyPressed(evento);
					//cout << "evento.key.keysym.mod && KMOD_SHIFT = " << (evento.key.keysym.mod && KMOD_SHIFT) << endl;	// DEPURACION
					if(scancodePulsado[evento.key.keysym.scancode] == -1 && pulsadosN < MAX_PRESS_KEY){
						pulsadosV[pulsadosN].scancode = evento.key.keysym.scancode;
						//pulsadosV[pulsadosN].sym = evento.key.keysym.mod && KMOD_SHIFT ? toupper(evento.key.keysym.sym) : evento.key.keysym.sym;
						pulsadosV[pulsadosN].sym = evento.key.keysym.sym;
						pulsadosV[pulsadosN].num = 0;
						pulsadosV[pulsadosN].time = SDL_GetTicks();

						if(evento.key.keysym.sym <= 255){
							keycodePulsado[pulsadosV[pulsadosN].sym] = pulsadosN;
						}

						scancodePulsado[evento.key.keysym.scancode] = pulsadosN;

						pulsadosN++;
					}
					break;
				case SDL_WINDOWEVENT:
					switch(evento.window.event){
						case SDL_WINDOWEVENT_RESIZED:
							onResize(evento.window.data1, evento.window.data2);
							semaforoDisplay.sumar();	// omisible si esta al soltar el raton
							break;
					}
					break;
				case SDL_MOUSEBUTTONDOWN:
				case SDL_MOUSEBUTTONUP:
				case SDL_MOUSEWHEEL:
					onMouseButton(evento);
					break;
				case SDL_MOUSEMOTION:
					onMouseMotion(evento);
					break;
				default:
					break;
			}
		}
		for(int i = 0; i < pulsadosN; i++){
			onPressed(pulsadosV[i]);
			pulsadosV[i].num++;
		}
		stopChrono = SDL_GetTicks();
		sumEvent += stopChrono - startChrono;
		startChrono = stopChrono;

		if (semaforoStep.consumir()){
			steps++;
			onStep(dt);
		}
		stopChrono = SDL_GetTicks();
		sumStep += stopChrono - startChrono;
		startChrono = stopChrono;

		if (semaforoDisplay.consumir()){
			displays++;
			onDisplay();
		}
		stopChrono = SDL_GetTicks();
		sumDisplay += stopChrono - startChrono;
		startChrono = stopChrono;

		//char asdf;
		//cin >> asdf;

		completarCiclo();
	}
}

void Ventana::setFps(int fps)
{
	if (fps > 0)
		mspf = 1000/fps;
}

int Ventana::getFps()
{
	return 1000/mspf;
}

void Ventana::completarCiclo()
{
	int delay(mspf - int(SDL_GetTicks() - tiempoAnt));

	//static Uint32 inicio(SDL_GetTicks());
	//static int iters = 0;
	//iters ++;
	//if (iters%1000 == 0)
	//{
	//cout << "acumulado = " << SDL_GetTicks() - inicio << endl;	// DEPURACION
	//inicio = SDL_GetTicks();
	//}

	//cout << "mspf = " << mspf << endl;	// DEPURACION
	sumDelay += delay;	// DEPURACION
	if(delay > 0)	// positive delay is good
	{
		dt = mspf/1000.0;
		//cout << "sleep = " << delay << endl;	// DEPURACION
		sumSleeps += delay;
		SDL_Delay(delay);
	}
	else // vamos retrasados
	{
		//cout << "me faltan " << delay << " ms" << endl;
		dt = (mspf - delay)/1000.0;
	}
	//cout << "dt = " << dt << endl << endl;	// DEPURACION

	tiempoAnt = SDL_GetTicks();
	ciclos++;
}

void Ventana::salir() {
	dentro = false;
}
void Ventana::initGL(){

	GLdouble aspect;
	int width = 640, height = 480;

	LOG_INFO("Creating Ventana with built-in initGL\n");
	if (window == NULL){
		LOG_WARN("There's no window, GL will use (640, 480) in the viewport, and you must create the sdl gl context ");
	} else {
		SDL_GetWindowSize(window, &width, &height);
		if(context == NULL) {   //Don't create a new context if it already esists;
			context = SDL_GL_CreateContext(window);
		}
		if (!context) {
			LOG_ERROR("Unable to create OpenGL context: %s\n", SDL_GetError());
			LOG_INFO("Maybe you forgot to add SDL_WINDOW_OPENGL to the window flags?");
			SDL_Quit();
			exit(2);
		}
	}

	glViewport(0, 0, width, height);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);        // This Will Clear The Background Color To Black
	glClearDepth(1.0);                // Enables Clearing Of The Depth Buffer
	glDepthFunc(GL_LESS);                // The Type Of Depth Test To Do
	glEnable(GL_DEPTH_TEST);            // Enables Depth Testing
	glShadeModel(GL_SMOOTH);            // Enables Smooth Color Shading

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();                // Reset The Projection Matrix

	aspect = (GLdouble)width / height;

	perspectiveGL (45, aspect, 0.1, 100);

	//glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 0.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
}

void Ventana::SDLcin(int &entero)
{
	//recogiendoTeclado = true;

	bool dentro(true);
	SDL_Event evento;
	entero = 0;
	SDL_Keycode pulsado;

	//cout << endl;

	while(dentro)
	{
		while ( SDL_PollEvent(&evento) )	// anadir la recogida de otros eventos
		{
			if (evento.type == SDL_KEYDOWN)
			{
				pulsado = evento.key.keysym.sym;
				if (pulsado == SDLK_RETURN || pulsado == SDLK_KP_ENTER)
					dentro = false;
				else if (pulsado >= SDLK_0 && pulsado <= SDLK_9)
					entero = entero*10 + (pulsado - SDLK_0)*(entero >= 0? 1: -1);
				else if (pulsado >= SDLK_KP_1 && pulsado <= SDLK_KP_9)
					entero = entero*10 + (pulsado - SDLK_KP_1 +1)*(entero >= 0? 1: -1);
				else if(pulsado == SDLK_KP_0)	// in sdl2 the value of SDLK_KP_0 is greater than SDLK_KP_9
					entero *= 10;
				else if (pulsado == SDLK_BACKSPACE)
					entero /= 10;
				else if (pulsado == SDLK_MINUS || pulsado == SDLK_KP_MINUS)
					entero *= -1;

				cout << "\r" << "            \r" << entero << flush;
			}
		}
		completarCiclo();
	}

	cout << endl;
}


void Ventana::SDLcin(char* &cadena, int longitud)
{
	char *cad = (char*) malloc (sizeof(char)* longitud);
	int cursor = 0;
	bool dentro(true);
	SDL_Event evento;
	SDL_Keycode pulsado;
	Uint16 mod;

	cad[cursor] = 0;


	while(dentro)
	{
		while ( SDL_PollEvent(&evento) )	// anadir la recogida de otros eventos
		{
			if (evento.type == SDL_KEYDOWN)
			{
				pulsado = evento.key.keysym.sym;
				mod = evento.key.keysym.mod;
				if (pulsado == SDLK_RETURN || pulsado == SDLK_KP_ENTER)
					dentro = false;
				else if (pulsado >= SDLK_a && pulsado <= SDLK_z && cursor < longitud)
				{
					cad[cursor] = pulsado - SDLK_a + 'a';
					if (mod == KMOD_SHIFT)
						cad[cursor] += 'A' - 'a';
					cursor ++;
				}
				else if (pulsado >= SDLK_KP_0 && pulsado <= SDLK_KP_9 && cursor < longitud)
				{
					cad[cursor] = pulsado - SDLK_KP_0 + '0';
					cursor ++;
				}
				else if (pulsado == SDLK_BACKSPACE && cursor > 0)
					cursor--;
				else
					cad[cursor++] = pulsado;	// confiemos en que el resto de teclas este mas o menos mapeado en ascii

				cad[cursor] = 0;

				cout << "\r" << cad << flush;
			}
		}
		completarCiclo();
	}

	cout << endl;

	cadena = (char*)malloc(sizeof(char)* longitud);
	memcpy(cadena, cad, longitud);
}


/**
 * Replaces gluPerspective. Sets the frustum to perspective mode.
 * @param fovY Field of vision in degrees in the y direction
 * @param aspect Aspect ratio of the viewport
 * @param zNear The near clipping distance
 * @param zFar The far clipping distance
 */
void Ventana::perspectiveGL( GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar )
{
	GLdouble fW, fH;

	fH = tan( fovY / 360 * M_PI ) * zNear;
	fW = fH * aspect;

	glFrustum( -fW, fW, -fH, fH, zNear, zFar );
}




