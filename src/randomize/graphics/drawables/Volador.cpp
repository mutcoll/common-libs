
#include "Volador.h"
#ifdef ANDROID
	#include "Draw.h"
#else
	//#include "Tricicle.h"
#endif
#include <stdio.h>
#include <string.h>

//
//	rx rx rx 0
//	ry ry ry 0
//	rz rz rz 0
//	tx ty tz 1
//
int list_tricicle = 0;


Volador::Volador()
{
	memset(mat, 0, sizeof(float)*16);

	mat [0] = 1;
	mat [5] = 1;
	mat [10] = 1;
	mat [15] = 1;

	vel = 10;
	vel_giro = 0;
	giro_amortiguado_x = 0;
	giro_amortiguado_y = 0;
	giro_amortiguado_z = 0;
}


void Volador::Init()
{
	#ifdef ANDROID
	#else
	//if(list_tricicle==0)
	//{
		//list_tricicle = glGenLists(1);
		//glNewList(list_tricicle, GL_COMPILE);
			//glScalef(0.1,0.1,0.1);
			//glRotatef(-90,0,1,0);
			//glTranslatef(1,0,0);
			//drawTricicle();
		//glEndList();
	//}
	#endif
}

Vector3D Volador::getPos()
{
	return Vector3D(mat[12],mat[13],mat[14]);
}
Vector3D Volador::getVel()
{
	return vel*Vector3D(mat[8],mat[9],mat[10]);	
	
}

Vector3D Volador::getUp()
{
	return Vector3D(mat[4],mat[5],mat[6]);
}
Vector3D Volador::getDir()
{
	return Vector3D(mat[8],mat[9],mat[10]);	
}
Vector3D Volador::getX()
{
	return Vector3D(mat[0],mat[1],mat[2]);
}

void Volador::setPos(Vector3D v)
{

	mat[12] = v[0];
	mat[13] = v[1];
	mat[14] = v[2];

}
void Volador::setVel(float v)
{
	vel = v;
}

void Volador::setOrientacion(Vector3D Z, Vector3D UP)
{
	Vector3D x = UP^Z;
	
	Vector3D up = (Z^x).Unit();
	
	x = x.Unit();
	Vector3D z = Z.Unit();
	
	for(int i = 0; i < 3; i++)
	{
		mat[i] = x[i];
		mat[i+4] = up[i];
		mat[i+8] = z[i];
	}
	
}

void Volador::Avanza(float dt)
{
	setPos(getPos()+getVel()*dt);
	if(vel_giro)
	{
		giro_amortiguado_x-=giro_amortiguado_x*dt*vel_giro;
		giro_amortiguado_y-=giro_amortiguado_y*dt*vel_giro;
		giro_amortiguado_z-=giro_amortiguado_z*dt*vel_giro;
	}
}

void Volador::Look()
{
	Vector3D pos = getPos();

	float aux_mat[16];
	memset(aux_mat,0,sizeof(float)*16);
	//------------------
	aux_mat[0] = -mat[0];
	aux_mat[4] = -mat[1];
	aux_mat[8] = -mat[2];
	//aux_mat[12] = 0.0;
	//------------------
	aux_mat[1] = mat[4];
	aux_mat[5] = mat[5];
	aux_mat[9] = mat[6];
	//aux_mat[13] = 0.0;
	//------------------
	aux_mat[2] = -mat[8];
	aux_mat[6] = -mat[9];
	aux_mat[10] = -mat[10];
	//aux_mat[14] = 0.0;
	//------------------
	//aux_mat[3] = aux_mat[7] = aux_mat[11] = 0.0;
	aux_mat[15] = 1.0;
	//------------------
	
	glRotatef(giro_amortiguado_y,0,1,0);
	glRotatef(giro_amortiguado_x,1,0,0);
	glRotatef(giro_amortiguado_z,0,0,1);
	glMultMatrixf(aux_mat);
	glTranslatef(-pos[0],-pos[1],-pos[2]);

}

void Volador::rotY(float c)
{
	glPushMatrix();
	
	glLoadMatrixf(mat);
	glRotatef(c,0,1,0);
	glGetFloatv(GL_MODELVIEW_MATRIX,mat);
	
	glPopMatrix();
}

void Volador::rotatef(float ang, float x, float y, float z)
{
	glPushMatrix();
	
	glLoadMatrixf(mat);
	glRotatef(ang, x, y, z);
	glGetFloatv(GL_MODELVIEW_MATRIX,mat);
	
	glPopMatrix();
}

void Volador::externalRotatef(float ang, float x, float y, float z)
{
	glPushMatrix();

    glLoadIdentity();
    glRotatef(ang, x, y, z);
	glMultMatrixf(mat);
	glGetFloatv(GL_MODELVIEW_MATRIX,mat);

	glPopMatrix();
}

void Volador::Draw()
{
	
	glPushMatrix();	
		glMultMatrixf(mat);
		#ifdef ANDROID
			DrawNave();
		#else
			//glCallList(list_tricicle);
		#endif
	glPopMatrix();
}

void Volador::Ejes()
{
	Ejes(100);
}

void Volador::Ejes(float n)
{
	#ifndef ANDROID
	glPushMatrix();	
		glMultMatrixf(mat);
		glBegin(GL_LINES);
		glColor4f(1,0,0,1);
		glVertex3f(-n,0,0);
		glVertex3f(n,0,0);
		
		glVertex3f(0,0,-n);
		glVertex3f(0,0,n);
		
		glVertex3f(0,-n,0);
		glVertex3f(0,n,0);
		
		glEnd();
	glPopMatrix();
	#endif
}

void Volador::rotateAround(Vector3D center, float angle, float axisX, float axisY, float axisZ) {
	Vector3D vc = center - getPos();
	rotatef(angle, axisX, axisY, axisZ);
	setPos(center-vc.Modulo()*getDir());
}

void Volador::externalRotateAround(Vector3D center, float angle, float axisX, float axisY, float axisZ) {
	Vector3D vc = center - getPos();
	externalRotatef(angle, axisX, axisY, axisZ);
	setPos(center-vc.Modulo()*getDir());
}
