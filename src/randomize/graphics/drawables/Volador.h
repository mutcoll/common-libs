#ifndef _VOLADOR_H_
#define _VOLADOR_H_

#include "randomize/math/Vector3D.h"

#ifdef ANDROID
	#include <GLES/gl.h>
#else
	#include "SDL_opengl.h"
#endif




class Volador
{
	protected:
		GLfloat mat[16];
		float vel;
	
		float giro_amortiguado_y;
		float giro_amortiguado_x;
		float giro_amortiguado_z;
		int vel_giro;
	public:
		Volador ();
		
		static void Init();
		
		Vector3D getPos();
		Vector3D getVel();
		
		Vector3D getUp();
		Vector3D getDir();
		Vector3D getX();
		
		float getVelMod(){return vel;}

		void setVelGiro(int v){vel_giro=v;}

		void Avanza(float dt);
		
		void setPos(Vector3D);
		void setVel(float);

		void setOrientacion(Vector3D z, Vector3D up);
		void Draw();
		void Ejes();
		void Ejes(float n);
		
		void Look();
		
		void AmortiguadoY(float g){giro_amortiguado_y+=g;}
		void AmortiguadoX(float g){giro_amortiguado_x+=g;}
		void AmortiguadoZ(float g){giro_amortiguado_z+=g;}
		
		void rotatef(float ang, float x, float y, float z);
		void externalRotatef(float ang, float x, float y, float z);
		void rotY(float c);
		void rotateAround(Vector3D center, float angle, float axisX, float axisY, float axisZ);
		void externalRotateAround(Vector3D center, float angle, float axisX, float axisY, float axisZ);

};

#endif //_VOLADOR_H_

