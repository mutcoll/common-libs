#ifndef _PARTICULA_H_
#define _PARTICULA_H_

#ifdef ANDROID
	#include "Draw.h"
#else

#endif
class Particula
{
	private:
		GLfloat pos[3];
		GLfloat vel[3];	
		GLfloat ace[3];
		
		
		GLfloat col[3];
		GLfloat tam;
		int forma;
	
	
	public:
	
		float fin;
		Particula(){fin = 0;}
		Particula (GLfloat* p,GLfloat* v,GLfloat* a, float f)
		{Particula (p,v,a, f,1);}
		Particula (GLfloat* p,GLfloat* v,GLfloat* a, float f, float t)
		{
			fin = f;
			int i;
			
			tam = t;

			for(i = 0; i < 3; i++)
			{
				ace[i] = a[i];
				vel[i] = v[i];
				pos[i] = p[i];
				col[i]=0;
			}
			
		}
		
		void setColor3f(GLfloat r, GLfloat g, GLfloat b)
		{
			col[0]=r;
			col[1]=g;
			col[2]=b;

		}
		
		void Mueve(float dt)
		{
			int i;
			if(fin > 0)
			{
				for(i = 0; i < 3; i++)
				{
					vel[i] += ace[i]*dt;
					pos[i] += vel[i]*dt;
				}
				fin -= dt;
//				if(pos[1]<0 && ace[1]<0)fin = 0;
//				if(pos[1]>0 && ace[1]>0)fin = 0;
				
//				tam+=dt*2;
			}
		}

		void Draw()
		{
			if(fin > 0)
			{
				glPushMatrix();
					glTranslatef(pos[0],pos[1],pos[2]);
//					glScalef(tam,tam,tam);
					glColor4f(col[0],col[1],col[2],1);
					#ifdef ANDROID
						glScalef(tam*10,tam*10,tam*10);
						DrawBola(0.5,col[0],col[1],col[2]);
					#else
						glutWireCube(tam);
					#endif
				glPopMatrix();
			}
		}

};

#endif

