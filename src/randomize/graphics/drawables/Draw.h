////////////////////////////////////////////////////////
//
//	Librería de funciones auxuliares en GLES
//
//
//
////////////////////////////////////////////////////////


#ifndef _DRAW_H_
#define _DRAW_H_

#include <EGL/egl.h>
#include <GLES/gl.h>



void gluPerspective(GLfloat fovy, GLfloat aspect,
                           GLfloat zNear, GLfloat zFar);
          
void glOrtho(float left, float right,float bottom, float top,float near, float far);

        
//Dibuja una especie de bola resultona.
void DrawBola(float d,float r, float g, float b);
void DrawBola(float d);
void DrawBola();
void DrawCube();

void DrawNave();
void DrawNave(float r, float g, float b);

void NaveMismoColor();

void DrawArbol();
#endif


