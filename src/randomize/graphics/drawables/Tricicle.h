
float angulo_manillar = 0, angulo_ruedas = 0, ejes = 0;

void drawCilindro(int caras, float tam)
{
	float w = 6.28/caras;
	float aux, col, c, s, c_, s_;
	tam = tam/2;
	
	glColor3f(1,1,1);

	c_ = 1;
	s_ = 0;
	
		glBegin(GL_QUAD_STRIP);
	for(int i = 1; i < caras; i++)
	{
		aux = w*i;
		col = 2.0*i/caras - 1;
		if(col < 0)
			col = -col;
		
		c = cos(aux);
		s = sin(aux);	
		
//		glBegin(GL_TRIANGLES);
//		glVertex3f(0,tam,0);
//		glVertex3f(c_, tam, s_);
//		glVertex3f(c, tam, s);
//		
//		glVertex3f(0,-tam,0);
//		glVertex3f(c_, -tam, s_);
//		glVertex3f(c, -tam, s);
//		glEnd();
		
		
		glVertex3f(c_, tam, s_);
		glVertex3f(c_, -tam, s_);

		glColor3f(col,col,col+0.5);
		glVertex3f(c, tam, s);
		glVertex3f(c, -tam, s);
		
		c_ = c;
		s_ = s;
	}
	c = 1; s = 0;

//		glEnd();
//	glBegin(GL_TRIANGLES);
//	glVertex3f(0,tam,0);
//	glVertex3f(c_, tam, s_);
//	glVertex3f(c, tam, s);
//	
//	glVertex3f(0,-tam,0);
//	glVertex3f(c_, -tam, s_);
//	glVertex3f(c, -tam, s);
//	glEnd();
//	
//	glBegin(GL_QUAD_STRIP);
	
	glVertex3f(c_, tam, s_);
	glVertex3f(c_, -tam, s_);

	glColor3f(1,1,1);
	glVertex3f(c, tam, s);
	glVertex3f(c, -tam, s);
	
	glEnd();
	
//		glColor3f(1,1,1);
//		glVertex3f(1, tam, 0);
//		glVertex3f(1, -tam, 0);
	
}

void drawEjes(GLfloat d)
{
	glBegin(GL_LINES);
	glColor3f(1,0,0);
	glVertex3f(0,0,0);
	glVertex3f(d,0,0);
	glColor3f(0,1,0);
	glVertex3f(0,0,0);
	glVertex3f(0,d,0);
	glColor3f(0,0,1);
	glVertex3f(0,0,0);
	glVertex3f(0,0,d);

	glEnd();
 }

void drawTricicle()
{
	#define CARAS_CILINDRO 5
	glPushMatrix();/* Triciclo */
		glColor3f(0,0,1);
		glTranslatef(0,1.7,0);
		
		glPushMatrix();/* Base */
			glTranslatef(-0.2,0,0);
			glRotatef(-90, 0,0,1);
			glScalef(0.3,1,1.6);
			drawCilindro(CARAS_CILINDRO,7.5);
		glPopMatrix();/* Base */
		glPushMatrix();/* Eje trasero */
			glTranslatef(-4,0,0);
			
			glPushMatrix();
				glRotatef(90,1,0,0);
				glScalef(0.2,1,0.2);
				drawCilindro(CARAS_CILINDRO,4);
			glPopMatrix();
			
			glColor3f(1,0,0);
			glRotatef(-angulo_ruedas,0,0,1);
			glTranslatef(0,0,1.8);
			glutSolidTorus(0.3,1.4,3,CARAS_CILINDRO);
			
			glTranslatef(0,0,-3.6);
			glutSolidTorus(0.3,1.4,3,CARAS_CILINDRO);
			
		glPopMatrix();/* Eje trasero */
		
		glColor3f(0,0,1);
		
		glPushMatrix();/* Enganche */
			glTranslatef(4,1.5,0);
			glRotatef(-45,0,0,1);
			glScalef(0.3,1,0.3);
			drawCilindro(CARAS_CILINDRO,4);
		glPopMatrix();/* Enganche */
	
		glPushMatrix(); /* Manillar */

			glTranslatef(5.5,4,0);
			glRotatef(angulo_manillar,0,1,0);
			
			if(ejes)
			{
		  		glLineWidth(3);
				drawEjes(3);	
		 		glLineWidth(1);
			}
			glPushMatrix(); 
				glScalef(0.3,1,0.3);
				drawCilindro(CARAS_CILINDRO,4);
			glPopMatrix();
			
			glPushMatrix();
				glTranslatef(0,2,0);
				glRotatef(-90, 1,0,0);
				glPushMatrix();
					glScalef(0.3,1,0.3);
					drawCilindro(CARAS_CILINDRO,4);
				glPopMatrix();
					glTranslatef(0,2,0);
					glScalef(0.4,1,0.4);
					drawCilindro(CARAS_CILINDRO,1);
					glTranslatef(0,-4,0);
					drawCilindro(CARAS_CILINDRO,1);
					
			glPopMatrix();
			
			glPushMatrix();
				glTranslatef(0,-4,0);
				glColor3f(1,0,0);
				glRotatef(-angulo_ruedas,0,0,1);
				glutSolidTorus(0.3,1.4,3,CARAS_CILINDRO);
			glPopMatrix();
			
		glPopMatrix(); /* Manillar */
	
		
	
	glPopMatrix();

}

