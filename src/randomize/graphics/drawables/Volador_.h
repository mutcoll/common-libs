#ifndef _Volador_H_
#define _Volador_H_

#include "Vector.h"
#include "string.h"

#include <GL/glut.h>    // Header File For The GLUT Library 
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#include <GL/glu.h>	// Header File For The GLu32 Library

class Volador
{
	private:
		GLfloat mat[16];
		float vel;
		static const GLfloat IDENT_MAT[16];
	
	public:
		Volador ();
		
		Vector3D getPos();
		Vector3D getVel();

		void Avanza(float dt);
		
		void setPos(Vector3D);
		void setVel(float);

		void setOrientacion(Vector3D z, Vector3D up);
		void Draw();
		
		void Look();
		
		void rotatef(float ang, float x, float y, float z);
		void rotY(float c);
		void rotX(float c) {rotatef(c, 1,0,0);}
		void rotZ(float c) {rotatef(c, 0,0,1);}
		

};

#endif

