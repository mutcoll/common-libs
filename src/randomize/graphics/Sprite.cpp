#include "Sprite.h"

Sprite::Sprite(){}

Sprite::Sprite(GLuint t, int f_w, int f_h){
	this->core.buildSpriteLegacy(t,f_w,f_h);
}
 
void Sprite::setFrame(int X, int Y){
	this->core.setFrame( X, Y);
}

void Sprite::setFrame(int N){
	this->core.advanceFrame( N);
}

void Sprite::TexCoordPointer(){
	this->core.TexCoordPointer();
}

void Sprite::BindTexture(){
	this->core.BindTexture();
}

