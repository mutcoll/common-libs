#ifndef _Window_H_
#define _Window_H_

#include <memory>
#include "SDL.h"
#include "SDL_opengl.h"
#include "randomize/log/log.h"

class Window{
public:
	Window( int width, int height, Uint32 flags): flags( flags), window( nullptr), width( width), height( height) {}
	Window(): Window( Window::DEFAULT_WIDTH, Window::DEFAULT_HEIGHT, Window::DEFAULT_FLAGS){}
	Window( int width, int height): Window( width, height, Window::DEFAULT_FLAGS){}
	Window( Uint32 flags): Window( Window::DEFAULT_WIDTH, Window::DEFAULT_HEIGHT, flags){}
	virtual ~Window(){ this->close();};
	virtual void open();
	virtual void close();
	virtual bool isOpen(){ return this->window != nullptr;};
	Uint32 getWindowID(){ return SDL_GetWindowID( this->window);};
	virtual void resize( Uint32 w, Uint32 h){ SDL_SetWindowSize( this->window, w, h);}
	void getWindowSize(int &w, int &h) { SDL_GetWindowSize(window, &w, &h); }
protected:
	Uint32 flags;
	SDL_Window *window;
	static const int DEFAULT_WIDTH = 640;
	static const int DEFAULT_HEIGHT = 480;
	static const int DEFAULT_FLAGS = SDL_WINDOW_RESIZABLE;
private:
	Uint32 width, height;	// only for initialization, use Window::getWindowSize
};

class WindowRenderer: public Window{
public:
	WindowRenderer( Uint32 flags = 0): rendererFlags( flags){}
	virtual ~WindowRenderer(){ this->close();};
	virtual void open(){ this->Window::open(); this->renderer = SDL_CreateRenderer( this->window, -1, this->rendererFlags);};
	virtual void close(){ if( this->renderer != nullptr){ SDL_DestroyRenderer( this->renderer);}; this->Window::close();};
	SDL_Texture *createSDLTexture( Uint32 w, Uint32 h, Uint32 access = SDL_TEXTUREACCESS_STATIC, Uint32 format = SDL_PIXELFORMAT_RGBA8888);
	SDL_Texture *createWindowTexture( Uint32 access = SDL_TEXTUREACCESS_STREAMING, Uint32 format = SDL_PIXELFORMAT_RGBA8888);
	void renderClear(){ SDL_RenderClear( this->renderer);};
	void renderCopy( SDL_Texture *texture, const SDL_Rect *dstrect = nullptr, const SDL_Rect *srcrect = nullptr);
	void renderPresent(){ SDL_RenderPresent( this->renderer);};
protected:
	SDL_Renderer *renderer;
	Uint32 rendererFlags;
};

class WindowGL: public Window{
public:
	WindowGL( int w, int h, Uint32 flags): Window( w, h, flags | WindowGL::DEFAULT_FLAGS), context( nullptr){};
	WindowGL( int w, int h): WindowGL( w, h, WindowGL::DEFAULT_FLAGS){};
	WindowGL( Uint32 flags = 0x0): WindowGL( Window::DEFAULT_WIDTH, Window::DEFAULT_HEIGHT, flags | WindowGL::DEFAULT_FLAGS){};
	virtual void close();
	/** You can call this after Window opening if you don't need a
	 * special opengl setup. You can also make your own initGL taking this as example.
	 */
	virtual void initGL();
	virtual void initGL(int width, int height);
	virtual void resize( Uint32 w, Uint32 h) override {
		initGL(w, h);
	}
	/**
	 * Replaces gluPerspective. Sets the frustum to perspective mode.
	 * @param fovY Field of vision in degrees in the y direction
	 * @param aspect Aspect ratio of the viewport
	 * @param zNear The near clipping distance
	 * @param zFar The far clipping distance
	 */
	void perspectiveGL (GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar);

	void makeCurrent() {
		if (this->context != nullptr) {
			SDL_GL_MakeCurrent(this->window, this->context.get()->context);
		};
		this->initGL();
	}

	void swapWindow(){ SDL_GL_SwapWindow( this->window);};
protected:
	struct WrapperSDLGLContext{
		SDL_GLContext context;
		WrapperSDLGLContext( SDL_Window *window){ this->context = SDL_GL_CreateContext( window);};
		~WrapperSDLGLContext(){ if( this->context != nullptr){ SDL_GL_DeleteContext( this->context);};};
	};
	static const int DEFAULT_FLAGS = Window::DEFAULT_FLAGS | SDL_WINDOW_OPENGL;
	std::shared_ptr< WindowGL::WrapperSDLGLContext> context;
private:
	static std::weak_ptr< WindowGL::WrapperSDLGLContext> globalContext;
};

#endif//_Window_H_
