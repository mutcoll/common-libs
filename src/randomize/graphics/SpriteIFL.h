#ifndef _SPRITE_IFL_H_
#define _SPRITE_IFL_H_

#ifdef ANDROID
	#include <GLES/gl.h>
	#include <EGL/egl.h>
#else
	#ifdef WIN32
		#define WIN32_LEAN_AND_MEAN
		#include <Windows.h>
	#endif
	#include <GL/gl.h>	// Header File For The OpenGL32 Library
#endif

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "TextureLoader.h"

class SpriteIFL{
public:
	SpriteIFL():autoAnimated(true),dt(0.0),current(0),frames_w(1){}	// frames_w used by buildSpriteLegacy and setFrame( X,Y); only
	/**
	 * Load a Sprite from a .ilf (Image List File) file, which is a text file with the next syntax
	 *	<Image File 1> <Time of 1º frame>
	 *	<Image File 2> <Time of 2º frame>
	 *	<Image File 3> <Time of 3º frame>
	 * etc...; The image files must be independently accesible, and the time must be expressed as a float in seconds
	 * */
	SpriteIFL &loadSpriteIFL( const char *path);
	SpriteIFL &loadSpriteIFL( std::istream &file);
	/**
	 * Add manually a frame to the end of the sprite from a image file .png
	 * */
	SpriteIFL &addFrame( const char *path, GLfloat time, GLboolean b = true);
	/**
	 * Add manually a frame to the end of the sprite from a texture
	 * */
	SpriteIFL &addFrame( GLuint text, GLuint w, GLuint h, GLfloat time, GLboolean b = true);
	/**
	 * Add manually a frame to the end of the sprite from a texture, with displacement
	 * */
	SpriteIFL &addFrame( GLuint text, GLint x, GLint y, GLuint w, GLuint h, GLfloat time, GLboolean b = true);
	/**
	 * Insert manually a frame on a given position in time from a texture, with displacement
	 * */
	SpriteIFL &insertFrame( GLuint text, GLint x, GLint y, GLuint w, GLuint h, GLfloat position, GLfloat duration, GLboolean b = true);
	/**
	 * Este método está aquí, para simular el constructor de la clase Sprite en Sprite.h
	 * */
	SpriteIFL &buildSpriteLegacy( GLuint text, GLuint w, GLuint h);
	/**
	 * Update the state of current, acording to the pass time expressed in seconds
	 * */
	SpriteIFL &onStep( GLfloat dt);
	/**
	 * To use only with buildSpriteLegacy. Under other circumstances the behaviour is undetermined.
	 * */
	SpriteIFL &setFrame( GLuint X, GLuint Y);
	/**
	 * Set manually the state of current
	 * */
	SpriteIFL &setFrame( GLuint n);
	/**
	 * Advance manually the state of current
	 * */
	SpriteIFL &advanceFrame( GLint n);
	/**
	 * Set the state of the autoAnimated flag, it's expected to be true in .ifl, and false on legacy code's
	 * */
	SpriteIFL &setAnimation( GLboolean b);
	/**
	 * Get the current state of the autoAnimated flag, it's expected to be true in .ifl, and false on legacy code's
	 * */
	GLboolean getAnimation();
	/**
	 * Returns the number of frames of the sprite
	 * */
	GLuint getSize();
	/**
	 * Returns the state of current
	 * */
	GLuint getCurrent();
	/**
	 * Returns the duration of the n frame
	 * */
	GLfloat getFrameDuration( GLuint n);
	/**
	 * Returns the total duration of the sprite
	 * */
	GLfloat getTotalDuration();
	/*
	 * Provides the dimensions of the current frame, in pixels on the original image, it must be used to adjust
	 * the size of the 'TexturedPanel' or Element that is going to be used to display this Sprite, in order to respect the
	 * original relation of sizes of the frames.
	 *
	 * Dario hizo cierto nivel de incapié en que esto se usara.
	 * */
	SpriteIFL &getSize( GLuint &w, GLuint &h);
	/*
	 * Provides the dimensions of the current frame, in relation to the dimensions of the first frame, it must be used to adjust
	 * the size of the 'TexturedPanel' or Element that is going to be used to display this Sprite, in order to respect the
	 * original relation of sizes of the frames.
	 *
	 * Lo mismo que arriba.
	 * */
	SpriteIFL &getSize( GLfloat &w, GLfloat &h);
	/**
	 * Provides the displacement of the current frame, in pixels.
	 * */
	SpriteIFL &getPos( GLuint &x, GLuint &y);
	/**
	 * Provides the displacement of the current frame, in relation to the width and height, respectively, of the first frame.
	 * */
	SpriteIFL &getPos( GLfloat &x, GLfloat &y);
	/**
	 * Provides current state of visibility
	 * */
	GLboolean getVisible();
	/**
	 * Set the visibility for a given frame
	 * */
	SpriteIFL &setVisible( GLuint frame, GLboolean b);
	/**
	 * Reset the animation
	 * */
	SpriteIFL &reset();
	/**
	 * Set the texture coordinades for a given Frame
	 * */
	SpriteIFL &setTextureCoord( GLuint frame, GLfloat rect[4][2]);
	/**
	 * Equivale a glTexCoordPointer
	 * 
	 * Requiere haber habilitado el ClientState con:
	 * 
	 * 		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	 * */
	SpriteIFL &TexCoordPointer();
	/**
	 * Selecciona la textura como actual.
	 * */
	SpriteIFL &BindTexture();
	/**
	 * Save the sprite information in .ifl file
	 * */
	SpriteIFL &saveToIFL( const char *path = "");
	/**
	 * Save the sprite information in an output stream
	 * */
	SpriteIFL &saveToIFL( std::ostream &file);
	/**
	 * Set the fileName
	 * */
	SpriteIFL &setFileName( const char *path);
	/**
	 * Get the fileName
	 * */
	const std::string &getFileName();
	/**
	 * Struct to contain the informacion about a frame
	 * */
	struct Frame{
		GLboolean visible;
		GLuint text, w, h;
		GLint x, y;
		GLfloat t, rect[4][2];
		Frame():visible(true){
			this->rect[0][0] = 0.0; this->rect[0][1] = 0.0;
			this->rect[1][0] = 0.0; this->rect[1][1] = 1.0;
			this->rect[2][0] = 1.0; this->rect[2][1] = 1.0;
			this->rect[3][0] = 1.0; this->rect[3][1] = 0.0;
		}
	};
protected:
	std::string fileName;		// Set by loadSpriteIFL, use by saveToIFL as default
	GLboolean autoAnimated;
	GLfloat dt;
	std::vector<Frame> frames;
	GLuint current, frames_w;	// frames_w used by buildSpriteLegacy and setFrame( X,Y); only
	inline const SpriteIFL::Frame &currentFrame(){return this->frames[ this->current % this->frames.size() ];}
	inline const SpriteIFL::Frame &currentFrameSpeed(){return this->frames[ this->current ];}
};

#endif//_SPRITE_IFL_H_
