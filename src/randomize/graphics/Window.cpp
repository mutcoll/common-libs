#include "Window.h"

void Window::open(){
	this->window = SDL_CreateWindow( "Demo Window", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, this->width, this->height, this->flags);
	if ( !this->window ) {
		LOG_ERROR("Unable to create window: %s\n", SDL_GetError());
		exit(1 );
	}
}

void Window::close(){
	if( this->window != nullptr){ SDL_DestroyWindow( this->window); this->window = nullptr;};
}

SDL_Texture *WindowRenderer::createSDLTexture( Uint32 w, Uint32 h, Uint32 access, Uint32 format){
	return SDL_CreateTexture( this->renderer, format, access, w, h);
}

SDL_Texture *WindowRenderer::createWindowTexture( Uint32 access, Uint32 format){
	Sint32 w, h;	SDL_GetWindowSize( this->window, &w, &h);
	return SDL_CreateTexture( this->renderer, format, access, w, h);
}

void WindowRenderer::renderCopy( SDL_Texture *texture, const SDL_Rect *dstrect, const SDL_Rect *srcrect){
	SDL_RenderCopy( this->renderer, texture, srcrect, dstrect);
};

void WindowGL::close(){
	this->Window::close();
}

void WindowGL::initGL() {
	int w, h;
	SDL_GetWindowSize(window, &w, &h);
	initGL(w, h);
}

void WindowGL::initGL(int width, int height) {
	GLdouble aspect;

	if( this->window == NULL){
		LOG_WARN("There's no window, GL will use (%d, %d) in the viewport, and you must create the sdl gl context ",
				width, height);
	} else {
		SDL_GetWindowSize( this->window, &width, &height);
		if( WindowGL::globalContext.expired()) {   //Don't create a new context if it already exists;
			this->context = std::make_shared< WindowGL::WrapperSDLGLContext>( this->window);
			WindowGL::globalContext = this->context;
		} else {
			this->context = WindowGL::globalContext.lock();
		}

		if( this->context == nullptr) {
			LOG_ERROR("Unable to create OpenGL context: %s\n", SDL_GetError());
			LOG_INFO("Maybe you forgot to add SDL_WINDOW_OPENGL to the window flags?");
			SDL_Quit();
			exit(1);
		}
	}

	glViewport(0, 0, width, height);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);        // This Will Clear The Background Color To Black
	glClearDepth(1.0);                // Enables Clearing Of The Depth Buffer
	glDepthFunc(GL_LESS);                // The Type Of Depth Test To Do
	glEnable(GL_DEPTH_TEST);            // Enables Depth Testing
	glShadeModel(GL_SMOOTH);            // Enables Smooth Color Shading

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();                // Reset The Projection Matrix

	aspect = (GLdouble) width / height;

	this->perspectiveGL (45, aspect, 0.1, 100);

	//glOrtho(-1.0, 1.0, -1.0 / aspect, 1.0 / aspect, 0.0, 1.0);

	glMatrixMode(GL_MODELVIEW);
}

void WindowGL::perspectiveGL( GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar ){
	GLdouble fW, fH;

	fH = tan( fovY / 360 * M_PI ) * zNear;
	fW = fH * aspect;

	glFrustum( -fW, fW, -fH, fH, zNear, zFar );
}

std::weak_ptr< WindowGL::WrapperSDLGLContext> WindowGL::globalContext;
