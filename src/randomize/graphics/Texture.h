
#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <stdio.h>
#include <stdlib.h> 
#include <string.h>

#ifdef ANDROID
	#include <GLES/gl.h>
#else
	//#include <GL/glut.h>    // Header File For The GLUT Library 
	#include <GL/gl.h>	// Header File For The OpenGL32 Library
	#include <GL/glu.h>	// Header File For The GLu32 Library
#endif


//// CARGA UNA TEXTURA .RGB EN OPENGL CON LOS PARAMETROS POR DEFECTO ///////////
unsigned int CargaTextura(char *Nombre);
unsigned int CargaTexturaMIPMAP(char *Nombre);

#endif


