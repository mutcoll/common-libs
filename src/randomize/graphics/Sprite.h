#ifndef _SPRITE_H_
#define _SPRITE_H_

/* Está es una versión implementada usando por debajo la nueva SpriteILF, pero por
	encima funciona igual que funcionaba antes, así que no debería afectar a códigos antiguos.
*/

#include "SpriteIFL.h"

class Sprite //Las coordenadas del sprite estan en sentido horario
{	
	public:
		Sprite();
		Sprite(GLuint tex, int f_w, int f_h);
	
		/**
		 * Establece el Frame actual
		 * */
		void setFrame(int X, int Y);
		
		/**
		 * Incrementa el frame actual en N posiciones
		 * */
		 void setFrame(int N);
		
		/**
		 * Equivale a glTexCoordPointer
		 * 
		 * Requiere haber habilitado el ClientState con:
		 * 
		 * 		glEnableClientState(GL_TEXTURE_COORD_ARRAY);
		 * */
		void TexCoordPointer();
		/**
		 * Selecciona la textura como actual.
		 * */
		void BindTexture();
		


	protected:
		
		SpriteIFL core;
	
};



#endif //_SPRITE_H_
