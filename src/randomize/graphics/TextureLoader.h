/**
 * @file TextureLoader.h
 * @author jmmut 
 * @date 2015-09-04.
 */

#ifndef PME_TEXTURELOADER_H
#define PME_TEXTURELOADER_H

#include <vector>
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#endif
#include <GL/gl.h>

#include "lodepng.h"
#include "randomize/log/log.h"

unsigned int LoadImg(std::vector<unsigned char> image, unsigned width, unsigned height);
unsigned int LoadPNG(const char* filename, unsigned &width, unsigned &height);
unsigned int LoadPNG(const char* filename);


//
//class TextureLoader {
//
//};


#endif //PME_TEXTURELOADER_H
