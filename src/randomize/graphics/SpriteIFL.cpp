#include "SpriteIFL.h"
#include "SpriteChar.h"

GLuint SpriteChar::tex_char = -1;
GLuint SpriteChar::W = 32;
GLuint SpriteChar::H = 4;

SpriteIFL &SpriteIFL::loadSpriteIFL( const char *path){
	std::string str;
	std::ifstream file( path);
	if( !file.is_open())
		throw std::string("Randomize_graphics/SpriteIFL::loadSpriteIFL: fail, opening file: ") + path;
	this->loadSpriteIFL( file);
	file.close();
	this->fileName = path;
	this->current = 0;
	return *this;
}

SpriteIFL &SpriteIFL::loadSpriteIFL( std::istream &file){
	GLuint w,h;
	GLint x,y, t;
	GLfloat f;
	std::string str;
	file >> str >> t;
	if( str != "Randomize_graphics/SpriteIFL__1.0__")
		throw std::string("Randomize_graphics/SpriteIFL::loadSpriteIFL: fail, file format not supported.");
	this->frames.reserve( t);
	if( t-- > 0) file >> str;
	while( t >= 0 && !file.eof()){
		if( str == "INVISIBLE_FRAME"){
			file >> x >> y >> w >> h >> f;
			this->addFrame( 0, x, y, w, h, f);
			this->frames.back().visible = false;
		}else if( str != ""){
			file >> f;
			this->addFrame( str.c_str(), f);
		}else{ t++;}
		if( t-- > 0) file >> str;
	}
	return *this;
}

SpriteIFL &SpriteIFL::addFrame( const char *path, GLfloat time, GLboolean b){
	SpriteIFL::Frame tmp;
	tmp.text = LoadPNG( path, tmp.w, tmp.h);
	tmp.x = 0;	tmp.y = 0;	tmp.t = time;	tmp.visible = b;
	this->frames.push_back( tmp);
	return *this;
}

SpriteIFL &SpriteIFL::addFrame( GLuint text, GLuint w, GLuint h, GLfloat time, GLboolean b){
	SpriteIFL::Frame tmp;	tmp.text = text;
	tmp.w = w;	tmp.h = h;	tmp.t = time;	tmp.visible = b;
	this->frames.push_back( tmp);
	return *this;
}

SpriteIFL &SpriteIFL::addFrame( GLuint text, GLint x, GLint y, GLuint w, GLuint h, GLfloat time, GLboolean b){
	SpriteIFL::Frame tmp;	tmp.text = text;
	tmp.x = x;	tmp.y = y;	tmp.w = w;	tmp.h = h;	tmp.t = time;	tmp.visible = b;
	this->frames.push_back( tmp);
	return *this;
}

SpriteIFL &SpriteIFL::insertFrame( GLuint text, GLint x, GLint y, GLuint w, GLuint h, GLfloat position, GLfloat duration, GLboolean b){
	const GLfloat precision = 0.000001;
	std::vector<SpriteIFL::Frame>::iterator it;
	SpriteIFL::Frame tmp, tmp2;
	tmp.text = text;	tmp.x = x;	tmp.y = y;	tmp.w = w;	tmp.h = h;	tmp.t = duration;	tmp.visible = b;
	if( position >= this->getTotalDuration())
		this->frames.push_back( tmp);
	else{
		for( it = this->frames.begin(); position > precision; it++) position -= it->t;
		if( position < -precision){	it--;
			tmp2.text = it->text; tmp2.x = it->x; tmp2.y = it->y;
			tmp2.w = it->w; tmp2.h = it->h; tmp2.t = -position; tmp2.visible = it->visible;
			it->t += position;	it++;
			this->frames.insert( it, tmp2);
		}else
			while( it->t == 0.0) it++;
		if( duration + precision >= it->t){
			it->text = tmp.text;	it->x = tmp.x;	it->y = tmp.y;	it->w = tmp.w;	it->h = tmp.h;	it->visible = tmp.visible;
		}else{
			it->t -= duration;
			this->frames.insert( it, tmp);
		}
	}
	return *this;
}

SpriteIFL &SpriteIFL::buildSpriteLegacy( GLuint text, GLuint w, GLuint h){
	unsigned int i,j;
	GLfloat tmp[4][2];
	for(j=0; j<h; j++){
		tmp[0][1] = tmp[3][1] = j / (float)h;
		tmp[1][1] = tmp[2][1] = (j+1) / (float)h;
		for(i=0; i<w; i++){
			tmp[0][0] = tmp[1][0] = i / (float)w;
			tmp[3][0] = tmp[2][0] = (i+1) / (float)w;
			this->addFrame( text, 0, 0, 1, 1, 1);
			this->setTextureCoord( this->frames.size()-1, tmp);
		}
	}
	this->frames_w = w;
	this->autoAnimated = false;
	return *this;
}

SpriteIFL &SpriteIFL::onStep( GLfloat dt){
	if( this->autoAnimated){
		this->dt += dt;
		while( this->dt >= this->currentFrame().t){
			this->dt -= this->currentFrame().t;
			this->current++;
			if( this->current >= this->frames.size())
				this->current = 0;
		}
	}
	return *this;
}

SpriteIFL &SpriteIFL::setFrame( GLuint x, GLuint y){
	this->setFrame( (x%this->frames_w) + this->frames_w * y);
	return *this;
}

SpriteIFL &SpriteIFL::setFrame( GLuint n){
	this->current = n % this->frames.size();
	return *this;
}

SpriteIFL &SpriteIFL::advanceFrame( GLint n){
	GLuint size = this->frames.size();
	while( n < 0) n += size;		// if( n < 0) n = tmp - (-n) % tmp;
	this->current = (this->current + n) % size;
	return *this;
}

SpriteIFL &SpriteIFL::setAnimation( GLboolean b){
	this->autoAnimated = b;
	return *this;
}

GLboolean SpriteIFL::getAnimation(){
	return this->autoAnimated;
}

GLuint SpriteIFL::getSize(){
	return this->frames.size();
}

GLuint SpriteIFL::getCurrent(){
	return this->current % this->frames.size();
}

GLfloat SpriteIFL::getFrameDuration( GLuint n){
	return this->frames[ n % this->frames.size() ].t;
}

GLfloat SpriteIFL::getTotalDuration(){
	GLfloat r = 0.0;
	GLuint i, size = this->frames.size();
	for( i = 0; i < size; i++)
		r += this->frames[i].t;
	return r;
}

SpriteIFL &SpriteIFL::getSize( GLuint &w, GLuint &h){
	w = this->currentFrame().w;
	h = this->currentFrame().h;
	return *this;
}

SpriteIFL &SpriteIFL::getSize( GLfloat &w, GLfloat &h){
	w = this->currentFrame().w / float (this->frames.front().w);
	h = this->currentFrame().h / float (this->frames.front().h);
	return *this;
}

SpriteIFL &SpriteIFL::getPos( GLuint &x, GLuint &y){
	x = this->currentFrame().x;
	y = this->currentFrame().y;
	return *this;
}

SpriteIFL &SpriteIFL::getPos( GLfloat &x, GLfloat &y){
	x = this->currentFrame().x / float (this->frames.front().w);
	y = this->currentFrame().y / float (this->frames.front().h);
	return *this;
}

GLboolean SpriteIFL::getVisible(){
	return this->currentFrame().visible;
}

SpriteIFL &SpriteIFL::setVisible( GLuint frame, GLboolean b){
	if( frame < this->frames.size())
		this->frames[frame].visible = b;
	return *this;
}

SpriteIFL &SpriteIFL::reset(){
	this->current = 0;	this->dt = 0.0;
	return *this;
}

SpriteIFL &SpriteIFL::setTextureCoord( GLuint frame, GLfloat rect[4][2]){
	GLuint i, j;
	if( frame < this->frames.size())
		for(i=0; i<4; i++) for(j=0; j<2; j++)
			this->frames[frame].rect[i][j] = rect[i][j];
	return *this;
}

SpriteIFL &SpriteIFL::TexCoordPointer(){
	if(this->currentFrame().visible)
		glTexCoordPointer(2, GL_FLOAT, 0, this->currentFrame().rect);
	return *this;
}

SpriteIFL &SpriteIFL::BindTexture(){
	if(this->currentFrame().visible)
		glBindTexture(GL_TEXTURE_2D, this->currentFrame().text);
	return *this;
}

SpriteIFL &SpriteIFL::saveToIFL( const char *path){
	if( std::string(path) != "") this->fileName = path;
	std::ofstream file( this->fileName.c_str());
	this->saveToIFL( file);
	file.close();

	return *this;
}

SpriteIFL &SpriteIFL::saveToIFL( std::ostream &file){		// TODO .ifl doesn't support Frame::rect yet
	file << "Randomize_graphics/SpriteIFL__1.0__\n" << this->frames.size() << std::endl;
	file.precision( 12);
	for( GLuint i=0; i<this->frames.size(); i++){
//		if( this->frames[i].visible){	//TODO support to save no invisible frames
//			file << this->frames[i]./***fileName.pngOfTexture****/ << " " << this->frames[i].t << std::endl;
//		}else{
			file << "INVISIBLE_FRAME " << this->frames[i].x << " " << this->frames[i].y << " ";
			file << this->frames[i].w << " " << this->frames[i].h << " " << this->frames[i].t << std::endl;
//		}
	}
	return *this;
}

SpriteIFL &SpriteIFL::setFileName( const char *path){
	this->fileName = path;
	return *this;
}

const std::string &SpriteIFL::getFileName(){
	return this->fileName;
}


