#ifndef _SPRITE_CHAR_H_
#define _SPRITE_CHAR_H_

#ifdef ANDROID
#include <GLES/gl.h>
#include <EGL/egl.h>
#else
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#endif

#include <cstdio>
#include "SpriteIFL.h"

class SpriteChar : public SpriteIFL{
	private:
		static GLuint tex_char, W, H;
		unsigned char esp;
	public:
		static void setTexture(GLuint t){tex_char = t;}


		class Keycode{
			public:
			enum {
				TILDE_a 	=  0x80,
				TILDE_e 	=  0x81,
				TILDE_i 	=  0x82,
				TILDE_o 	=  0x83,
				TILDE_u 	=  0x84,

				TILDE_A 	=  0x85,
				TILDE_E 	=  0x86,
				TILDE_I 	=  0x87,
				TILDE_O 	=  0x88,
				TILDE_U 	=  0x89,

				TILDE	 	=  0x8A,

				ENYE_MAYUS 	=  0xA0,
				ENYE_MINUS 	=  0xA1,
				BACK 		=  0xA2,
				ENTER 		=  0xA3,
				SHIFT 		=  0xA4

			};
		};

		SpriteChar():esp(0)
		{
			this->buildSpriteLegacy(0,W,H);
		}
		void BindTexture()
		{
			glBindTexture(GL_TEXTURE_2D, tex_char);
		}
		bool setChar(unsigned char l)
		{
			int x, y;
			bool caracter_compuesto = false;
			switch(esp)
			{
				case 195:   //Código UTF8
                    switch(l){
                        case 161:x = 0;y = 2;break; //á
						case 169:x = 1;y = 2;break; //é
						case 173:x = 2;y = 2;break; //í
						case 179:x = 3;y = 2;break; //ó
						case 186:x = 4;y = 2;break; //ú
						case 129:x = 5;y = 2;break; //Á
						case 137:x = 6;y = 2;break; //É
						case 141:x = 7;y = 2;break; //Í
						case 147:x = 8;y = 2;break; //Ó
						case 154:x = 9;y = 2;break; //Ú
						case 177:x = 31;y = 1;break; //ñ
						case 145:x = 31;y = 0;break; //N
                    }
					esp = 0;
                    break;
				case '\'':
					switch(l)
					{
						case 'a':x = 0;y = 2;break;
						case 'e':x = 1;y = 2;break;
						case 'i':x = 2;y = 2;break;
						case 'o':x = 3;y = 2;break;
						case 'u':x = 4;y = 2;break;
						case 'A':x = 5;y = 2;break;
						case 'E':x = 6;y = 2;break;
						case 'I':x = 7;y = 2;break;
						case 'O':x = 8;y = 2;break;
						case 'U':x = 9;y = 2;break;
						case '<':x = 27;y = 3;break;
						default:
						case '\'':x = 27;y = 1;break;
					}
					esp = 0;
					break;
				case '~':
					if(l == 'n')
					{
						x = 31;
						y = 1;
					}
					else
					{
						x = 31;
						y = 0;
					}
					esp = 0;
					break;
				default:
					if(isupper(l))	//Mayuscula
					{
						x = l - 65;
						y = 0;
					}
					else if(islower(l)) 			//Minuscula
					{
						x = l - 97;
						y = 1;
					}
					else if(isdigit(l))
					{
						y = 3;
						x = l - 48;
					}
					else
					{
						switch(l)
						{
                            case 195:caracter_compuesto=true;esp = l;break;
							case '~':caracter_compuesto=true;esp = l;break;
							case'\'':caracter_compuesto=true;esp = l;break;
							case ' ':x = 26; y = 1;break;
							case '?':x = 28; y = 1;break;
							case '!':x = 29; y = 1;break;
							case '@':x = 30; y = 1;break;
							case '-':x = 26; y = 0;break;
							case '*':x = 27; y = 0;break;
							case '.':x = 28; y = 0;break;
							case ',':x = 29; y = 0;break;
							case '+':x = 30; y = 0;break;
							case '"':x = 27; y = 2;break;
							case '(':x = 28; y = 2;break;
							case ')':x = 29; y = 2;break;
							case ':':x = 30; y = 2;break;
							case '[':x = 28; y = 3;break;
							case ']':x = 29; y = 3;break;
							case '\n':x = 26;y = 3;break;
							case Keycode::ENYE_MINUS : x = 31; y = 1; break;
							case Keycode::ENYE_MAYUS : x = 31; y = 0; break;

							case Keycode::TILDE_a :x = 0;y = 2;break;
							case Keycode::TILDE_e :x = 1;y = 2;break;
							case Keycode::TILDE_i :x = 2;y = 2;break;
							case Keycode::TILDE_o :x = 3;y = 2;break;
							case Keycode::TILDE_u :x = 4;y = 2;break;
							case Keycode::TILDE_A :x = 5;y = 2;break;
							case Keycode::TILDE_E :x = 6;y = 2;break;
							case Keycode::TILDE_I :x = 7;y = 2;break;
							case Keycode::TILDE_O :x = 8;y = 2;break;
							case Keycode::TILDE_U :x = 9;y = 2;break;
							case Keycode::TILDE   :x = 27;y = 1;break;
							default: x = 28; y = 1;break;   //Interrogante como valor por defecto
						}
					}
					break;
			}
			if(!caracter_compuesto)
				setFrame( x, y);
			return caracter_compuesto;
		}
};


#endif //_SPRITE_CHAR_H_
