#include "Log.h"

#include <stdarg.h>

#include <stdio.h>
#include <string.h>

#define SILENT_RELEASE

char tag[30] = "My_App";

void Log::setTag(const char* t)
{
	strncpy(tag, t, 30);
}

void Log::info(const char* pMessage, ...) {

#ifdef SILENT_RELEASE
	return;
#endif
    va_list lVarArgs;
    va_start(lVarArgs, pMessage);
    vprintf(pMessage,lVarArgs);
    printf("\n");
    va_end(lVarArgs);
}

void Log::error(const char* pMessage, ...) {
    va_list lVarArgs;
    va_start(lVarArgs, pMessage);
    vprintf(pMessage,lVarArgs);
    printf("\n");
    va_end(lVarArgs);
}

void Log::warn(const char* pMessage, ...) {
    va_list lVarArgs;
    va_start(lVarArgs, pMessage);
    vprintf(pMessage,lVarArgs);
    printf("\n");
    va_end(lVarArgs);
}

void Log::debug(const char* pMessage, ...) {
#ifdef SILENT_RELEASE
	return;
#endif
    va_list lVarArgs;
    va_start(lVarArgs, pMessage);
    vprintf(pMessage,lVarArgs);
    printf("\n");
    va_end(lVarArgs);
}

