#ifndef _LOG_HPP_
#define _LOG_HPP_

class Log {
	public:
		static void setTag(const char*);
		static void error(const char* pMessage, ...);
		static void warn(const char* pMessage, ...);
		static void info(const char* pMessage, ...);
		static void debug(const char* pMessage, ...);
};


#ifndef NDEBUG
    #define packt_Log_debug(...) packt::Log::debug(__VA_ARGS__)
#else
    #define packt_Log_debug(...)
#endif

#endif
