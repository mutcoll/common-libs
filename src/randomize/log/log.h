/*
 * log.h
 * 
 * Copyright 2014 Jacobo <jacollmo@alumni.uv.es>
 * 
 * Last modification: 2014/01/11 17:32:42
 */

#ifndef __MACRO_LOG__H__
#define __MACRO_LOG__H__

#include "randomize/platform/port.h"
#include <cstdio>
#include <cstring>
#include <string>
#include <map>
#include <vector>
#include <stdexcept>


/* *****
 * State Variables & default value
 * *****/
#define LOG_TAG_LENGTH 30
namespace randomize {
namespace log {

extern int log_level;
extern int log_stderr_ok;
extern char log_tag[LOG_TAG_LENGTH];
extern FILE* log_file;


/**
 * if you add values, add them to the map !!!
 */
enum Levels {
	VERBOSE_LEVEL,
	DEBUG_LEVEL,
	INFO_LEVEL,
	WARN_LEVEL,
	ERROR_LEVEL,
	FATAL_LEVEL
};
extern std::map<enum Levels, std::string> levelNames;


/**
 * if `level_name` matches the string of a log_level, its value is returned
 * if no name matches the string, a std::invalid_argument exception is thrown
 */
int parseLogLevel(const char * level_name);

std::vector<enum Levels> getLevels();
std::vector<std::string> getNames();

};  // log
};  // randomize

#define LOG_DEFAULT_LEVEL      LOG_DEBUG_LEVEL
#define LOG_DEFAULT_VERBOSE    1
#define LOG_DEFAULT_TAG        "LOG"


/* *****
 * Level Const
 * *****/
#define LOG_VERBOSE_LEVEL 0
#define LOG_DEBUG_LEVEL 1
#define LOG_INFO_LEVEL 2
#define LOG_WARN_LEVEL 3
#define LOG_ERROR_LEVEL 4
#define LOG_FATAL_LEVEL 5



/* *****
 * Function Name Alternative
 * you can override these in your application
 * *****/
#ifdef WIN32
#define LOG_FUNCTION_NAME __FUNCSIG__ //__FUNCTION__
#else
#ifndef LOG_FUNCTION_NAME
#define LOG_FUNCTION_NAME /*__func__*/ __PRETTY_FUNCTION__ //__FUNCTION__
#endif
#endif

#ifndef LOG_FILE_STYLE
#define LOG_FILE_STYLE __FILE__
#endif


/* **********************************************
* Log macros functions
* **********************************************/

#define LOG_LEVEL(level)   randomize::log::log_level = level;
#define LOG_TAG(tag)       strncpy(randomize::log::log_tag, tag, LOG_TAG_LENGTH);
#ifndef __ANDROID_API__
	#define LOG_FILE(file)     randomize::log::log_file = file;
	#define LOG_STDOUT(v)      randomize::log::log_stderr_ok = v;
#endif

//Logs an expresion and his value
#define LOG_EXP(exp, form) LOG_EXP_T(randomize::log::log_tag, exp, form)

#define LOG_VERBOSE(msg, ...)  LOG_VERBOSE_T(randomize::log::log_tag, msg, ##__VA_ARGS__)
#define LOG_DEBUG(msg, ...)  LOG_DEBUG_T(randomize::log::log_tag, msg, ##__VA_ARGS__)
#define LOG_INFO(msg,  ...)  LOG_INFO_T(randomize::log::log_tag, msg,  ##__VA_ARGS__)
#define LOG_WARN(msg,  ...)  LOG_WARN_T(randomize::log::log_tag, msg,  ##__VA_ARGS__)
#define LOG_ERROR(msg, ...)  LOG_ERROR_T(randomize::log::log_tag, msg, ##__VA_ARGS__)
#define LOG_FATAL(msg, ...)  LOG_FATAL_T(randomize::log::log_tag, msg, ##__VA_ARGS__)

/* *****
 * Other stuff
 * *****/

#ifdef __ANDROID_API__
	#define LOG_DEBUG_M    ANDROID_LOG_VERBOSE
	#define LOG_EXP_M      ANDROID_LOG_VERBOSE
	#define LOG_DEBUG_M    ANDROID_LOG_DEBUG
	#define LOG_INFO_M     ANDROID_LOG_INFO
	#define LOG_WARN_M     ANDROID_LOG_WARN
	#define LOG_ERROR_M    ANDROID_LOG_ERROR
	#define LOG_FATAL_M    ANDROID_LOG_FATAL
#else
	#define LOG_VERBOSE_M  "VERBOSE"
	#define LOG_EXP_M      "EXP"
	#define LOG_DEBUG_M    "DEBUG"
	#define LOG_INFO_M     "INFO "
	#define LOG_WARN_M     "WARN "
	#define LOG_ERROR_M    "ERROR"
	#define LOG_FATAL_M    "FATAL"
#endif
    
#define LOG_EXP_T(tag, exp, form) { \
	if (LOG_DEBUG_LEVEL >= randomize::log::log_level) { \
		LOG_FORMAT(tag, LOG_EXP_M, #exp " --> " form, exp) \
	} \
}

#define LOG_VERBOSE_T(tag, msg, ...) { \
	if (LOG_VERBOSE_LEVEL >= randomize::log::log_level) { \
		LOG_FORMAT(tag, LOG_VERBOSE_M, msg, ##__VA_ARGS__)\
	} \
}


#define LOG_DEBUG_T(tag, msg, ...) { \
	if (LOG_DEBUG_LEVEL >= randomize::log::log_level) { \
		LOG_FORMAT(tag, LOG_DEBUG_M, msg, ##__VA_ARGS__)\
	} \
}

#define LOG_INFO_T(tag, msg, ...) { \
	if (LOG_INFO_LEVEL >= randomize::log::log_level) { \
		LOG_FORMAT(tag, LOG_INFO_M, msg, ##__VA_ARGS__)\
	} \
}

#define LOG_WARN_T(tag, msg, ...) { \
	if (LOG_WARN_LEVEL >= randomize::log::log_level) { \
		LOG_FORMAT(tag, LOG_WARN_M, msg, ##__VA_ARGS__)\
	} \
}

#define LOG_ERROR_T(tag, msg, ...) { \
	if (LOG_ERROR_LEVEL >= randomize::log::log_level) { \
		LOG_FORMAT(tag, LOG_ERROR_M, msg, ##__VA_ARGS__)\
	} \
}
#define LOG_FATAL_T(tag, msg, ...) { \
	if (LOG_FATAL_LEVEL >= randomize::log::log_level) { \
		LOG_FORMAT(tag, LOG_FATAL_M, msg, ##__VA_ARGS__)\
	} \
	exit(1);\
}


/* *****
 * Log Format type
 * *****/


#ifdef __ANDROID_API__
	#define LOG_FORMAT(tag, level, msg, ...) {\
		LOG_FORMAT_F(randomize::log::log_file, tag,level, msg, ##__VA_ARGS__) \
	}
	#define LOG_FORMAT_F(file, tag, level, msg, ...)   {\
	    __android_log_print(level, tag, msg, ##__VA_ARGS__); \
		__android_log_print(level, tag, "\n");\
	}
#else
	#define LOG_FORMAT(tag, level, msg, ...) {\
		if(randomize::log::log_stderr_ok && randomize::log::log_file != stderr) \
			LOG_FORMAT_F( stderr , tag,level, msg, ##__VA_ARGS__) \
		LOG_FORMAT_F(randomize::log::log_file, tag,level, msg, ##__VA_ARGS__) \
		fflush(randomize::log::log_file); \
	}
	#define LOG_FORMAT_F(file, tag, level, msg, ...)   {\
		fprintf(file, "%s::%s\t%s:%03i in [%s]: ",tag, level, \
		LOG_FILE_STYLE, __LINE__, LOG_FUNCTION_NAME); \
		fprintf(file, msg , ##__VA_ARGS__); \
		fprintf(file, "\n"); \
	}
#endif



#endif //__MACRO_LOG__H__
