/*
 * log.h
 * 
 * Copyright 2014 Jacobo <jacollmo@alumni.uv.es>
 * 
 * Last modification: 2014/01/11 17:32:42
 */


#include "randomize/utils/Walkers.h"
#include "log.h"

namespace  randomize {
namespace log{

int log_level = LOG_DEFAULT_LEVEL;
int log_stderr_ok = LOG_DEFAULT_VERBOSE;
char log_tag[LOG_TAG_LENGTH] = LOG_DEFAULT_TAG;
FILE* log_file = stderr;

std::map<enum Levels, std::string> levelNames = {
        {VERBOSE_LEVEL, "verbose"},
        {DEBUG_LEVEL, "debug"},
        {INFO_LEVEL,  "info"},
        {WARN_LEVEL,  "warn"},
        {ERROR_LEVEL, "error"},
        {FATAL_LEVEL, "fatal"},
};



int parseLogLevel(const char* name) {

    for (auto entry : levelNames) {
        if (strncasecmp(entry.second.c_str(), name, entry.second.size()) == 0) {
            return entry.first;
        }
    }

    // no entry matched
    std::string names = utils::container_to_string_ss(getNames());
    throw std::invalid_argument(std::string("there's no loglevel called \"") + name + "\", try " + names);
}

std::vector<enum Levels> getLevels() {
    std::vector<enum Levels> values;
    for (const auto &entry : levelNames) {
        values.push_back(entry.first);
    }
    return values;
}


std::vector<std::string> getNames() {
    std::vector<std::string> values;
    for (const auto &entry : levelNames) {
        values.push_back(entry.second);
    }
    return values;
}

};
};
