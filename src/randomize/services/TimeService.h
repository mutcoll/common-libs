#ifndef _TIMESERVICE_H_
#define _TIMESERVICE_H_

#include <time.h>


/**
 * Time Service
 * 
 * Esta clase tiene guardados dos tiempos.
 * */
class TimeService {
	public:
		TimeService();

		void reset();	//Actualiza el último tiempo, y resetea el tiempo trascurrido
		void update();	//Actualiza el último tiempo, y el tiempo transcurrido

		void setFPS(unsigned int fps);
		void sleep();	//Duerme el hilo para lograr el FPS establecido.

		double now();	//Devuelve el calor del tiempo actual de un reloj
		float elapsed();	//Devuelve el tiempo que ha pasado desde el último update
		

	private:
		float mElapsed;	//Tiempo transcurrido en segundos
		double mLastTime;	//Ultimo tiempo en segundos
		
		double mPeriod;	//Intervalo entre ciclos en segundos
		unsigned int mFPS;
};

#endif
