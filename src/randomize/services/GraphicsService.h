#ifndef _GRAPHICS_SERVICE_H_
#define _GRAPHICS_SERVICE_H_

#include "GraphicsTexture.h"

#define MAX_NUM_TEXTURAS 20


class GraphicsService
{
	protected:
		unsigned int v_tex[MAX_NUM_TEXTURAS];
		int num_tex;
		android_app* app;
		
		GLuint addTexture(char path[]);
		
		
	public:
		
		GraphicsService(android_app* a);
		
		virtual void LoadTextures() = 0;
		virtual int getTexture(int tex) = 0;
		void UnloadTextures();
	
	
};


#endif //_GRAPHICS_SERVICE_H_

