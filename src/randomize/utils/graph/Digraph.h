/**
 * @file Digraph.h
 * @author jmmut
 * @date 2016-08-27.
 */

#ifndef MCV_DIGRAPH_H
#define MCV_DIGRAPH_H


#include <memory>
#include <vector>
#include <set>

namespace randomize {
namespace utils {
namespace graph {

/**
 * @class Digraph
 *
 * This class is a simple directed graph.
 *
 * The first template parameter is the Node type, but the edges are not configurable, they are just
 * pointers.
 *
 * This graph is good for navigation, it only stores the nodes that either don't have predecessors
 * or don't have successors. In other words, only stores explicitly the roots and the leafs of the
 * graph.
 */
template<typename NODE>
class Digraph {
public:
    struct Node {
        Node(NODE node,
                const std::vector<std::shared_ptr<Node>> &predecessors,
                const std::vector<std::shared_ptr<Node>> &successors)
                : node(node), predecessors(predecessors), successors(successors) { }

        NODE node;
        std::vector<std::shared_ptr<Node>> predecessors;
        std::vector<std::shared_ptr<Node>> successors;
    };

    std::shared_ptr<Node> addNode(NODE node) {
        std::shared_ptr<Node> new_node = std::make_shared<Node>(Node(node, {}, {}));
        roots.insert(new_node);
        leafs.insert(new_node);
        return new_node;
    }

    void addConnection(std::shared_ptr<Node> source, std::shared_ptr<Node> sink) {
        source->successors.push_back(sink);
        leafs.erase(source);
        sink->predecessors.push_back(source);
        roots.erase(sink);
    }

    const std::set<std::shared_ptr<Node>> &getRoots() const {
        return roots;
    }

    const std::set<std::shared_ptr<Node>> &getLeafs() const {
        return leafs;
    }

private:
    std::set<std::shared_ptr<Node>> roots;
    std::set<std::shared_ptr<Node>> leafs;
};

};  // graph
};  // utils
};  // randomize

#endif //MCV_DIGRAPH_H
