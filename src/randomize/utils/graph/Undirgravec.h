#ifndef _UNDIRGRAVEC_H_
#define _UNDIRGRAVEC_H_

#include <iostream>
#include <vector>
#include <unordered_map>
#include <map>
#include <functional>
#include <stdexcept>      // std::out_of_range
#include "Undirgraph.h"
/*
template<typename AT>
class B {
public:
    void printT(AT at) {}
};

class A : public B<A::miint> {
public:
    typedef int miint;
};
*/
//class Undirgravec;
//struct Undirgravec::EdgeContainer;
//typedef vector<Undirgravec::EdgeContainer> Undirgravec::Neighbours;
//using Neighbours = vector<EdgeContainer>;
namespace randomize {
namespace utils {
namespace graph {
/**
* @class Undirgravec
* @brief Undirected graph template with adjacency list.
*
* This graph is good at consecutive access to edges. It is
* bad at accessing from nodes. To access to an edge you must provide the nodes
* it links.
* It stores a std::vector of NODE (template parameter), and a std::vector of
* Neighbours (edges from each node).
*
* Complexity: @n
* node insertion: same as std::vector (at the end): amortized constant O(1) @n
* node access: same as std::vector: constant O(1) @n
* edge insertion: O(neighbours(src)) to avoid duplicate nodes @n
* edge access: O(neighbours(node)) @n
* neighbours access:  constant O(1) @n
*
* If you get some error like:
*
* error: cannot bind ‘std::basic_ostream<char>’ lvalue to ‘std::basic_ostream<char>&&’
*
* notice that printGraph() assumes that template parameters NODE and EDGE are
* cout-able. If you define the "ostream operator<<" as a friend in NODE and
* EDGE everything should work.
*/
template <typename NODE, typename EDGE, typename NAME = int>
class Undirgravec : public Undirgraph<NODE, EDGE, NAME> {
public:
    /** It contans the destiny node and the value. the value is the template parameter.
    struct EdgeContainer {
        //unsigned int src; // currently, the source is the location of the list
        unsigned int dst;
        EDGE val;
    };
    */
    virtual void clear() override {
        this->nodes.clear();
        this->edges.clear();
    }

    /** @return 0: ok. */
    virtual int addNode (NODE node) override {
        this->nodes.push_back(node);
        edges.resize(this->nodes.size());
        return 0;
    };
    /** @return 0: ok. @n -1: cycles are not supported. @n -2: either one or both vertices are not valid. @n
    * -3: edge already exists, it will not be inserted. */
    virtual int addEdge(unsigned int src, unsigned int dst, EDGE edge) override {  // TODO store edges in a vector and here put its id
        if (src == dst) {
            return -1;
        }
        if (!this->isValidNode(src) || !this->isValidNode(dst)) {
            return -2;
        }

        for (EdgeContainer e: this->edges[src]) {
            if (e.dst == dst) {
                return -3;
            }
        }
        this->edges[src].push_back({dst, edge});
        this->edges[dst].push_back({src, edge});

        return 0;
    }

    /** Returns the value of the edge. The value is the template parameter.
     * @n Setting the edge after calling this method will not affect the graph. Use replaceEdge instead.
     */
    virtual bool getEdge(unsigned int srcNode, unsigned int dstNode, EDGE *&edge) override {
        if (!this->isValidNode(srcNode) || !this->isValidNode(dstNode)) {
            return false;
        }

        for (EdgeContainer &e: this->edges[srcNode]) {
            if (e.dst == dstNode) {
                edge = &e.val;
                return true;
            }
        }

        return false;
    }

    virtual unsigned int getEdgesCount() const override {
        unsigned int sum = 0;
        for (const Neighbours &ng: edges) {
            sum += ng.size();
        }
        return sum;
    }

    virtual unsigned long getNeighboursCount(unsigned int nodeNumber) const override {
        return edges[nodeNumber].size();
    };

    virtual bool replaceEdge (unsigned int src, unsigned int dst, const EDGE &edge) override {
        if (!this->isValidNode(src) || !this->isValidNode(dst)) {
            return false;
        }

        for (EdgeContainer &e: this->edges[src]) {
            if (e.dst == dst) {
                e.val = edge;
                return true;
            }
        }

        return false;
    }

    virtual void forEachEdge (std::function<void (EDGE &e)> func) override {
        for (Neighbours &ng: this->edges) {
            for (EdgeContainer &e: ng) {
                func(e.val);
            }
        }
    }

    virtual void forEachNeighbour (unsigned int nodeId, std::function<void (unsigned int dstId, EDGE &edge)> func) override {
        if (!this->isValidNode(nodeId)) {
            return;
        }
        for (EdgeContainer &e: this->edges[nodeId]) {
            func(e.dst, e.val);
        }
    }


private:
    struct EdgeContainer {unsigned int dst; EDGE val;};
    typedef std::vector<EdgeContainer> Neighbours;
    std::vector<Neighbours> edges;
};

};  // graph
};  // utils
};  // randomize

#endif // _UNDIRGRAVEC_H_

