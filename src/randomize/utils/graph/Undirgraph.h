#ifndef _UNDIRGRAPH_H_
#define _UNDIRGRAPH_H_


#include <randomize/log/log.h>

namespace randomize {
namespace utils {
namespace graph {

template <typename NODE, typename EDGE, typename NAME = int>
class Undirgraph {

public:
    Undirgraph() {
    };

    ~Undirgraph() {
        /*
        unsigned int i;
        for (i = 0; i < edges.size(); i++) {
            delete edges[i];
        }*/
        // TODO: check that edges and nodes delete themselves
    }

    /** Restarts the graph with 0 edges and 0 nodes. */
    virtual void clear() = 0;
//    {
        /*  TODO? edges[i].clear()
        unsigned int i;
        for (i = 0; i < edges.size(); i++) {
            delete edges[i];
        }
        */
//        edges.clear();
//        nodes.clear();
//    }
    /** @return 0: ok. @n -1: name already used, this node will not have name. */
    virtual int addNamedNode (NODE node, NAME name) {
        unsigned int id = nodes.size();
        addNode(node);
        if (names.find(name) != names.end()) {  // already exists
            return -1; // TODO enum to name error codes.
        }
        names[name] = id;
        return 0;
    };

    /** @return 0: ok. */
    virtual int addNode (NODE node) = 0;
//    {
//        nodes.push_back(node);
//        edges.reserve(nodes.size());
//        return 0;
//    };
//
    /** @return 0: ok. @n -10: either one or both names don't name any node. */
    int addNamedEdge(NAME src, NAME dst, EDGE edge) {
        typename std::unordered_map<NAME, unsigned int>::iterator srcIt = names.find(src);   // iterator to pair
        typename std::unordered_map<NAME, unsigned int>::iterator dstIt = names.find(dst);
        if (srcIt == names.end() || dstIt == names.end()) {
            return -10;
        }
        return addEdge(srcIt->second, dstIt->second, edge);

    }

    /** @return 0: ok. @n -1: loops are not supported. @n -2: either one or both vertices are not valid. @n
    * -3: edge already exists, it will not be inserted. */
    virtual int addEdge(unsigned int src, unsigned int dst, EDGE edge) = 0;

    bool isValidNode(unsigned int nodeNumber) const {
        try {
            nodes.at(nodeNumber);
        } catch (const std::out_of_range& oor) {
            LOG_DEBUG("node %d out of range: %s", nodeNumber, oor.what());
            return false;
        }
        return true;
    };


    /** You don't need to check the return everytime, but only if you get errors or you are not
    * sure if it will succed. */
    bool getNode(unsigned int nodeNumber, NODE &node) const {
        if (!isValidNode(nodeNumber)) {
            return false;
        }
        node = nodes[nodeNumber];
        return true;
    }
    bool getNodeByName (NAME name, NODE &node) const {
        auto id = names.find(name);
        if (id == names.end()) {
            return false;
        }
        return getNode(id->second, node);
    }

    /** Made especially to use like this: for( NODE n: g.getNodesVector()) {...}. */
    std::vector<NODE> getNodesVector() const {
        return nodes;
    }


    virtual bool getEdge(unsigned int srcNode, unsigned int dstNode, EDGE *&edge) = 0;

    /*
    bool getNeighbours (unsigned int nodeNumber, NEIGHBOURS *&neighbours) const {
        if (!isValidNode(nodeNumber)) {
            return false;
        }
        neighbours = &edges[nodeNumber];
        return true;
    }*/

    unsigned int getNodesCount() const {
        return nodes.size();
    }

    virtual unsigned int getEdgesCount() const = 0;
//    {
//        unsigned int sum = 0;
//        for (NEIGHBOURS ng: edges) {
//            sum += ng->size();
//        }
//        return sum;
//    }

    virtual unsigned long getNeighboursCount(unsigned int nodeNumber) const = 0;

    bool replaceNode(unsigned int nodeNumber, const NODE &node) {
        if (!isValidNode(nodeNumber)) {
            return false;
        }
        nodes[nodeNumber] = node;
        return true;
    }
    virtual bool replaceEdge (unsigned int src, unsigned int dst, const EDGE &edge) = 0;
    void forEachNode (std::function<void (NODE &node)> func) {
        for (NODE &n: nodes) {
            func(n);
        }
    }
    virtual void forEachEdge (std::function<void (EDGE &e)> func) = 0;
    virtual void forEachNeighbour (unsigned int nodeId, std::function<void (unsigned int dstId, EDGE &edge)> func) = 0;

    /** TODO use enable_if to avoid requiring operator<< implementation */
    void printGraph(std::ostream &o) {
        for (unsigned int i = 0; i < nodes.size(); i++) {
            o << "[" << i << "]: " << nodes[i] << std::endl;
            /*
            for (const auto &e: edges[i]) {
                o << "\t" << i << "-" << e.first << ": " << e.second << std::endl;
            }
            */
            forEachNeighbour(i, [i, &o](unsigned int j, EDGE &edge) {
                o << "\t" << i << "-" << j << ": " << edge << std::endl;
            });
        }
    };

protected:
    std::vector<NODE> nodes;
    std::unordered_map<NAME, unsigned int> names;
};

};  // graph
};  // utils
};  // randomize



#endif // _UNDIRGRAPH_H_
