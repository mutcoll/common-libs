#ifndef _UNDIRGRAMAP_H_
#define _UNDIRGRAMAP_H_

#include <iostream>
#include <vector>
#include <unordered_map>
#include <map>
#include <functional>
#include <stdexcept>      // std::out_of_range
using namespace std;

//struct Node {
//int asdf;
//float qwer;
//};

//typedef int NODE;
//typedef float EDGE_VALUE;

//struct Neighbours {vector<Edge> ng;};


/**
 * @class Undirgraph
 * @brief Undirected graph template with adjacency list.
 * 
 * This graph is good at consecutive access to nodes and its neighbours. It is
 * bad at accessing from edges. To access to an edge you must provide the nodes
 * it links.
 * It stores a std::vector of NODE (template parameter), and a std::vector of
 * Neighbours (edges from each node).
 *
 * Complexity: @n
 * node insertion: same as std::vector (at the end): amortized constant O(1) @n
 * node access: same as std::vector: constant O(1) @n
 * edge insertion: O(neighbours(src)) to avoid duplicate nodes @n
 * edge access: O(neighbours(node)) @n
 * neighbours access:  constant O(1) @n
 *
 * Future versions may change to std::unordered_map (instead of std::vector) to
 * store the neighbours, if there are quite neighbours, but the main purpose is
 * consecutive access through the neighbours of a node.
 *
 * If you get some error like:
 *
 * error: cannot bind ‘std::basic_ostream<char>’ lvalue to ‘std::basic_ostream<char>&&’
 *
 * notice that printGraph() assumes that template parameters NODE and EDGE_VALUE are
 * cout-able. If you define the "ostream operator<<" as a friend in NODE and
 * EDGE_VALUE everything should work.
 */
//typedef int NODE;
//typedef float EDGE_VALUE;
//typedef int NAME;
template <typename NODE, typename EDGE, typename NAME = int>
class Undirgramap {
    public:
        /** It contans the destiny node and the value. the value is the template parameter.
         * Neighbours is a list of Edge. */
        /*
        struct Edge {
            //unsigned int src; // currently, the source is the location of the list
            //unsigned int dst;
            EDGE_VALUE val;
        };
        */
        typedef EDGE_VALUE Edge;
        typedef unordered_map<unsigned int, Edge> Neighbours;
        //typedef vector<Edge> Neighbours;


        /** @return 0: ok. @n -1: loops are not supported. @n -2: either one or both vertices are not valid. @n
         * -3: edge already exists, it will not be inserted. */
        int addEdge(unsigned int src, unsigned int dst, EDGE_VALUE edge) {    // TODO store edges in a vector and here put its id
            if (src == dst) {
                return -1;
            }
            if (!isValidNode(src) || !isValidNode(dst)) {
                return -2;
            }

            typename std::unordered_map<unsigned int, Edge>::iterator srcIt = edges[src]->find(dst);
            typename std::unordered_map<unsigned int, Edge>::iterator dstIt = edges[dst]->find(src);
            if (srcIt != edges[src]->end() || dstIt != edges[dst]->end()) {
                return -3;
            }
            
            (*edges[src])[dst] = edge;
            (*edges[dst])[src] = edge;
            return 0;
        }




        /** Returns the value of the edge. The value is the template parameter. */
        bool getEdgeValue(unsigned int srcNode, unsigned int dstNode, EDGE_VALUE *&edge) const {
            if (!isValidNode(srcNode) || !isValidNode(dstNode)) {
                return false;
            }
            typename std::unordered_map<unsigned int, Edge>::iterator srcIt = edges[srcNode]->find(dstNode);
            if (srcIt != edges[srcNode]->end()) {
                cout << "&(srcIt->second) = " << &(srcIt->second) << endl;    // DEPURACION
                edge = &(srcIt->second);
                cout << "edge = " << edge << endl;    // DEPURACION
                return true;
            }

            return false;
        }



        bool replaceEdge (unsigned int src, unsigned int dst, const EDGE_VALUE &edge) {
            if (!isValidNode(src) || !isValidNode(dst)) {
                return false;
            }

            typename std::unordered_map<unsigned int, Edge>::iterator srcIt = edges[src]->find(dst);
            typename std::unordered_map<unsigned int, Edge>::iterator dstIt = edges[dst]->find(src);
            if (srcIt == edges[src]->end() || dstIt == edges[dst]->end()) {
                return false;
            }
            srcIt->second = edge;
            dstIt->second = edge;
            return true;
        }

        void forEachEdge (function<void (EDGE_VALUE &e)> func) {
            for (Neighbours *ng: edges) {
                for (std::pair<const unsigned int, EDGE_VALUE> &e: *ng) {
                    func(e.second);
                }
            }
        }

        void forEachNeighbour (unsigned int nodeId, std::function<void (unsigned int dstId, EDGE_VALUE &edge)> func) {
            if (!isValidNode(nodeId)) {
                return;
            }
            for (pair<const unsigned int, EDGE_VALUE> &e: *edges[nodeId]) {
                func(e.first, e.second);
                //func(nodeId, (*edges[e.first])[nodeId]);
            }
        }


    private:
    public:
        std::vector<NODE> nodes;
        std::vector<Neighbours*> edges;
        std::unordered_map<NAME, unsigned int> names;
};

#endif // _UNDIRGRAMAP_H_
