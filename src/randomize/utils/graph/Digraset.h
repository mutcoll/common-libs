/**
 * @file Digraset.h
 * @author jmmut
 * @date 2016-10-21.
 */

#ifndef COMMONLIBS_DIGRASET_H
#define COMMONLIBS_DIGRASET_H

#include <memory>
#include <vector>
#include <algorithm>
#include <sstream>
#include <set>
#include <tuple>
#include "randomize/utils/exception/StackTracedException.h"

namespace randomize {
namespace utils {
namespace graph {

/**
 * @class Digraset
 *
 * This class is a simple directed graph.
 *
 * It stores a set of nodes, so that it is easy to know if it's present in the graph or not, and
 * then query its neighbours.
 */
template<typename NODE>
class Digraset {
public:
    struct NodeWrapper {
        NodeWrapper(NODE node,
                const std::set<std::shared_ptr<NodeWrapper>> &predecessors,
                const std::set<std::shared_ptr<NodeWrapper>> &successors)
                : node(node), predecessors(predecessors), successors(successors) { }

        NODE node;
        std::set<std::shared_ptr<NodeWrapper>> predecessors;
        std::set<std::shared_ptr<NodeWrapper>> successors;
    };

    using NodeWrapperPtr = std::shared_ptr<NodeWrapper>;
    using Connection = std::pair<NodeWrapperPtr, NodeWrapperPtr>;

    static bool lessThan(NodeWrapperPtr first, NodeWrapperPtr second) {
        return first->node < second->node;
    }
    /**
     * This constructor makes that the set comparator uses the internal NODE (the template
     * parameter) instead of `std::less<shared_ptr>`, because we don't want to store a
     * `set<NODE>` nor a `set<Node>` directly.
     */
    Digraset() : nodes(lessThan) {}

    Digraset(const Digraset &other) : Digraset() {
        addGraph(other);
    }
    Digraset(Digraset &&other) : Digraset() {
        addGraph(other);
    }
    Digraset &operator=(const Digraset &other) {
        clear();
        addGraph(other);
        return *this;
    }


    /**
     * adds all the nodes and connections from other graph, without overwriting the current ones
     */
    void addGraph(const Digraset &other) {
        other.walk(
                [&](NodeWrapperPtr node) {
                    addNode(node->node);
                },
                [&](NodeWrapperPtr source, NodeWrapperPtr sink) {
                    addConnection(source->node, sink->node);
                }
        );
    }

    // ------------- node methods -------------

    bool addNode(NODE node) {
        auto itSource = findNode(node);
        if (not itSource.first) {
            nodes.insert(std::make_shared<NodeWrapper>(NodeWrapper(node, {}, {})));
            return true;
        }
        return false;
    }

    /**
     * @return pair<bool // whether the node was found
     *      shared_ptr<NodeWrapper> // the pointer to element, or nullptr if was not found
     * >
     */
    std::pair<bool, std::shared_ptr<NodeWrapper>> findNode(NODE node) const {
        auto wrapper = std::make_shared<NodeWrapper>(NodeWrapper(node, {}, {}));
        auto itSource = nodes.find(wrapper);
        if (itSource != nodes.end()) {
            return {true, *itSource};
        }
        return {false, nullptr};
    };


    /**
     * @param node
     * @return `true` if the node was removed. `false` if the node was not present.
     */
    bool removeNode(std::shared_ptr<NodeWrapper> node) {
        bool removed = false;
        for (std::shared_ptr<NodeWrapper> successor : node->successors) {
            removeConnection(node, successor);
        }
        for (std::shared_ptr<NodeWrapper> predecessor : node->predecessors) {
            removeConnection(predecessor, node);
        }
        removed |= nodes.erase(node) == 1;
        return removed;
    }


    // ------------- connection methods -------------

    bool addConnection(NODE source, NODE sink) {
        auto itSource = findNode(source);
        if (itSource.first) {
            auto itSink = findNode(sink);
            if (itSink.first) {
                addConnection(itSource.second, itSink.second);
                return true;
            }
        }
        return false;
    }

    void addConnection(std::shared_ptr<NodeWrapper> source, std::shared_ptr<NodeWrapper> sink) {
        source->successors.insert(sink);
        sink->predecessors.insert(source);
    }

    std::pair<bool, Connection> findConnection(NODE source, NODE sink) const {
        auto itSource = findNode(source);
        if (itSource.first) {
            auto itSink = findNode(sink);
            if (itSink.first) {
                return doFindConnection(itSource.second, itSink.second);
            }
        }
        return {false, {nullptr, nullptr}};
    }

    bool removeConnection(std::shared_ptr<NodeWrapper> source, std::shared_ptr<NodeWrapper> sink) {
        bool removed = false;
        auto it = std::find(source->successors.begin(), source->successors.end(), sink);
        if (it != source->successors.end()) {
            source->successors.erase(it);
            removed = true;
        }

        it = std::find(sink->predecessors.begin(), sink->predecessors.end(), source);
        if (it != sink->predecessors.end()) {
            sink->predecessors.erase(it);
            removed = true;
        }
        return removed;
    }

    typename std::set<std::shared_ptr<NodeWrapper>>::iterator begin() {
        return nodes.begin();
    }
    typename std::set<std::shared_ptr<NodeWrapper>>::iterator end() {
        return nodes.end();
    }
    typename std::set<std::shared_ptr<NodeWrapper>>::iterator begin() const {
        return nodes.cbegin();
    }
    typename std::set<std::shared_ptr<NodeWrapper>>::iterator end() const {
        return nodes.cend();
    }

    void clear() { nodes.clear(); }

    size_t size() const { return nodes.size(); }

    /**
     * returns the set difference: nodes that are in this but are not in other.
     * This method loses the relationship among the nodes!
     */
    Digraset operator-(const Digraset &other) const {
        auto thisBegin = begin();
        auto thisEnd = end();
        auto otherBegin = other.begin();
        auto otherEnd = other.end();
        Digraset result;

        while (thisBegin != thisEnd && otherBegin != otherEnd) {
            if (lessThan(*thisBegin, *otherBegin)) {
                // present in this, not in other: store
                result.addNode(thisBegin->operator*().node);
                ++thisBegin;
            } else if (lessThan(*otherBegin, *thisBegin)) {
                // present in other, not in this: ignore
                ++otherBegin;
            } else {
                // present in both this and other: ignore
                ++thisBegin;
                ++otherBegin;
            }
        }

        // we reached at least the end of other, copy the rest of this
        for (; thisBegin != thisEnd; ++thisBegin) {
            result.addNode(thisBegin->operator*().node);
        }
        return result;
    }

    /**
     * returns the set intersection: nodes that are in both this and other.
     * This method loses the relationship among the nodes!
     */
    Digraset operator^(const Digraset &other) const {
        auto thisBegin = begin();
        auto thisEnd = end();
        auto otherBegin = other.begin();
        auto otherEnd = other.end();
        Digraset result;

        while (thisBegin != thisEnd && otherBegin != otherEnd) {
            if (lessThan(*thisBegin, *otherBegin)) {
                // present in this, not in other: ignore
                ++thisBegin;
            } else if (lessThan(*otherBegin, *thisBegin)) {
                // present in other, not in this: ignore
                ++otherBegin;
            } else {
                // present in both this and other: store
                result.addNode(thisBegin->operator*().node);
                ++thisBegin;
                ++otherBegin;
            }
        }
        return result;
    }

    /**
     * returns whether this is less than other. This can be used to order digrasets.
     */
    bool operator<(const Digraset &other) const {
        bool equal;
        return compare(other, equal);
    }

    /**
     * returns whether this is less than other, and whether they are equal.
     * This can be used to order digrasets.
     */
    bool compare(const Digraset &other, bool &equal) const {
        equal = false;
        if (this->size() < other.size()) {
            return true;
        } else if (this->size() > other.size()) {
            return false;
        } else {
            // return as lesser the one with the first lexicographical node
            auto thisBegin = begin();
            auto thisEnd = end();
            auto otherBegin = other.begin();
            auto otherEnd = other.end();

            while (thisBegin != thisEnd && otherBegin != otherEnd) {
                if (lessThan(*thisBegin, *otherBegin)) {
                    // present in this, not in other: lesser
                    return true;
                } else if (lessThan(*otherBegin, *thisBegin)) {
                    // present in other, not in this: greater
                    return false;
                } else {
                    // present in both this and other: continue comparing
                    ++thisBegin;
                    ++otherBegin;
                }
            }
            equal = true;
            return false;
        }
    }

    void walkForward(std::function<void (NodeWrapperPtr)> nodeFunc,
            std::function<void (NodeWrapperPtr, NodeWrapperPtr)> connectionFunc) const {
        for (NodeWrapperPtr node : nodes) {
            nodeFunc(node);
        }
        for (NodeWrapperPtr node : nodes) {
            for (NodeWrapperPtr successor : node->successors) {
                connectionFunc(node, successor);
            }
        }
    }

    void walk(std::function<void (NodeWrapperPtr)> nodeFunc,
            std::function<void (NodeWrapperPtr, NodeWrapperPtr)> connectionFunc) const {
        for (NodeWrapperPtr node : nodes) {
            nodeFunc(node);
        }
        for (NodeWrapperPtr node : nodes) {
            for (NodeWrapperPtr successor : node->successors) {
                connectionFunc(node, successor);
            }
            for (NodeWrapperPtr predecessor : node->predecessors) {
                connectionFunc(predecessor, node);
            }
        }
    }

    void print (std::ostream &out) const {
        walkForward(
                [&](NodeWrapperPtr node) {
                    printNode(out, node);
                    out << std::endl;
                },
                [&](NodeWrapperPtr source, NodeWrapperPtr sink) {
                    printConnection(out, source, sink);
                    out << std::endl;
                }
        );
    }

    void printNode(std::ostream &out, const std::shared_ptr<NodeWrapper> node) const {
        out << "{\"" << node << "\"[label=\"" << node->node << "\"]}";
    }

    void printConnection(std::ostream &out,
            const NodeWrapperPtr source, const NodeWrapperPtr sink) const {
        printNode(out, source);
        out << " -> ";
        printNode(out, sink);
    }

private:

    std::pair<bool, Connection> doFindConnection(
            std::shared_ptr<NodeWrapper> source, std::shared_ptr<NodeWrapper> sink) const {
        auto successorIt = std::find(
                source->successors.begin(),
                source->successors.end(),
                sink);
        bool successorFound = successorIt != source->successors.end();

        bool doChecks = true;
        if (doChecks) {
            auto predecessorIt = std::find(
                    sink->predecessors.begin(),
                    sink->predecessors.end(),
                    source);
            bool predecessorFound = predecessorIt != sink->predecessors.end();

            if (predecessorFound ^ successorFound) {
                std::stringstream ss;
                ss << "inconsistency found, node " << (predecessorFound ? sink->node : source->node)
                   << " has " << (predecessorFound ? source->node : sink->node)
                   << " linked, but not the other way";
                throw randomize::utils::exception::StackTracedException(ss.str());
            }
        }

        if (successorFound) {
            return {true, {source, sink}};
        }
        return {false, {nullptr, nullptr}};
    }

    std::set<
            std::shared_ptr<NodeWrapper>,
            std::function<bool (std::shared_ptr<NodeWrapper>, std::shared_ptr<NodeWrapper>)>
    > nodes;

public:
    typedef typename decltype(nodes)::iterator iterator;
    typedef typename decltype(nodes)::value_type value_type;
};

};  // graph
};  // utils
};  // randomize

#endif //COMMONLIBS_DIGRASET_H
