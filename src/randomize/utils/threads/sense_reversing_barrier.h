/***************************************
 * Sense Reversing Barrier
 *       Josemi's local variant
 *
 * Author: Jacobo Coll Moragón
 *
 *
 * Data: 2014-01-24
 * Version: 1.0
 * Compile: -lpthread
 *
 ***************************************/

#ifndef _SENSE_REVERSING_BARRIER_H_
#define _SENSE_REVERSING_BARRIER_H_

#include <pthread.h> 

typedef struct sense_reversing_barrier {
	pthread_mutex_t lock;
	pthread_cond_t condition;
	int count;
	int num_threads;
	char release;
} sense_reversing_barrier_t;

void sense_reversing_barrier_init(sense_reversing_barrier_t *barrier, int num_threads);

void sense_reversing_barrier_wait(sense_reversing_barrier_t *barrier);


#endif /* _SENSE_REVERSING_BARRIER_H_ */
