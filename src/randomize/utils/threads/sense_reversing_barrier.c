
#include "sense_reversing_barrier.h"

void sense_reversing_barrier_init(sense_reversing_barrier_t *barrier, int num_threads){
	pthread_mutex_init(&barrier->lock, NULL);
	pthread_cond_init(&barrier->condition, NULL);
	barrier->count = 0;
	barrier->num_threads = num_threads;
	barrier->release = 0;
}

void sense_reversing_barrier_wait(sense_reversing_barrier_t *barrier){
	char local_sense = !barrier->release;
	pthread_mutex_lock(&(barrier->lock));
	barrier->count++;
	if(barrier->count == barrier->num_threads){
		barrier->count = 0;
		barrier->release = local_sense;
		pthread_cond_broadcast(&(barrier->condition));
	}
	
	while(local_sense != barrier->release)
		pthread_cond_wait(&(barrier->condition), &(barrier->lock));
	
	pthread_mutex_unlock(&(barrier->lock));
}
