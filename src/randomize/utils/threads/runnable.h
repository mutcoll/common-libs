#ifndef _RUNNABLE_H_
#define _RUNNABLE_H_

#include <thread>
/**
 * Version 1.0
 * Ultima modificación: 2013/06/09
 * Estado : Actualizado
 * */

using std::thread;

class runnable : public thread
{
	public:
	
		runnable():thread(),state(STOP){}
		
		void start()
		{
			if(state == STOP)
			{
				thread t(launch, this);
				state = RUNNING;
				swap(t);
			}
		}
		
		
		int getState(){return state ;}
		int getSelf(){return self ;}
	protected:
		pthread_t self;
	private:
		enum State{STOP = 0,RUNNING = 1} state;
		static void launch(runnable* r)
		{
			r->self = pthread_self();
			r->run();
			r->state = STOP;
		}
		virtual void run() = 0;
};

#endif //_RUNNABLE_H_
