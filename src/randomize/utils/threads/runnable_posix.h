#ifndef _RUNNABLE_POSIX_H_
#define _RUNNABLE_POSIX_H_

#include <pthread.h>
/**
 * Version 1.0
 * Ultima modificación: 2013/06/09
 * Estado : Actualizado
 * */
class runnable_posix
{
	public:
	
		runnable_posix():state(STOP){}

		void start()
		{
			if(state == STOP)
			{
				state = RUNNING;
				detached = false;
				pthread_create(&t,NULL,launch,this);
			}
		}
		
		void join()
		{
			if(state == RUNNING)
			{
				void* v;
				pthread_join(t,&v);
			}
		}
		
		void detach()
		{
			pthread_detach(t);
			detached = true;
		}
		
		int getState(){return state ; }
	private:
	
		pthread_t t;
		bool detached;
		enum State{STOP = 0,RUNNING = 1} state;
		static void* launch(void* v)
		{	
			runnable_posix* r = (runnable_posix*)v;
			r->run();
			r->state = STOP;
			if(!r->detached)
				r->detach();	//Liberación de recursos
			return NULL;
		}
		virtual void run() = 0;
};

#endif //_RUNNABLE_POSIX_H_
