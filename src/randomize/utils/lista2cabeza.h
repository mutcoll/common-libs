
#include <iostream>

using namespace std;

#ifndef _LISTA2CABEZA_H_
#define _LISTA2CABEZA_H_



//Clase LISTA (con punto de interes)
//Lista doblemente enlazada circular con nodo cabeza
////////////////////////////////////////////////////////

//Declaracion del tipo base de la lista
//Si el tipo base cambia es preciso modificar esta linea
//typedef Circulo Valor;
template <typename T>
class Lista
{
    public:
        //constructores y destructor
        Lista ();
        Lista (const Lista&);
        ~Lista();
        Lista operator= (const Lista&);

        //Operaciones con datos:
        
        //InsertarUnico(Valor): Inserta el T ordenado si no esta ya en la lista. Debe estar sobrecargado el operador igual y menor que.
        //Devuelve false si no hay memoria suficiente.
        bool InsertarUnico(T);
        //InsertarOrdenado(Valor): Inserta el T ordenadamente. Debe estar sobrecargado el operador menor que.
        //Devuelve false si no hay memoria suficinete.
        bool InsertarOrdenado(T);
        //InsertarDelante (Valor): Inserta el T delante del pto de interes
        //devuelve false si no hay memoria suficiente.
        bool InsertarDelante (T);
        //InsertarDetras (T): Inserta el T detras del pto de interes
        //devuelve false si no hay memoria suficiente.
        bool InsertarDetras (T);
        //Eliminar(): Elimina el T del pto de interes
        //devuelve false si el pto de interes esta mas alla del ultimo elemento
        bool Eliminar ();
        //Consultar (T&): Recupera el T del pto de interes
        //y lo almacena en el argumento por referencia
        //devuelve false si el pto de interes esta mas alla del ultimo elemento
        bool Consultar (T&);
        //ListaVacia(): Indica si la lista no tiene elementos
        //devuelve true si la lista esta vacia
        bool ListaVacia();

        //Operaciones con el pto. de interes:
        //IrInicio(): Desplaza el pto. de interes al primer elemento
        //de la lista
        void IrInicio();
        //IrUltimo(): Desplaza el pto. de interes al ultimo elemento
        //de la lista
        void IrUltimo();
        //Avanzar(): Desplaza el pto. de interes al siguiente elemento
        //de la lista. Es posible ir una posicion mas alla del ultimo
        //Devuelve false si el pto. de interes esta detras del ultimo elemento
        //y no es posible seguir avanzando
        bool Avanzar();
        //Retroceder(): Desplaza el pto. de interes al anterior elemento
        //de la lista. No es posible retroceder mas atras del primer elemento
        //Devuelve false si el pto. de interes esta en el primer elemento
        bool Retroceder ();
        //FinalLista(): Indica si el pto. de interes ha sobrepasado
        //la posicion del ultimo elemento de la lista y devuelve true
        //en ese caso.
        bool FinalLista();

	//Vacia la lista
	void Vaciar ();

	//Devuelve el puntero a la información a la que apunta el punto de interés
	T* getPuntero ();
	
	void* getPTO(){return pto;}
	void setPTO(void* p){pto = (Nodo*)p;}
	
	
	int Altura(){return tam;}
	
private: //lista doblemente enlazada circular con nodo cabeza
        struct Nodo;
        typedef Nodo* Puntero;
        struct Nodo
        {
            T info;
            Puntero sig;
            Puntero ant;
        };

	int tam;
	
        Puntero cabeza;
        Puntero pto;

        bool Copiar (const Lista&);
        
};


template<typename T>
Lista<T>::Lista ()
{
    cabeza = new Nodo;
    cabeza->sig = cabeza;
    cabeza->ant = cabeza;
    pto = cabeza;
    tam = 0;
}

template<typename T>
Lista<T>::Lista (const Lista& orig)
{
   cabeza = new Nodo;
   cabeza->sig = cabeza;
   cabeza->ant = cabeza;

   if (! Copiar (orig))
       cerr << "Lista: Error en el constructor de copia" << endl;    
}

template<typename T>
Lista<T>::~Lista ()
{
   Vaciar();
   delete cabeza;
}
template<typename T>
Lista<T> Lista<T>::operator= (const Lista& orig)
{
    Vaciar();
    if (! Copiar (orig))
       cerr << "Lista: Error en la asignacion" << endl;
    return (*this);
}

template<typename T> 
bool Lista<T>::InsertarUnico(T x)
{
    Puntero aux;
    bool ok;
    
    if(x < pto->info)   //Si el punto de interes esta por detras de donde queremos insertar, vamos al principio.
        pto = cabeza->sig;
    
    while(pto != cabeza)
    {
        if(x < pto->info)//De menor a mayor.
            break;
        pto = pto->sig;
    }
    if (pto!=cabeza)
    {//Inserta Delante
        if (!(pto->ant->info == x))
        {
            aux = new Nodo;
            if ( aux == NULL )
               ok = false;
            else
            {
               ok = true;
               aux->info = x;
               aux->sig = pto->sig;
               aux->ant = pto;
               pto->sig->ant = aux;
               pto->sig = aux;
               tam++;
            }
        }
    }
    return ok;
}

template<typename T> 
bool Lista<T>::InsertarOrdenado(T x)
{
    Puntero aux;
    bool ok;
    
    if(x < pto->info)   //Si el punto de interes esta por detras de donde queremos insertar, vamos al principio.
        pto = cabeza->sig;
    
    while(pto != cabeza)
    {
        if(x < pto->info)
            break;
        pto = pto->sig;
    }
    if (pto!=cabeza)
    {
        aux = new Nodo;
        if ( aux == NULL )
           ok = false;
        else
        {
           ok = true;
           aux->info = x;
           aux->sig = pto->sig;
           aux->ant = pto;
           pto->sig->ant = aux;
           pto->sig = aux;
           tam++;
        }
    }
    return ok;
}

template<typename T> 
bool Lista<T>::InsertarDelante ( T x )
{
    Puntero aux;
    bool ok;
    
    aux = new Nodo;
    if ( aux == NULL )
       ok = false;
    else
    {
       ok = true;
       aux->info = x;
       aux->sig = pto->sig;
       aux->ant = pto;
       pto->sig->ant = aux;
       pto->sig = aux;
       tam++;
    }
    return (ok);
}

template<typename T>
bool Lista<T>::InsertarDetras ( T x )
{
    Puntero aux;
    bool ok;

    aux = new Nodo;
    if ( aux == NULL )
       ok = false;
    else
    {
       ok = true;
       aux->info = x;
       aux->sig = pto;
       aux->ant = pto->ant;
       pto->ant->sig = aux;
       pto->ant = aux;
       tam++;
    }
    return (ok);
}

template<typename T>
bool Lista<T>::Eliminar ()
{
    Puntero aux;
    bool ok;

    if (pto == cabeza)
        ok = false;
    else
    {
        ok = true;
        aux =  pto;
        pto->sig->ant = pto->ant;
        pto->ant->sig = pto->sig;
        pto = pto->sig;
        delete aux;
    }
    return (ok);
}

template<typename T>
bool Lista<T>::Consultar (T& x)
{
    bool ok;

    if (pto == cabeza)
        ok = false;
    else
    {
        ok = true;
        x = pto->info;
    }
    return (ok);
}

template<typename T>
T* Lista<T>::getPuntero ()
{
    if (pto == cabeza)
        return NULL;
    else
	return &pto->info;
}

template<typename T>
bool Lista<T>::ListaVacia ()
{
    return ( cabeza->sig == cabeza );
}

template<typename T>
void Lista<T>::IrInicio()
{
    pto = cabeza->sig;
}

template<typename T>
void Lista<T>::IrUltimo()
{
    pto = cabeza->ant;
}

template<typename T>
bool Lista<T>::Avanzar()
{
    bool ok;

    if ( pto == cabeza )
        ok = false;
    else
    {
        ok = true;
        pto = pto->sig;
    }
    return ok;
}

template<typename T>
bool Lista<T>::Retroceder()
{
    bool ok;

    if ( pto == cabeza->sig )
        ok = false;
    else
    {
        ok = true;
        pto = pto->ant;
    }
    return ok;
}

template<typename T>
bool Lista<T>::FinalLista()
{
    return ( pto == cabeza );
}

template<typename T>
bool Lista<T>::Copiar (const Lista& orig)
{
    Puntero aux, dup;
    bool ok=true;
    tam = orig.tam;
    //El nodo cabeza de this ya existe,
    //no se vuelve a crear
    aux = orig.cabeza->sig;

    while ( aux != orig.cabeza )
    {
        dup = new Nodo;
        if (dup==NULL)
           ok =false;
        else
        {
           dup->info = aux->info;
           dup->sig = cabeza;
           dup->ant = cabeza->ant;
           cabeza->ant->sig = dup;
           cabeza->ant = dup;

           if ( aux == orig.pto ) //fijar pto. de interes
              pto = dup;
           aux = aux->sig;
        }
    }
    return (ok);
}

template<typename T>
void Lista<T>::Vaciar ()
{
   IrInicio();
   while ( Eliminar() );
}



#endif
