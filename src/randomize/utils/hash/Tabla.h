#ifndef _TABLA_HASH_H_
#define _TABLA_HASH_H_

template<class T, int MAX_ELEM>
class Tabla {
	private:
			
	public:
		T tabla[MAX_ELEM];
	
		
		//Tabla(){}
		
		
		T& Hash(float v[3]) { return Hash(v[0],v[1],v[2]); }
		T& Hash(float x, float y, float z)
		{
			return tabla[Modulo(int ( int(x) * 73856093 + int(y) * 19349663 + int(z) * 83492791), MAX_ELEM)];
		}
		T& Hash(const char* c,const int n)
		{
			int key = 0;
			for(int i = 0; i < n; i++)
				key+=c[i]*i;
			key = key*73856093 + c[0]*19349663+c[n-1]*83492791;
			return tabla[Modulo(key, MAX_ELEM)];
		}
		
		T& noHash(int i)
		{
			return tabla[i];
		}

		int Modulo(int n, int m)
		{
			if (m == 0)
				return n;
			
			int mod (n%m);

			if (mod < 0)
				mod += m;
			
			return mod;
		}
};



#endif //_TABLA_HASH_H_
