#ifndef _HASH_H_
#define _HASH_H_

#ifndef GRID_TAM
    #define GRID_TAM 1
#endif

#ifndef __CUDACC__
    #define __device__
    #define __host__
    #define __global__
#endif
#include <cuda.h>


#define KEY Vector
#define TYPE int
#define N_BUCKET 50
#define TAM_BUCKET 40

//template <class KEY, class TYPE, int N_BUCKET, int TAM_BUCKET>
class Hash {
    public:
        int v_num[N_BUCKET];
        TYPE h[N_BUCKET][TAM_BUCKET];

    public:
        Hash(){
            clear();
        }

       __device__ __host__  int hashFunc(KEY k){
            k = k+KEY(300,300,300);
            long long int x = k.x < 0 ? k.x-1 : k.x;
           // long long int y = k.y < 0 ? k.y-1 : k.y;
            long long int z = k.z < 0 ? k.z-1 : k.z;
            int ret = int((x * 73856093 + /*y * 19349663 + */z * 83492791)%N_BUCKET);

            return ret<0?ret+N_BUCKET:ret;
        }
        void insert(KEY k, TYPE t){
            int p = hashFunc(k);
            if(v_num[p] < TAM_BUCKET){
                h[p][v_num[p]++] = t;
            } else {
                //cout << "LIADA EN EL " <<  p << endl;
            }
        }
        __device__ __host__ TYPE* get(KEY k, int *n){
            int p = hashFunc(k);
            if(n != NULL){
                *n = v_num[p];
            }
            return h[p];
        }
        void clear(){
            memset(v_num, 0,N_BUCKET*sizeof(int));
        }
};

#endif //_HASH_H_
