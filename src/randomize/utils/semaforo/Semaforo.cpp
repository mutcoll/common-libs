#include "Semaforo.h"
#include <iostream>
using namespace std;

Semaforo::Semaforo():s(0),abierto(true){}

bool Semaforo::consumir()
{
	if (abierto)
		return true;
	else
		if (s > 0)
		{
			s--;
			return true;
		}
		else
			return false;
}

void Semaforo::sumar()
{
	if (!abierto)
		s++;
}

void Semaforo::abrir()
{
	abierto = true;
}

void Semaforo::cerrar()
{
	abierto = false;
	s = 0;
}

bool Semaforo::estado()
{
	//cout << "s = " << s << endl;	// DEPURACION
	//cout << "abierto = " << abierto << endl;	// DEPURACION
	return (abierto);
}

bool Semaforo::isOpen() {
    return abierto;
}

int Semaforo::getPoints() {
    return s;
}

bool Semaforo::toggle() {
    abierto = !abierto;
    s = 0;
    return abierto;
}

