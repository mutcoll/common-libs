#ifndef _SEMAFORO_H_
#define _SEMAFORO_H_

/**
 * Small class to control (non-concurrent safe) access.
 * you can easily avoid using this, but you will have to reimplement this,
 * spreading silly code around.
 * you control if you should enter to some zone by having points.
 * each access consumes one point.
 * if you have no points, you know you don't have to enter.
 */
class Semaforo
{
	public:
		Semaforo();
		bool consumir();	// test if we should access.
		void sumar();	// stack one more access grant
		void abrir();	// allow all accesses
		void cerrar();	// deny all accesses
		bool estado();	// ask if we will enter in the next Consumir()
        bool isOpen();
        int getPoints();
        bool toggle();  // toggle and return if isOpen()
	private:
		int s;
		bool abierto;
};

#endif
