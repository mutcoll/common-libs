
#include "SignalHandler.h"

#if !defined DO_NOT_BACKTRACE && !defined WIN32
using namespace std;

ostream * SignalHandler::out(nullptr);
__sighandler_t SignalHandler::defaultAbortHandler(nullptr);
bool SignalHandler::didWeGetThePreviousHandler(false);


void SignalHandler::activate(std::ostream &os) {
    out = &os;
    activate();
}

void SignalHandler::activate() {
    if (signal((int) SIGSEGV, handleSignal) == SIG_ERR) {
        throw runtime_error("!!!!! Error setting up signal handlers !!!!!");
    }
}

/**
 * Here you can see that the previous SIGABRT handler is stored. This is done because if that signal
 * is handled, wrapped in an exception, and the exception is not caught to make it end the program,
 * another SIGABRT will be raised, giving infinite recursion. So, if we already handled that signal,
 * we restore the default handler.
 */
void SignalHandler::activate(int signalToCatch) {
    __sighandler_t previousHandler = signal(signalToCatch, handleSignal);
    if (signalToCatch == SIGABRT && !didWeGetThePreviousHandler) {
        defaultAbortHandler = previousHandler;
        didWeGetThePreviousHandler = true;
    }

    if (previousHandler == SIG_ERR) {
        throw runtime_error("!!!!! Error setting up signal handlers !!!!!");
    }
}

// a previous abort was raised in the same execution. It may be a different one,
// but if it's the same and we handle it again we make infinite recursion.
void SignalHandler::handleSignal(int signalToCatch) {
    static bool alreadyAborted = false;

    if (!alreadyAborted) {
        if (signalToCatch == SIGABRT) {
            alreadyAborted = true;
        }
        string message = "exception wrapper for a raised signal ";
        message += to_string(signalToCatch) + ".";
        randomize::utils::exception::StackTracedException exception(message);
        if (out != nullptr) {
            (*out) << exception.what();
        }

        throw exception;
    } else {
        if (didWeGetThePreviousHandler && defaultAbortHandler != nullptr) {
            defaultAbortHandler(signalToCatch);
        }
    }
}

#else // DO_NOT_BACKTRACE

void SignalHandler::activate(std::ostream &os) {}
void SignalHandler::activate() {}
void SignalHandler::activate(int) {}
void SignalHandler::handleSignal(int) {}

#endif // DO_NOT_BACKTRACE

