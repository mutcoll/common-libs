/**
 * @file Walkers.h
 * @author jmmut 
 * @date 2016-02-15.
 */

#ifndef COMMONLIBS_WALKERS_H
#define COMMONLIBS_WALKERS_H

#include <string>
#include <sstream>
#include <functional>
#include <algorithm>
#include <numeric>


namespace randomize {
namespace utils {


template<typename C>
std::string container_to_string(C v) {
//    string joined = join<vector<T>, string, function<string(T, string)>>(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
//    string joined = join(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
    std::string joined = std::accumulate(
            std::begin(v),
            std::end(v),
            std::string(),
            [] (std::string s, decltype(*std::begin(v)) elem) {
                return s + " " + std::to_string(elem) + ",";
            }
    );
    joined[0] = '[';
    joined[joined.size()-1] = ']';
    return joined;
}


/**
 * the other one can not print vector<string>
 */
template<typename C>
std::string container_to_string_ss(C v) {
    if (v.empty()) {
        return "[]";
    } else {
        //    string joined = join<vector<T>, string, function<string(T, string)>>(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
        //    string joined = join(v, [] (T elem, string s) -> string {return s + ", " + to_string(elem);});
        std::string joined = std::accumulate(
                std::begin(v),
                std::end(v),
                std::string(),
                [] (std::string s, decltype(*std::begin(v)) elem) {
                    std::stringstream ss;
                    ss << s << " " << elem << ",";
                    return ss.str();
                }
        );
        joined[0] = '[';
        joined[joined.size()-1] = ']';
        return joined;
    }
}

};  // utils
};  // randomize

#endif //COMMONLIBS_WALKERS_H
