#ifndef _UTILS_STRING_H_
#define _UTILS_STRING_H_

#include <string>
#include <vector>

namespace Utils{ namespace String{

std::vector<std::string> split( const std::string &str, const std::string &endChars);

};};

#endif//_UTILS_STRING_H_
