/**
 * @file TimeRecorder.h
 * @author jmmut 
 * @date 2015-10-10.
 */

#ifndef GEMU_TIMERECORDER_H
#define GEMU_TIMERECORDER_H


#include <chrono>
#include <string>
#include <sstream>


namespace randomize {
namespace utils {
namespace time {


/**
Usage:
- call start()
- call stop()
- call any other method to retrieve the duration.

simplest example:
    TimeRecorder timeRecorder;
    timeRecorder.start();
    // do your stuff...
    timeRecorder.stop();
    cout << timeRecorder << endl;   // or timeRecorder.toString("custom description")

anything out of this (in this strict order) is undefined behaviour. I could add some asserts or exceptions, but
I would be adding overhead checking stupid usage.

Reusing variables is OK. you can do the loop {start, stop, show} any number of times.
*/
class TimeRecorder {
private:
    std::chrono::time_point<std::chrono::steady_clock, std::chrono::nanoseconds> t0;
    std::chrono::nanoseconds dt; // equivalent to: std::chrono::duration<long int, ratio<1, 1000000000>> dt;
public:
    void start();
    void stop();
    std::chrono::nanoseconds value() const;
    std::chrono::seconds seconds() const;
    std::chrono::nanoseconds nanoseconds() const;
    std::string toString() const;
    std::string toString(const std::string &intervalDescription) const;
    std::ostream & operator>>(std::ostream & os);
    friend std::ostream & operator<<(std::ostream & os, const TimeRecorder &tr);

};

};  // time
};  // utils
};  // randomize

#endif //GEMU_TIMERECORDER_H
