/**
 * @file TimeRecorder.cpp
 * @author jmmut 
 * @date 2015-10-10.
 */

#include "TimeRecorder.h"


namespace randomize {
namespace utils {
namespace time {

using namespace std::chrono;
using std::string;
using std::stringstream;

void TimeRecorder::start() {
    t0 = steady_clock::now();
}

void TimeRecorder::stop() {
    dt = steady_clock::now() - t0;
}

std::chrono::nanoseconds TimeRecorder::value() const {
    return dt;
}

std::chrono::seconds TimeRecorder::seconds() const {
    std::chrono::seconds s;
    s = duration_cast<std::chrono::seconds>(dt);
//    return std::chrono::duration_cast<seconds>(dt);
    return s;
}

std::chrono::nanoseconds TimeRecorder::nanoseconds() const {
//    std::chrono::nanoseconds ns;
//    ns = duration_cast<nanoseconds>(dt);
    return duration_cast<std::chrono::nanoseconds>(dt);
}

std::string TimeRecorder::toString() const {
    return this->toString("dt");
}

std::string TimeRecorder::toString(const std::string &intervalDescription) const {
    stringstream ss;

    ss << intervalDescription << ": " << seconds().count() << ".";
    ss.fill('0');
    ss.width(9);
    ss << dt.count() << " s";

    return ss.str();
}

std::ostream &TimeRecorder::operator>>(std::ostream &os) {
    return os << this->toString();
}

std::ostream& operator<<(std::ostream &os, const TimeRecorder & tr) {
    return os << tr.toString();
}

};  // time
};  // utils
};  // randomize


