#ifndef _STATIC_VEC_H_
#define _STATIC_VEC_H_

#include "StaticMem.h"
#include "randomize/common-libs/src/log/Log.h"

template <class T, int NUM_BLOCS, int NUM_ELEMENTS, int MAX_BLOCS>
class StaticVec
{
	public:
		
		StaticVec();
		StaticVec(StaticMem<T,NUM_BLOCS,NUM_ELEMENTS>  *m)
		{
			mem = m;
		}
		
		/**
		 * Accede al elemento i del vector
		 * */
		T& operator[](int i);
		
		/**
		 * Aumenta en un bloque su tamaño
		 * */
		void crece();

		/**
		 * Aumenta en N bloques su tamaño
		 * */
		void crece(int n);
		
		/**
		 * Decrece el tamaño en un bloque
		 * */
		void decrece();
		
		/**
		 * Libera toda la memoria
		 * */
		void free();
		
		
		/**
		 * Devuelve el tamaño del vector
		 * */
		int getLength();
		
		void setId(int i){id = i;}
		int getId(){return id;}
		static void setMem(StaticMem<T,NUM_BLOCS,NUM_ELEMENTS>  *m)
		{mem = m;}
		
	private:
		static StaticMem<T,NUM_BLOCS,NUM_ELEMENTS> *mem;
		int v_blocs[MAX_BLOCS];
		int num_blocs;
		int length;
		int id;
		
		
};

template <class T, int NUM_BLOCS, int NUM_ELEMENTS, int MAX_BLOCS>
StaticMem<T,NUM_BLOCS,NUM_ELEMENTS> *StaticVec<T,NUM_BLOCS,NUM_ELEMENTS,MAX_BLOCS>::mem = 0;


template <class T, int NUM_BLOCS, int NUM_ELEMENTS, int MAX_BLOCS>
StaticVec<T,NUM_BLOCS,NUM_ELEMENTS,MAX_BLOCS>::StaticVec() : num_blocs(0), length(0), id(-1)
{
}

/**
 * Accede al elemento i del vector
 * */
template <class T, int NUM_BLOCS, int NUM_ELEMENTS, int MAX_BLOCS>
T& StaticVec<T,NUM_BLOCS,NUM_ELEMENTS,MAX_BLOCS>::operator[](int i)
{
	int block = i/NUM_ELEMENTS;
	if(block >= num_blocs)
		Log::error("El id(%d) intenta acceder al elemento i(%d) del bloque(%d) que no es suyo. Tiene num_blocs(%d)", id, i, block, num_blocs);
	if(i >= length)
		Log::error("El id(%d) intenta acceder al elemento i(%d) . Tiene length(%d)", id, i,length);
		
	return mem->accessMem(v_blocs[block],i%NUM_ELEMENTS, id);
}

/**
 * Aumenta en un bloque su tamaño
 * */
template <class T, int NUM_BLOCS, int NUM_ELEMENTS, int MAX_BLOCS>
void StaticVec<T,NUM_BLOCS,NUM_ELEMENTS,MAX_BLOCS>::crece()
{
	crece(1);
}

/**
 * Aumenta en N bloques su tamaño
 * */
template <class T, int NUM_BLOCS, int NUM_ELEMENTS, int MAX_BLOCS>
void StaticVec<T,NUM_BLOCS,NUM_ELEMENTS,MAX_BLOCS>::crece(int n)
{
	for(int i = 0; i < n; i++)
	{
		if(num_blocs == MAX_BLOCS)
			break;
		v_blocs[num_blocs] = mem->takeMem(id);
		if(v_blocs[num_blocs] == -1)	//Memoria llena
			break;
		num_blocs++;
		length += NUM_ELEMENTS;
	}
	Log::info("Libres en mem: %d",mem->num_free);
}

/**
 * Decrece el tamaño en un bloque
 * */
template <class T, int NUM_BLOCS, int NUM_ELEMENTS, int MAX_BLOCS>
void StaticVec<T,NUM_BLOCS,NUM_ELEMENTS,MAX_BLOCS>::decrece()
{
	if(num_blocs > 0)
	{
		mem->freeMem(v_blocs[num_blocs-1], id);
		num_blocs--;
		length -= NUM_ELEMENTS;
	}
}

/**
 * Livera toda la memoria
 * */
template <class T, int NUM_BLOCS, int NUM_ELEMENTS, int MAX_BLOCS>
void StaticVec<T,NUM_BLOCS,NUM_ELEMENTS,MAX_BLOCS>::free()
{
	for(int i = 0; i < num_blocs; i++)
	{
		mem->freeMem(v_blocs[i], id);
	}
	num_blocs = 0;
	length = 0;
}


/**
 * Devuelve el tamaño del vector
 * */
template <class T, int NUM_BLOCS, int NUM_ELEMENTS, int MAX_BLOCS>
int StaticVec<T,NUM_BLOCS,NUM_ELEMENTS,MAX_BLOCS>::getLength()
{
	return length;
}
		

#endif //_STATIC_VEC_H_
