
#ifndef _STATIC_MEM_H_
#define _STATIC_MEM_H_
#include "randomize/common-libs/src/log/Log.h"
template <class T, int NUM_BLOCS, int NUM_ELEMENTS>
class StaticMem
{
	public:
	
		StaticMem();
	
		/**
		 * Concede un bloque de memoria
		 * El bloque son T[NUM_ELEMENTS]
		 * */
		int takeMem(int id);
		
		/**
		 * Libera el bloque de memoria indicado
		 * */
		void freeMem(int block, int id)
		{
			if(v_id[block] != id)
				Log::error("ERROR: Liberacion no autorizada de id(%d) al bloque(%d) de v_id(%d)", id, block, v_id[block]);
			if(!free[block])
			{
				num_free++; 
				free[block]=true;
			}
		}
		
		/**
		 * Accede al elemento i del bloque de memoria indicado
		 * */
		T& accessMem(int block, int i, int id)
		{
			if(v_id[block] != id)
				Log::error("ERROR: Acceso no autorizado de id(%d) al bloque(%d) de v_id(%d)", id, block, v_id[block]);
			return mem[block][i];
		}
	
	public:
		T mem[NUM_BLOCS][NUM_ELEMENTS];
		bool free[NUM_BLOCS];
		int v_id[NUM_BLOCS];
		//Indica la última posición donde se encontró un hueco libre. 
		//La siguiente vez se empieza a buscar por ese sitio.
		int index;
		int num_free;
		
	
};



template <class T, int NUM_BLOCS, int NUM_ELEMENTS>
StaticMem<T, NUM_BLOCS, NUM_ELEMENTS>::StaticMem() : index(0), num_free(NUM_BLOCS)
{
	for(int i = 0; i < NUM_BLOCS; i++)
		free[i]=true;
}

/**
 * Concede un bloque de memoria
 * El bloque son T[NUM_ELEMENTS]
 * */
template <class T, int NUM_BLOCS, int NUM_ELEMENTS>
int StaticMem<T, NUM_BLOCS, NUM_ELEMENTS>::takeMem(int id)
{
	if(num_free > 0)
	{
		while(free[index] == false)	//Nos saltamos los ocupados
		{
			index++;
			if(index >= NUM_BLOCS)
				index -= NUM_BLOCS;
		}
		//Salimos porque index apunta a un bloque libre
		free[index] = false;
		v_id[index] = id;
		num_free--;
		return index;
	}
	else
		return -1;
	
}


//template <class T, int NUM_BLOCS, int NUM_ELEMENTS>
//void StaticMem<T, NUM_BLOCS, NUM_ELEMENTS>::freeMem(int bloque)
//{
	//free[bloque] = true;
//}


//template <class T, int NUM_BLOCS, int NUM_ELEMENTS>
 //T StaticMem<T, NUM_BLOCS, NUM_ELEMENTS>::accessMem(int bloque, int i)
//{
	//return mem[bloque][i];
//}



#endif //_STATIC_MEM_H_
