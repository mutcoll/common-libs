using namespace std;
#include <stdlib.h>

#ifndef _VECTOR_N_H_
#define _VECTOR_N_H_

template <class T>
class Vector_n
{
	private:
		struct nodo;
		struct nodo
		{
			nodo * sig;
			T* inf;
			int tam;
		};
		
		int tam;
		nodo* ini;
		nodo* fin;
	
	public:
		Vector_n (int n);
		Vector_n ();
		~Vector_n ();
		bool Crece (int n );	
		T& operator[] (int n);//Acceder de forma transparente
		T& Locate(int x, int y);//Acceder de forma lista de vectores
		int length(){return tam;}

};

template <class T>
Vector_n<T>::Vector_n (int n)
{
	ini = new nodo;
	ini->sig = NULL;
	ini->inf = new T[n];
	ini->tam = n;
	fin = ini;
	tam = n;
}

template <class T>
Vector_n<T>::Vector_n ()
{
	ini = new nodo;
	ini->sig = NULL;
	ini->inf = new T[0];
	ini->tam = 0;
	fin = ini;
	tam = 0;
}

template <class T>
Vector_n<T>::~Vector_n()
{
	nodo *aux = ini;
	nodo *aux2;
	while(aux->sig != NULL)
	{
		delete[] aux->inf;
		aux2 = aux->sig;
		delete aux;
		aux = aux2;
	}
	//delete ini;
	delete fin;
}

template <class T>
T& Vector_n<T>::operator[](int n)
{
	int posf = ini->tam;
	int posi = 0;
	nodo* aux = ini;
	while(n >= posf)
	{
		posi = posf;
		aux = aux->sig;
		posf += aux->tam;
	}
	return aux->inf[n-posi];
}

template <class T>
T& Vector_n<T>::Locate(int x, int y)
{
	nodo* aux = ini;
	for (int i = 0; i < x; i ++)
		aux = aux->sig;
	return aux->inf[y];
	
}
template <class T>
bool Vector_n<T>::Crece(int n)
{
	fin->sig = new nodo;
	fin = fin->sig;
	fin->inf = new T[n];
	fin->tam = n;
	fin->sig = NULL;
	tam += n;
	
	return true;
}
#endif

