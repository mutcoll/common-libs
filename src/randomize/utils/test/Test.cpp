
#include <randomize/utils/time/TimeRecorder.h>
#include "Test.h"

namespace randomize {
namespace utils {
namespace test {

using namespace std::chrono;

int time_test(int (*func)(int argc, char **argv), int argc, char **argv) {
    time::TimeRecorder dt;
    dt.start();

    int ret = func(argc, argv);

    dt.stop();
    std::cout << dt << std::endl;

    return ret;
}

}   // test
}   // utils
}   // randomize
