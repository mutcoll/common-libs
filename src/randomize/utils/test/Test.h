/* 
 * File:   Test.h
 * Author: jmmut
 *
 * Created on 2014-11-02
 */

#ifndef _TEST_H_
#define	_TEST_H_

#include <chrono>
#include <iostream>

namespace randomize {
namespace utils {
namespace test {

/**
 * Runs the parameter function and prints the time elapsed.
 * @param func must have the signature of a standard main
 * @param argc
 * @param argv
 * @return the returned value by func
 */
int time_test(int (*func)(int argc, char **argv), int argc, char **argv);

}   // test
}   // utils
}   // randomize
#endif	/* TEST_H */

