/**
 * @file TestSuite.h
 * @author jmmut 
 * @date 2015-10-10.
 */

#ifndef COMMONLIBS_TESTSUITE_H
#define COMMONLIBS_TESTSUITE_H

#include <vector>
#include <set>
#include <functional>
#include <iostream>
#include <string>
#include <randomize/utils/exception/StackTracedException.h>
#include "randomize/log/log.h"

namespace randomize {

namespace utils {
namespace test {

/**
 * to use like this:
```
int main (int argc, char **argv) {
    TestSuite suite(argc, argv, {
            test1, test2,
    }, "my test suite or class test");
}
```
 * the name/description string is optional. You can use it to differentiate between TestSuites.
 * the tests must have the same prototype as the main, to be able to receive arguments. they must
 * return 0 if the test passed, and other value otherwise. An easy way to think about it is
 * returning how many fails detected the test. Example:
```
int test1 (int argc, char **argv) {
    int fails = 0;
    fails += (2+2 != 4);
    fails += !(somethingThatMustBeTrue())
    return fails;
}
```
 */
class TestSuite {
private:
    std::vector<std::function<int()>> functions;
    std::set<int> failedTests;
    std::string name;

public:
    TestSuite(int argc, char **, std::initializer_list<int (*)(int, char**)>, std::string name = "");
    TestSuite(std::initializer_list<void(*)()>, std::string name = "");
    unsigned long countFailed();

    void run();
};

#define ASSERT_OR_THROW(expression) ASSERT_OR_THROW_MSG((expression), "")

#define ASSERT_OR_THROW_MSG(expression, message) \
    if (not (expression)) \
        throw randomize::utils::exception::StackTracedException(\
                __FILE__ ":" + std::to_string(__LINE__) + (": assertion ("  #expression  ") failed: ")\
                        + std::to_string(expression) + ". " + (message))

#define ASSERT_EQUALS_OR_THROW(actual, expected) ASSERT_EQUALS_OR_THROW_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_MSG(actual, expected, message) \
    if (not ((actual) == (expected))) \
        throw randomize::utils::exception::StackTracedException( \
                __FILE__ ":" + std::to_string(__LINE__) \
                + (": assertion (" #actual " == " #expected ") failed: ") \
                + std::to_string(actual) + " == " + std::to_string(expected) \
                + ". " + (message))



};  // test
};  // utils
};  // randomize

#endif //COMMONLIBS_TESTSUITE_H
