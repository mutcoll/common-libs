/**
 * @file TestSuite.cpp
 * @author jmmut 
 * @date 2015-10-10.
 */

#include "TestSuite.h"

namespace randomize {
namespace utils {
namespace test {


using std::string;

TestSuite::TestSuite(int argc, char **argv,
        std::initializer_list<int (*)(int, char **)> mains, std::string name)
        : name(name) {

    for (auto &&func : mains) {
        functions.push_back([=]() {
            return func(argc, argv);
        });
    }
    run();
}

TestSuite::TestSuite(std::initializer_list<void (*)()> mains, string name) : name(name) {

    for (auto &&func : mains) {
        functions.push_back([=]() {
            func();
            return 0;
        });
    }
    run();
}

void TestSuite::run() {
    int i = 0;
    for (auto &func : functions) {
        int res = 0;
        try {
            res = func();
        } catch (std::exception &except) {
            std::cout << "test " << name << "[" << i << "]: " << except.what() << std::endl;
            res = 1;
        }
        bool failed = res != 0;
        if (failed) {
            failedTests.insert(i);
        }
        i++;
    }

    std::cout << "TestSuite: " << failedTests.size() << "/" << functions.size() << " "
        << name << " tests failed " << std::endl;

    if (failedTests.size() != 0) {
        string tests;
        for (auto index : failedTests) {
            tests += std::to_string(index);
            tests += ", ";
        }
        LOG_DEBUG("failed tests indices are: %s", tests.c_str());
    }
}

unsigned long TestSuite::countFailed() {
    return failedTests.size();
}
};  // test
};  // utils
};  // randomize

