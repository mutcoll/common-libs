/**
 * @file StackTracedException.h
 * @author jmmut 
 * @date 2016-03-26.
 */

#ifndef COMMONLIBS_STACKTRACEDEXCEPTION_H
#define COMMONLIBS_STACKTRACEDEXCEPTION_H


// in case the user doesn't have execinfo.h, or unix's signals: disable backtracing on segfaults:
//#define DO_NOT_BACKTRACE


#if !defined DO_NOT_BACKTRACE && !defined WIN32

#include <signal.h>
#include <errno.h>
#include <execinfo.h>
#include <sys/types.h>
#include <unistd.h>
#include <cxxabi.h>
#endif // DO_NOT_BACKTRACE

#include <iostream>
#include <sstream>
#include <stdexcept>

namespace randomize {
namespace utils {
namespace exception {

/**
 * exception that carries the call stack in the message. Use as a regular exception:

    if (badcondition) {
        throw StackTracedException("something went wrong");
    }
 */
class StackTracedException : public std::exception {
private:
    std::string message;

public:
    StackTracedException(const std::string &message);
    virtual const char *what() const noexcept override;
};


};  // exception

void print_stacktrace(std::ostream &out, unsigned int max_frames = 63);

};  // utils
};  // randomize


#endif //COMMONLIBS_STACKTRACEDEXCEPTION_H
