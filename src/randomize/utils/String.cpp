#include <algorithm>
#include "String.h"

std::vector<std::string> Utils::String::split( const std::string &str, const std::string &endChars){
	std::vector<std::string> strings;
	std::string::const_iterator first = str.begin(), last;
	for(last = first; last != str.end(); last++){
		if( std::find( endChars.begin(), endChars.end(), *last) != endChars.end()){
			if(first != last)
				strings.push_back(std::string(first,last));
			first = last + 1;
		}
	}
	if(first != last)
		strings.push_back(std::string(first,last));
	return strings;
}

