 /***************************************
 * QuickSort Algorith
 * 		Using klib methodology
 * 
 * Author: Jacobo Coll Moragón
 *
 * Data: 2014-02-01
 * Version: 1.0
 *
 ***************************************/

#ifndef _QS_SEC_H_
#define _QS_SEC_H_

#define QS_INIT(NAME, TIPO_DATO, COMPARATION)		\
void qs_##NAME (TIPO_DATO *v, int ini_v, int fin_v, int profundidad){					\
	                                                                                                \
	TIPO_DATO pivote = v[fin_v-1];                                                                  \
	TIPO_DATO aux;                                                                                  \
	int ini = ini_v;                                                                                \
	int fin = fin_v-2;                                                                              \
	int p;                                                                                            \
	if(fin <= ini){                                                                                 \
		if(fin == ini){                                                                             \
			p = fin_v-1;                                                                            \
			if(COMPARATION(v[p],v[ini])){                                                           \
				aux = v[p];                                                                         \
				v[p] = v[ini];                                                                      \
				v[ini] = aux;                                                                       \
			}                                                                                       \
		}                                                                                           \
		return;                                                                                     \
	}                                                                                               \
                                                                                                    \
	while(ini < fin){                                                                               \
		if(COMPARATION(v[ini], pivote)){                                                            \
			ini++;                                                                                  \
			if(!COMPARATION(v[fin], pivote)){                                                       \
				fin--;                                                                              \
			}                                                                                       \
			                                                                                        \
		} else if(!COMPARATION(v[fin], pivote)){                                                    \
			fin--;                                                                                  \
		} else {                                                                                    \
			aux = v[fin];                                                                           \
			v[fin] = v[ini];                                                                        \
			v[ini] = aux;                                                                           \
		}                                                                                           \
	}                                                                                               \
	p = fin>ini?fin:ini;                                                                            \
	if(pivote > v[p])                                                                               \
		p++;                                                                                        \
		                                                                                            \
                                                                                                    \
	                                                                                                \
	v[fin_v-1] = v[p];                                                                              \
	v[p] = pivote;                                                                                  \
	                                                                                                \
	                                                                                                \
	qs_##NAME(v, ini_v, p, profundidad+1);                                                          \
	qs_##NAME(v, p, fin_v, profundidad+1);                                                          \
	                                                                                                \
}

#define __COMPARATION_LE(a,b) (a <= b)
#define __COMPARATION_G(a,b) (a > b)


#define QSORT_L2M_INIT(NAME, TIPO_DATO) QS_INIT(NAME, TIPO_DATO, __COMPARATION_G)
#define QSORT_M2L_INIT(NAME, TIPO_DATO) QS_INIT(NAME, TIPO_DATO, __COMPARATION_LE)


#define QS(NAME, V, SIZE) qs_##NAME(V, 0, SIZE, 0)


#endif /* _QS_SEC_H_ */
