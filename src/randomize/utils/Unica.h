#ifndef _UNICA_H_
#define _UNICA_H_


template<class T, int id = 0>
class Unica()
{

	static T* ptr = NULL;
	
	static T* get()  {
		if(ptr == NULL){
			ptr = new T;
		}
		return ptr;
	}

};


#endif //_UNICA_H_


//Moto *m1 = Unica<Moto, 0>::get()
//Moto *m2 = Unica<Moto, 1>::get()
//Moto *m3 = Unica<Moto, 2>::get()
//enum {TIME = 0, FPS = 1};
//float &time = Unica<float, TIME>::get()
//float &fps = Unica<float, FPS>::get()

