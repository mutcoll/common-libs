#ifndef _PORT_H_
#define _PORT_H_

#ifdef WIN32

#define _USE_MATH_DEFINES	// for M_PI
#include <cmath>
#ifndef M_PI
const double M_PI = 3.14159265358979323846;   // pi
#endif
inline void sincosf(float angle, float* s, float* c) {
	*s = sin(angle);
	*c = cos(angle);
}

#include <cstring>
inline int strncasecmp(const char * c1, const char *c2, int l) { return _strnicmp(c1, c2, l); }

#endif

#endif
