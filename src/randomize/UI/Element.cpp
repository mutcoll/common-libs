#include "Element.h"

//float x = ((px-win_w/2.0)/win_w)*100.0;
//float y = -((py-win_h/2.0)/win_h)*100.0;

//int px = (x/100.0)*win_w + win_w/2.0;
//int py = - ((y/100.0)*win_h + win_h/2.0);

//Para usar los elementos private como si fueran public.
namespace randomize { namespace UI{
	extern int win_h;
	extern int win_w;
	extern float asp;
	extern bool test_box;
};

UI::Element::Element():
	pos_x(0),pos_y(0),
	tam_h(1),tam_w(1),
	degree(0),
	layout(LAYOUT_FREE)
{
}

void UI::Element::loadFromSFL( const char *path){
	GLfloat w = 0, h = 0;
	std::string str;
	std::ifstream file( path);
	file >> str;
	for(GLuint i = this->v_capa.size(); !file.eof(); i++){
		if( str == "INLINE") this->addCapa( i, file);
		else this->addCapa( i, str.c_str());
		if( w == 0 && h == 0) { w = this->tam_w; h = this->tam_h;}
		file >> str;
		if( str == "NO_COLISIONABLE") this->v_capa[i].setColisionable( false);
		file >> str;
	}
	file.close();
	this->tam_w = w;
	this->tam_h = h;
}

/**
* Posición en pixeles absolutos
* */
void UI::Element::setPosPixel(int x, int y)
{
	//Coordenadas pixel -> lógicas
	convertPixelToLogic(x, y, pos_x, pos_y);
}

void UI::Element::setTamPixel(float w, float h) {
	tam_w = w / win_w * 100.0;
	tam_h = h / win_h * 100.0;

	if(asp > 1) 	//Apaisado
		tam_w *= asp;
	else
		tam_h /=asp;
}


/**
* Posición en pixeles absolutos
* */
void UI::Element::addPosPixel(int x, int y)
{
	float px = float(x)/win_w*100.0;
	float py = -(float(y))/win_h*100.0;
	
	if(asp > 1) 	//Apaisado
		px *= asp;
	else
		py /=asp;
	//LOG_DEBUG("Add %f, %f ",px, py);
	pos_x+=px;
	pos_y+=py;
}

/**
 * Coloca según configuración predeterminada
 * */
 //enum UI::Layout {	
					//LAYOUT_CENTER, 
					//LAYOUT_FULL_SCREEN, 
					//LAYOUT_HALF_SCREEN_DOWN,
					//LAYOUT_HALF_SCREEN_UP,
					//LAYOUT_FREE
					//};
 void UI::Element::setLayout(UI::Layout lay)
 {
	layout = lay;
	
	float tw = 100, th = 100;	//Tamaño en coordenadas UI de la pantalla
	if(asp > 1)
		tw*=asp;
	else
		th/=asp;
		
	switch(lay)
	{
		case LAYOUT_CENTER:
			setPos(0,0);
			break;
		case LAYOUT_FULL_SCREEN:
			setTam(tw,th);
			setPos(0,0);		
			break;
		case LAYOUT_HALF_SCREEN_DOWN:
			
			setTam(tw,th/2);
			LOG_INFO("Colocando en tamaño %fx%f",tw, th);
			setPos(0,-th/4);
			break;
		case LAYOUT_HALF_SCREEN_UP:
			setTam(tw,th/2);
			setPos(0,th/4);
			break;
		case LAYOUT_FREE:
			break;
		default:
			layout = LAYOUT_FREE;
	}
	 
 }


/**
* Establece la imagen que muestra una capa seleccionada
* */
void UI::Element::setCapa(unsigned int num_capa, int x, int y)
{
	if(num_capa < v_capa.size())
	{
		v_capa[num_capa].setFrame(x,y);
	}
};

void UI::Element::setCapa(unsigned int num_capa, int n)
{
	if(num_capa < v_capa.size())
	{
		v_capa[num_capa].setFrame(n);
	}
};

void UI::Element::advanceFrameCapa(unsigned int num_capa, int n)
{
	if(num_capa < v_capa.size())
	{
		v_capa[num_capa].advanceFrame(n);
	}
};

void UI::Element::setCapaActivo(unsigned int num_capa, bool activo)
{
	if(num_capa < v_capa.size())
	{
		v_capa[num_capa].setActivo(activo);
	}
};

void UI::Element::setCapaColisionable(unsigned int num_capa, bool colisionable)
{
	if(num_capa < v_capa.size())
	{
		v_capa[num_capa].setColisionable(colisionable);
	}
};

/**
* Añade o modifica una capa. Hay que especificar:
* 
* @param int num_capa : Numero de capa
* @param int tex : Identificador de la textura
* 
* */
void UI::Element::addCapa(int num_capa, GLuint tex, int num_x, int num_y)
{
	if(num_capa >= v_capa.size()) v_capa.resize( num_capa+1);
	if(num_capa < v_capa.size())
	{
		v_capa[num_capa] = Capa(tex,num_x,num_y);
	}
	
}

/**
 * Añade o modifica una capa. Hay que especificar:
 * 
 * @param int num_capa : Numero de capa
 * @param const char *path : archivo .ifl a cargar
 * 
 * */
void UI::Element::addCapa(int num_capa, const char *path){
	if(num_capa >= v_capa.size()) v_capa.resize( num_capa+1);
	if(num_capa < v_capa.size())
	{
		GLuint w,h;
		this->v_capa[num_capa] = Capa( path);
		this->v_capa[num_capa].getSize(w,h);
		this->setTamPixel( w, h);
	}
}

/**
 * Añade o modifica una capa. Hay que especificar:
 * 
 * @param int num_capa : Numero de capa
 * @param std::ifstream &file : stream con la posicion de lectura al principio de la descripcion insertada de un .ifl
 * 
 * */
void UI::Element::addCapa(int num_capa, std::istream &file){
	if(num_capa >= v_capa.size()) v_capa.resize( num_capa+1);
	if(num_capa < v_capa.size())
	{
		GLuint w,h;
		this->v_capa[num_capa] = Capa( file);
		this->v_capa[num_capa].getSize(w,h);
		this->setTamPixel( w, h);
	}
}

//bool UI::Element::run(int action, int x, int y)
//{
	//if(dentro(x,y))
	//{
		//CB_Click(action,x,y);
		//return true;
	//}
	//else
	//{
		//CB_NoClick();
		//return false;
	//}
	
//}

/**
* Indica si una pulsación está dentro o no del elemento
* */
bool UI::Element::inside(int x, int y){

	GLuint i;
	GLfloat despX, despY, scaleW, scaleH;	

	float px =  (x-win_w/2.0)/win_w*100.0;
	float py =  -(y-win_h/2.0)/win_h*100.0;
	
	if(asp > 1) 	//Apaisado
		px *= asp;
	else
		py /=asp;
	
	px -= pos_x;
	py -= pos_y;
	
	
	float cos_;
	float sin_;
	sincosf(degree*M_PI/180, &sin_, &cos_);
	
	float vx =  cos_*px + sin_*py;
	float vy = -sin_*px + cos_*py;
	//cout << "\nCp: " <<  x << " " <<  y << endl;
	//cout <<   "Cw: " << px << " " << py << endl;
	//cout <<   "Cv: " << vx << " " << vy << endl;
	//for(int yi = 0; yi < 4; yi++) { for(int xi = 0; xi < 4; xi++) {printf("%3.2f\t",imat[yi*4+xi]);}cout << endl;}cout << endl;

	for( i=0; i<v_capa.size(); i++)
		if(v_capa[i].isActivo() && v_capa[i].isColisionable()){
			v_capa[i].getPos( despX, despY);
			v_capa[i].getSize( scaleW, scaleH);
			if(	vx > -scaleW*tam_w/2 + despX*tam_w && vx < scaleW*tam_w/2 + despX*tam_w && 
				vy > -scaleH*tam_h/2 + despY*tam_h && vy < scaleH*tam_h/2 + despY*tam_h)
				return true;
	}
	return false;
}

int UI::Element::CB_Click(UI::Event action, int x, int y){
	if(listener!=nullptr)
		listener(action, x, y, this, user_data);
	return 0;
}

/*
bool UI::Element::dentro(int x, int y){
	
	
	if(!imat_ok){
		gluInvertMatrixd(mat, imat);
		imat_ok = true;
	}
	cout << "Cp: " << x << " " << y << endl;
	
	float px =  (x-win_w/2.0)/win_w*100.0;
	float py =  (y-win_h/2.0)/win_h*100.0;
	cout << "Cw: " << px << " " << py << endl;
	
	if(asp > 1) 	//Apaisado
		px *= asp;
	else
		py /=asp;
		
	float vx = imat[0]*px-imat[4]*py+imat[12];
	float vy = imat[1]*px-imat[5]*py+imat[13];
	cout << "Cv: " << vx << " " << vy << endl;
	for(int yi = 0; yi < 4; yi++) { for(int xi = 0; xi < 4; xi++) {printf("%3.2f\t",imat[yi*4+xi]);}cout << endl;}cout << endl;

	if(	vx > -0.5 && vx < 0.5 && 
		vy > -0.5 && vy < 0.5)
	{
		return true;
	}
	else
		return false;
}
*/


/*
bool UI::Element::dentro_old(int X, int Y)
{
	cout << "Cp: " << X << " " << Y << endl;
	
	//float x = mat[0]*X+mat[4]*Y+mat[12];
	//float y = mat[1]*X+mat[5]*Y+mat[13];
	//float x = (X-mat[12])/mat[0];
	//float y = (Y-mat[13])/mat[5];
	//float x = mat[0]*X+mat[4]*Y;
	//float y = mat[1]*X+mat[5]*Y;
	float x = (X-win_w/2.0);
	float y = -(Y-win_h/2.0);
	cout << "Cw: " << x << " " << y << endl;
	x = mat[0]*(X-win_w/2.0)/win_w*100.0-mat[4]*(Y-win_h/2.0)/win_h*100.0;
	y = mat[1]*(X-win_w/2.0)/win_w*100.0-mat[5]*(Y-win_h/2.0)/win_h*100.0;
	cout << "Cv: " << x << " " << y << endl;
	
	
	for(int yi = 0; yi < 4; yi++) { for(int xi = 0; xi < 4; xi++) {printf("%3.2f\t",mat[yi*4+xi]);}cout << endl;}cout << endl;

	
	float px = (x-win_w/2.0)/win_w*100.0;
	float py = -(y-win_h/2.0)/win_h*100.0;
	
	//LOG_DEBUG("--------------");
	//LOG_DEBUG("x: %f, y:%f",x,y);
	//LOG_DEBUG("px: %f, py:%f",px,py);
	if(asp > 1) 	//Apaisado
		px *= asp;
	else
		py /=asp;
		
	px+=-pos_x+tam_w/2;
	py+=-pos_y+tam_h/2;
	
	//LOG_DEBUG("px: %f, py:%f",px,py);
	
	if(	px > 0 && px < tam_w && 
		py > 0 && py < tam_h)
	{
		return true;
	}
	else
		return false;
}
*/

void UI::Element::Draw()
{
	GLfloat vertices[] = {
		-0.5,-0.5, -1,
		-0.5, 0.5, -1,	
		 0.5, 0.5, -1,
		 0.5,-0.5, -1
	}, tmpX, tmpY, tmpW, tmpH;
	
	glPushMatrix();
	glTranslatef(pos_x,pos_y,0);
	glRotatef(degree, 0,0,1);
	glScalef(tam_w,tam_h,1);

	for(int i = 0; i < v_capa.size(); i++)
	{
		if(v_capa[i].isActivo() && v_capa[i].getVisible())
		{
			v_capa[i].getPos(tmpX,tmpY);
			v_capa[i].getSize(tmpW,tmpH);
			glPushMatrix();
			glTranslatef(tmpX,tmpY,0);
			glScalef(tmpW,tmpH,1);

			// TODO is this necessary? glActiveTexture(GL_TEXTURE0);
			v_capa[i].BindTexture();
			
			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);
			
			glVertexPointer(3, GL_FLOAT, 0, vertices);
			v_capa[i].TexCoordPointer();

			glDrawArrays ( GL_TRIANGLE_FAN, 0, 4 );
			
			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);

			glPopMatrix();
		}
		
	}
	if(test_box){
		glDisable(GL_TEXTURE_2D);
		glEnableClientState(GL_VERTEX_ARRAY);
		
		glVertexPointer(3, GL_FLOAT, 0, vertices);
		glDrawArrays ( GL_LINE_LOOP, 0, 4 );
		
		glDisableClientState(GL_VERTEX_ARRAY);
		
		glEnable(GL_TEXTURE_2D);
	}
	
	glPopMatrix();

}

void UI::Element::onStep( GLfloat dt){
	GLuint i, size = v_capa.size();
	for(i = 0; i < size; i++)
		if(v_capa[i].isActivo())
			v_capa[i].onStep( dt);
}

void UI::Element::reset(){
	GLuint i, size = v_capa.size();
	for(i = 0; i < size; i++)
		if(v_capa[i].isActivo())
			v_capa[i].reset();
}

void UI::Element::convertPixelToLogic(float pix_x, float pix_y, float &log_x, float &log_y) {
	//Coordenadas pixel -> lógicas 
	log_x = pix_x * 100.0f / win_w - 50; //[0 , win_w] -> [-50 , 50]
	log_y = -(pix_y * 100.0f / win_h - 50); //[0 , win_h] -> [-50 , 50]

	if (asp > 1)    //Apaisado
		log_x *= asp;
	else
		log_y /= asp;

}
void UI::Element::convertLogicToPixel(float log_x, float log_y, float &pix_x, float &pix_y){
	//Coordenadas lógicas ->  pixel
	if(asp > 1) 	//Apaisado
		log_x /= asp;
	else
		log_y *=asp;
	
	pix_x= (log_x+50)/100.0*win_w; //[-50 , 50] -> [0 , win_w]
	pix_y=-(log_y+50)/100.0*win_h; //[-50 , 50] -> [0 , win_h]
}
};
