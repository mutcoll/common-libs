#include "Button.h"

namespace randomize { namespace UI { 

Button::Button():pulsado(false)
{
	f1[0]=0;
	f1[1]=0;

	f2[0]=0;
	f2[1]=0;
}

Button::Button(Conf conf):pulsado(false) {
	setConfig(conf);
}

Button::Conf::Conf(GLuint texture,
					int frames_X,
					int frames_Y,
					int frames_reposo_x,
					int frames_reposo_y,
					int frames_activo_x,
					int frames_activo_y,
					int tam_W,
					int tam_H): tex_0(texture),
			                    frames_x(frames_X),
			                    frames_y(frames_Y),
			                    tam_w(tam_W),
			                    tam_h(tam_H)
{
	f1[0] = frames_reposo_x;
	f1[1] = frames_reposo_y;
	f2[0] = frames_activo_x;
	f2[1] = frames_activo_y;
}

Button * Button::setConfig(Button::Conf conf)
{
	addCapa(0,conf.tex_0,conf.frames_x,conf.frames_y);
	f1[0]=conf.f1[0];
	f1[1]=conf.f1[1];

	f2[0]=conf.f2[0];
	f2[1]=conf.f2[1];
	setTam(conf.tam_w,conf.tam_h);

	setCapa(0,f1[0],f1[1]);
	return this;
}


Button * Button::setButtonFrames(int frame_rest_x, int frame_rest_y, int frame_active_x, int frame_active_y)
{
	f1[0]= frame_rest_x;
	f1[1]= frame_rest_y;

	f2[0]= frame_active_x;
	f2[1]= frame_active_y;

	if (pulsado) {
		setCapa(0, f2[0], f2[1]);
	} else {
		setCapa(0, f1[0], f1[1]);
	}
	return this;
}

int Button::CB_Click(Event action, int x, int y)
{
	Element::CB_Click(action, x, y);
	if( (pulsado = (action == UI_EVENT_DOWN || ( action == UI_EVENT_MOVE && pulsado ) ) ) )
		v_capa[0].setFrame(f2[0],f2[1]);
	else
		v_capa[0].setFrame(f1[0],f1[1]);

	return 0;
}
int Button::CB_NoClick()
{
	v_capa[0].setFrame(f1[0],f1[1]);
	pulsado = false;

	return 0;
}

}};
