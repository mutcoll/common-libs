#ifndef _UI_SLIDER_H_
#define _UI_SLIDER_H_

#include "Button.h"

namespace randomize { namespace UI {
class Slider : public Button
{
	private:
		int x_ant, y_ant;
		bool free_x, free_y;
		float min_x, min_y;
		float max_x, max_y;
	public:
		
		Slider(): Button(),free_x(true),free_y(true),min_x(-50), min_y(-50),max_x(50), max_y(50){}
		
		void setFree(bool x, bool y){free_x=x; free_y=y;}
		void setMax(float x, float y){max_x = x; max_y = y;}
		void setMin(float x, float y){min_x = x; min_y = y;}
		
		float getX(){return (pos_x-min_x)/(max_x-min_x);}
		float getY(){return (pos_y-min_y)/(max_y-min_y);}
		
		int CB_Click(UI::Event action, int x, int y)
		{
			UI::Element::CB_Click(action, x, y);
			if(!pulsado)
			{
				x_ant = x;
				y_ant = y;
			}
			Button::CB_Click(action, x, y);
			
			addPosPixel(free_x?x-x_ant:0, free_y?y-y_ant:0);

			x_ant = x;
			y_ant = y;

			
			if(pos_x > max_x)
				pos_x = max_x;
			else if(pos_x < min_x)
				pos_x = min_x;
			
			if(pos_y > max_y)
				pos_y = max_y;
			else if(pos_y < min_y)
				pos_y = min_y;
			
			return 0;
		}

		bool dentro(int x, int y)
		{
			if(pulsado)
				return true;
			else
				return Button::inside(x, y);
		}
};


}}; //namespace UI

#endif //_UI_SLIDER_H_
