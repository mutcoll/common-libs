#ifndef _UI_ELEMENT_H_
#define _UI_ELEMENT_H_

#include "randomize/platform/port.h"
#include <cmath>

#ifdef ANDROID
#include <GLES/gl.h>
#include <EGL/egl.h>
#else
#include <SDL_opengl.h>
//#ifdef WIN32
//#define WIN32_LEAN_AND_MEAN
//#include <Windows.h>
//#endif
//#include <GL/gl.h>	// Header File For The OpenGL32 Library
#endif

#include <vector>
#include <string>
#include <functional>
#include "randomize/graphics/SpriteIFL.h"
#include "UI.h"
#include "randomize/log/log.h"



//#define UI_EVENT_UP AMOTION_EVENT_ACTION_UP
//#define UI_EVENT_DOWN AMOTION_EVENT_ACTION_DOWN
//#define UI_EVENT_MOVE AMOTION_EVENT_ACTION_MOVE

namespace randomize { namespace UI {


/**
 * 		********************
 * 		Definición del elemento UI		
 * 		********************
 * 
 * 		-El elemento se define con el siguiente sistema de coordenadas:
 * 				-(0,0) en el centro de la pantalla
 * 				-El lado menor (dependiendo de orientación) vale 100u
 * 			
 * 	+50 -----------------------------  ^
 * 		|                           |  |
 * 		|                           |  |
 * 		|                           |  |
 * 		|                           |  |  100u
 * 		|                           |  |
 * 		|                           |  |
 * 		|                           |  |
 * 		|                           |  |
 * 	-50 -----------------------------  v
 * 
 * 		********************
 * 		Elemento por capas de sprites
 * 		********************
 * 
 * 		-Los elementos tienen diferentes capas que se superponen.
 * 		-Las capas se comportan como sprites, con lo que la textura se
 *		 divide como una matriz de frames
 * 
 * 		********************
 * 		El arbol de elementos
 * 		********************
 * 
 * 		-Los paneles pueden tener asociados otros elementos "hijo"
 * 		-La posición del hijo es relativa a la del padre
 * 			Es decir, el centro de coordenadas de un elemento hijo será el 
 * 		 	centro del panel padre.
 * 		-El tamaño del hijo es relativo 
 * 
 * 		-La función "CB_Click" retorna el identificador del hijo en el que se ha pulsado
 * 		
 * 
 * */

//template <int NUM_MAX_TEX_EN_MENU>
class Element
{
	public:
	/*
	 * Configuración del elemento
	 */
		
		Element();
		/**
		 * Carga una lista de Sprites de un archivo .sfl (Sprite File List)
		 * */
		void loadFromSFL( const char *path);
		/**
		 * Posición respecto al centro del panel
		 * */
		virtual void setPos(float x, float y){pos_x=x;pos_y=y;}
		
		/**
		 * Añade a la posición, es decir, mueve
		 * */
		void addPos(float x, float y){pos_x+=x;pos_y+=y;}
		
		/**
		 * Posición en pixeles absolutos
		 * */
		 void setPosPixel(int x, int y);
		
		/**
		 * Añade a la posición pixeles absolutos
		 * */
		 void addPosPixel(int x, int y);
		
		/**
		 * Tamaño del elemento en unidades relativas
		 * */
		virtual void setTam(float w, float h){tam_w=w;tam_h=h;}

		/**
		 * Tamaño del elemento en unidades relativas
		 * */
		virtual void getTam(float &w, float &h){w=tam_w;h=tam_h;}

		/**
		 * Tamaño del elemento en pixeles absolutos
		 * */
		virtual void setTamPixel(float w, float h);

		/**
		 * Rotación del panel sobre el plano
		 * */
		void setRotation(float deg){degree = deg;}
		
		/**
		 * Coloca según configuración predeterminada
		 * El Layout hay que recolocarlo cuando se cambia el tamaño de pantalla
		 * */
		void setLayout(Layout lay);
		
		void setId(int id_p){id = id_p;}
		
		int getId(){return id;}

	/*
	 * Configuración de las capas del elemento
	 */
		/**
		 * Establece la imagen que muestra una capa seleccionada
		 * */
		void setCapa(unsigned int num_capa, int x, int y);
		/**
		 * Establece la imagen que muestra una capa seleccionada
		 * */
		void setCapa(unsigned int num_capa, int n);
		/**
		 * Avanza la imagen que muestra una capa seleccionada
		 * */
		void advanceFrameCapa(unsigned int num_capa, int n);
		/**
		 * Establece qué la capa "num_capa" esta activas o no en cada momento
		 * */
		void setCapaActivo(unsigned int num_capa, bool activo);
		/**
		 * Establece qué la capa "num_capa" es colisionable o no en cada momento
		 * */
		void setCapaColisionable(unsigned int num_capa, bool colisionable);
		
		/**
		 * Añade o modifica una capa. Hay que especificar:
		 * 
		 * @param int num_capa : Numero de capa
		 * @param GLuint tex : Identificador de la textura
		 * 
		 * */
		void addCapa(int num_capa, GLuint tex, int num_x, int num_y);
		/**
		 * Añade o modifica una capa. Hay que especificar:
		 * 
		 * @param int num_capa : Numero de capa
		 * @param const char *path : archivo .ifl a cargar
		 * 
		 * */
		void addCapa(int num_capa, const char *path);
		/**
		 * Añade o modifica una capa. Hay que especificar:
		 * 
		 * @param int num_capa : Numero de capa
		 * @param std::ifstream &file : stream con la posicion de lectura al principio de la descripcion insertada de un .ifl
		 * 
		 * */
		void addCapa(int num_capa, std::istream &file);
	
	/*
	 * Metodos de funcionamiento
	 */
		/**
		 * Indica si una pulsación está dentro o no del elemento
		 * */
		virtual bool inside(int x, int y);
		
		/**
		 * Establece como tiene que comportarse el elemento al ser pulsado
		 * */
		virtual int CB_Click(UI::Event action, int x, int y);
		
		/**
		 * Establece como tiene que comportarse el elemento al no ser pulsado
		 * */
		virtual int CB_NoClick(){return 0;}
	
//		/**
//		 * Añade un listener que será llamado cuando llegue un evento sobre el elemento.
//		 * */
//		void setListener(void (*listener)(UI::Event action, int x, int y, Element* this_element, void* user_data), void* user_data) {
//            setListener(std::function<void (UI::Event, int, int, Element*, void*)>(listener), user_data);
//        }
		/**
		 * Añade un listener que será llamado cuando llegue un evento sobre el elemento.
		 * */
		void setListener(std::function<void (UI::Event action, int x, int y, Element* this_element, void* user_data)> listener, void* user_data) {
			this->listener = listener;
			this->user_data = user_data;
		}
	/*
	 * Metodo de dibujado
	 */
		virtual void Draw();

		virtual void onStep( GLfloat dt);

		virtual void reset();

		
	protected:

		void convertPixelToLogic(float pix_x, float pix_y, float &log_x, float &log_y);
		void convertLogicToPixel(float log_x, float log_y, float &pix_x, float &pix_y);

		class Capa : public SpriteIFL
		{
			private:
				bool activo,colisionable;
			public:
				Capa():activo(false),colisionable(true){}
				Capa(GLuint tex, int f_w, int f_h):activo(true),colisionable(true){this->buildSpriteLegacy(tex,f_w,f_h);}
				Capa( const char *path):activo(true),colisionable(true){this->loadSpriteIFL( path);}
				Capa( std::istream &file):activo(true),colisionable(true){this->loadSpriteIFL( file);}
				void setActivo(bool a){activo = a;}
				bool isActivo(){return activo;}
				void setColisionable(bool a){this->colisionable = a;}
				bool isColisionable(){return this->colisionable;}
		};
		std::vector<Capa> v_capa;
	
		float pos_x, pos_y;
		float tam_h, tam_w;
		float degree;

		Layout layout;

		std::function<void (UI::Event action, int x, int y, Element* this_element, void* user_data)> listener = nullptr;
		void *user_data;

		int id;	//Identificador del Element. 
		
		//double mat[16];
		//double imat[16];
		//bool imat_ok = false;
		
		
	
	

};

}}; //namespace UI

#endif
