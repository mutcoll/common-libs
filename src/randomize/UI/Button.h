#ifndef _UI_BOTON_H_
#define _UI_BOTON_H_

#include "Element.h"

namespace randomize { namespace UI {


class Button : public Element
{
	protected:
		bool pulsado;
		int f1[2];	//Reposo
		int f2[2];	//Activo
	public:
		
		struct Conf {
			Conf(){}
			Conf(GLuint texture, 
					int frames_X, 
					int frames_Y, 
					int frames_reposo_x, 
					int frames_reposo_y,  
					int frames_activo_x, 
					int frames_activo_y, 
					int tam_W, 
					int tam_H);
			
			GLuint tex_0;	//Textura de fondo
			int frames_x;		//Frames a lo ancho
			int frames_y;		//Frames a lo largo
			
			int f1[2];	//Frames Reposo
			int f2[2];	//Frames Activo
			int tam_w;	//Tamaño
			int tam_h;
		};

		
		Button();
		Button(Conf conf);

		Button * setConfig(Conf conf);
		
		Button * setButtonFrames(int frame_rest_x, int frame_rest_y, int frame_active_x, int frame_active_y);
		
		int CB_Click(UI::Event action, int x, int y);
		
		int CB_NoClick();
};




};}; //namespace UI


#endif


/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
//////////////// BASURERO DE IDEAS INFERTILES ///////////////////
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
/*
class Boton_Text : public Button, public UI_Text
{
	private:

	public:
		Boton_Text():Button()
		{
			UI_Text::setTamAuto(true);
		}
		void setPos(float x, float y)
		{
			Button::setPos(x,y);
			UI_Text::setPos(x-Button::tam_h*0.2,y);
		}
		void setTam(float w, float h)
		{
			Button::setTam(w,h);
			UI_Text::setTamLetra(h*0.45);
		}
		
		void Draw()
		{
			Button::Draw();
			UI_Text::Draw();
		}

};
*/

