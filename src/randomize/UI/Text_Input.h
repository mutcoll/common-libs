#ifndef _UI_TEXT_INPUT_H_
#define _UI_TEXT_INPUT_H_

#include "Element.h"
#include "Button.h"
#include "Text.h"
//#include "Teclado.h"

namespace randomize{ namespace UI{

class Text_Input : public Text
{

	private:
		int len_letra;
		//int len_frase;
		int max_len;
	public:
		Text_Input():Text(),len_letra(0),/*len_frase(0),*/max_len(MAX_TAM_TEXTO)
		{
			memset(frase,' ',MAX_TAM_TEXTO);
			setFrase(frase);
		}

		void setMaxLength(unsigned int max)
		{
			max_len = max;
			frase[max_len] = 0;
			setFrase(frase);
		}

		//void addChar(UI_Teclado &t)
		//{
			//char c = t.getChar();
			//addChar(c);
		//}
		void addChar(unsigned char c)
		{
			switch(c)
			{
				case SpriteChar::Keycode::BACK:
					if(len_frase <= 0)
						break;
					len_frase--;
					len_letra--;
					frase[len_frase] = ' ';
					v_letra[len_letra].setChar(' ');
					if(len_frase < 0) {
						if(frase[len_frase-1] == '~' || frase[len_frase-1] == '\'') {
							len_frase--;
							frase[len_frase] = ' ';
						}
                    }
					break;
				case SpriteChar::Keycode::ENTER:

					break;
				case SpriteChar::Keycode::SHIFT:

					break;
				case SpriteChar::Keycode::ENYE_MINUS:
					//if(len_frase >= max_len)
						//break;
					//frase[len_frase] = '~';
					//frase[len_frase+1] = 'n';
					//v_letra[len_letra].setChar('~');
					//v_letra[len_letra].setChar('n');
					//len_frase +=2;
					//len_letra++;
					//max_len++;

					//break;
				case SpriteChar::Keycode::ENYE_MAYUS:
					//if(len_frase >= max_len)
						//break;
					//frase[len_frase] = '~';
					//frase[len_frase+1] = 'N';
					//v_letra[len_letra].setChar('~');
					//v_letra[len_letra].setChar('N');
					//len_frase +=2;
					//len_letra++;
					//max_len++;


					//break
				default:
					//if(c > 127)
					//{
						//Log::info("Sobrepasa %d" , c);
						//break;
					//}
					if(len_frase >= max_len)
						break;
					frase[len_frase] = c;
					if(!v_letra[len_letra].setChar(c)){
                        len_frase++;
                        len_letra++;
					}
//					printf("len_frase: %d\n", len_frase);
//					printf("frase: %s\n", frase);
					//setFrase(frase);

			}
			_setTam();
		}
		/*
		void addChar(char c)
		{
			if(len_frase < max_len)
			{
				frase[len_frase] = c;
				v_letra[len_letra].setChar(c);
				len_frase++;
				len_letra++;
				//_setTam();
			}
		}
		*/

		//void addChar(char *c){}



};
}};//namespace UI

#endif
