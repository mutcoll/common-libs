
#include "UI.h"

//Variables private del Namespace
namespace randomize { namespace UI{

	int win_h;
	int win_w;
	float asp;

	bool test_box = false;

	bool drawing = false;

	int blend_src;
	int blend_dst;
	bool depth_test;
	bool blend;
	bool texture_2d;

}};

using namespace randomize;

/*
 * Configuración de la ventana
 */
/**
 * Establece la configuración de ventana
 * */

void UI::setConfWindow(int w, int h){
	win_w=w;
	win_h=h;
	asp = float(w)/float(h);
}

void UI::setTestBox(bool test){
	test_box = test;
}

int UI::getWinWidth(){
	return win_w;
}
int UI::getWinHeight(){
	return win_h;
}
/*
 * Metodos de dibujado
 */

/**
 * @return Bool : Cierto si ha configurado el estado. False si ya estaba configurado.
 * */
bool UI::BeginDraw(){
	if(drawing)
		return false;
	drawing = true;

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();

	//Configuración ortoedrica	100u en el eje menor
	if(asp > 1)
		glOrtho(-50*asp,50*asp,-50,50,50,-50);
	else
		glOrtho(-50,50,-50/asp,50/asp,50,-50);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();


	//Guardar el estado previo
	depth_test 	= glIsEnabled(GL_DEPTH_TEST);
	blend 		= glIsEnabled(GL_BLEND);
	texture_2d 	= glIsEnabled(GL_TEXTURE_2D);
	glGetIntegerv(GL_BLEND_SRC, &blend_src);
	glGetIntegerv(GL_BLEND_DST, &blend_dst);

	//Establecer un estado apropiado
	glDisable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_TEXTURE_2D);

	glColor4f(1,1,1,1);
	//Todo configurado
	return true;
}
void UI::EndDraw(){

	//Restaurando estado
	if(depth_test){
		glEnable(GL_DEPTH_TEST);
	}
	if(!blend){
		glDisable(GL_BLEND);
	}
	glBlendFunc(blend_src, blend_dst);

	if(!texture_2d){
		glDisable(GL_TEXTURE_2D);
	}

	//Restaurando matrices
	glPopMatrix();
	glMatrixMode(GL_PROJECTION);
	glPopMatrix();
	glMatrixMode(GL_MODELVIEW);
	//Todo restaurado!


	drawing = false;
}



