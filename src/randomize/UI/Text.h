#ifndef _UI_TEXT_H_
#define _UI_TEXT_H_
#include "randomize/graphics/SpriteChar.h"
#include "Element.h"
#include "Button.h"
#include <stdio.h>
#include <stdarg.h>

namespace randomize { namespace UI{

#define MAX_TAM_TEXTO 50
class Text : public Element
{
	protected:
		char frase[MAX_TAM_TEXTO];
		int len_frase;
		float tam_letra;
		float separacion;
		bool tam_auto;
		GLfloat color[4];
		SpriteChar v_letra[MAX_TAM_TEXTO];
		void _setTam(){if(tam_auto){setTam(len_frase*separacion*tam_letra,tam_letra);}}
		void _setFrase()
		{
			int len = strnlen(frase, MAX_TAM_TEXTO);
			int i;
			len_frase = 0;
			for(i = 0; i < len ; i++)
			{
				if(!v_letra[len_frase].setChar(frase[i]))
					len_frase++;
			}
			_setTam();
		}
	public:

		Text():Element(),len_frase(0),tam_letra(10),separacion(0.6),tam_auto(false)
		{
			memset(frase, 0, MAX_TAM_TEXTO);
			color[0]=1;
			color[1]=1;
			color[2]=1;
			color[3]=1;
		}

		//Establece el tamaño de los caracteres.
		void setTamLetra(float t){tam_letra = t;_setTam();}
		//Modifica el tamaño del elemento en función del contenido y tamaño de letra
		void setTamAuto(bool b){tam_auto=b;_setTam();}
		//Establece la separación entre caracteres.
		void setSeparacion(float s){separacion = s; _setTam();}
		//Establece el color de las letras
		void setColor3f(GLfloat r, GLfloat g, GLfloat b){color[0]=r;color[1]=g;color[2]=b;color[3]=1;}

		char* getFrase()
		{
			return frase;
		}
		//Frase a mostrar.
		void setFrase(const char* f, ...)
		{
			va_list arg;
			va_start (arg, f);
			vsnprintf(frase,MAX_TAM_TEXTO,f, arg);
			va_end(arg);
			_setFrase();
		}

		bool inside(int x, int y){return false;}
		void Draw()
		{
			Element::Draw();
			GLfloat vertices[] = {
				-0.5,-0.5, -1,
				-0.5, 0.5, -1,
				 0.5, 0.5, -1,
				 0.5,-0.5, -1
			};
			glPushMatrix();

			glTranslatef(pos_x,pos_y,0);
			glScalef(tam_w,tam_h,1);

			GLfloat env_color[] = {1,1,1,1};
			glTexEnvfv(GL_TEXTURE_ENV,GL_TEXTURE_ENV_COLOR, env_color);
			glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_BLEND);
			glColor4f(color[0],color[1],color[2],color[3]);

			glScalef(1.0/(len_frase*separacion),1,1);
			glTranslatef(-((1+len_frase)*separacion)/2,0,0);

			for(int i = 0; i < len_frase; i++)
			{
				glTranslatef(separacion,0,0);

				// TODO is this needed? glActiveTexture( GL_TEXTURE0);
				v_letra[i].BindTexture();

				glEnableClientState(GL_VERTEX_ARRAY);
				glEnableClientState(GL_TEXTURE_COORD_ARRAY);

				glVertexPointer(3, GL_FLOAT, 0, vertices);
				v_letra[i].TexCoordPointer();

				glDrawArrays ( GL_TRIANGLE_FAN, 0, 4 );

				glDisableClientState(GL_VERTEX_ARRAY);
				glDisableClientState(GL_TEXTURE_COORD_ARRAY);

			}
			glTexEnvi(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE,GL_MODULATE);
			glColor4f(1,1,1,1);

			glPopMatrix();
		}

};
class Boton_Text : public Button
{
	private:
		Text frase;

	public:
		Boton_Text(): Button(),frase()
		{
			frase.setTamAuto(true);
		}
		void setPos(float x, float y)
		{
			Button::setPos(x,y);
			frase.setPos(x-tam_h*0.2,y);
		}
		void setTam(float w, float h)
		{
			Button::setTam(w,h);
			frase.setTamLetra(h*0.45);
		}
		void setTamLetra(float f)
		{
			frase.setTamLetra(f);
		}
		void setSeparacion(float f)
		{
			frase.setSeparacion(f);
		}
		void setFrase(const char* c)
		{
			frase.setFrase(c);
		}
		void Draw()
		{
			Button::Draw();
			frase.Draw();
		}
};




//TODO Falta Testear
class Marcador : public Text
{
	#define BASE 10
	public:
		void setInt(int n)
		{
			bool neg = false;
			if(n < 0)
			{
				neg = true;
				n = -n;
			}
			int len = 0;
			//int d = 1;
			char c[30];
			while( n > 0)
			{
				c[len] = (n % BASE) + '0';
				n /= BASE;
				len++;
			}
			char cc[30];
			int f = len;
			int i = 0;
			if(len == 0)
			{
				cc[i] = '0';
				len++;
				i++;
			}
			else if(neg)
			{
				cc[i] = '-';
				i++;
				len++;
			}
			for(; i < len; i ++)
			{
				f--;
				cc[i] = c[f];
			}
			cc[i] = 0;
			setFrase(cc);
		}
		void setFloat(float aFloat)
		{
			setFrase( "%f", aFloat);
		}

	#undef BASE
};

}};//namespace UI

#endif
