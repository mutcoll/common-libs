#ifndef _UI_H_
#define _UI_H_

#ifdef ANDROID
#include <GLES/gl.h>
#include <EGL/egl.h>
#else
#ifdef WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif
#include <GL/gl.h>	// Header File For The OpenGL32 Library
#endif

#include <cstdio>

/*****
 * FIXME:
 * 
 * 		UI::Panel::CB_Click
 * 			Problema con los paneles anidados!
 * 
 * TODO:
 * 
 * Replantearse CB_Click? (nombre y parámetros)
 * 		onClick(event, x, y, &identificador)	-> Identificador solo util en Panel, poner esto solo en Panel?
 * 
 * forcePixelSize
 * 		El tamaño de los objetos viene dado en píxeles, no en coordenadas de UI, y debe ser respetado en escalados de pantalla
 * 	    OPT 1: 
 * 			Cuando se cambie el tamaño de la pantalla se debe cambiar tambien el de estos elementos
 * 			Quién los cambia?
 * 	   *OPT 2: 
 * 			Al dibujar los elementos, if(bool) se debe tener en cuenta el tamaño en píxeles, no en coordenadas UI
 * 			Obliga pasar de pixeles a coordenadas en cada dibujado. O no, guardando win_h  win_w y comparando con el global...
 * 		OPT 3:
 * 			Poner un recordatorio de que, al cambiar de tamaño de pantalla, hay que resetear los tamaños en píxeles.
 * 	   *OPT 4: Ponerlo solo en el Panel, y que sea "recursivo"
 * 		Nombres alternativos: 
 * 			usePixelSize 
 * 
 * windowsUpdate
 * 		Booleano que indica que el tamaño de la ventana ha cambiado.
 * 	   *OPT 1:
 * 			Miembro en cada Element.
 * 			Quién lo pone a True? (Panel?)
 * 		OPT 2:
 * 			Static del namespace UI
 * 			Cuánto tiempo estaría a true?
 * 
 * Privatizar elementos
 * 		private Element::Draw --> Los Element (y descendientes) no pueden ser dibujados directamente.
 * 		private Element::CB_Click --> Los Element (y descendientes) no pueden ser clicados directamente.
 * 		
 * 		Permitiría generar diferentes versiones si "Panel" actua como controlador para diferentes plataformas
 * 		Traducción a eventos de UI
 * 		SDL_Panel
 * 		GLUT_Panel
 * 		ANDROID_Panel
 * 
 * 		Pros:
 * 			Supercontrolador de los elementos.
 * 		Contras:
 * 			Encapsulación
 * 			Overhead
 * 
 * ***/

namespace randomize { namespace UI {
	
enum Event {
				UI_EVENT_UP,
				UI_EVENT_DOWN,
				UI_EVENT_MOVE
				};

enum Layout {	
				LAYOUT_CENTER, 
				LAYOUT_FULL_SCREEN, 
				LAYOUT_HALF_SCREEN_DOWN,
				LAYOUT_HALF_SCREEN_UP,
				LAYOUT_FREE,
				};
	
/*
 * Configuración de la ventana
 */	
/**
 * Establece la configuración de ventana
 * */
	void setConfWindow(int w, int h);
	
	void setTestBox(bool test);
	
	
	int getWinWidth();
	int getWinHeight();
/*
 * Metodos de dibujado
 */
	
	bool BeginDraw();
	void EndDraw();
	
	 
}};


#endif //_UI_H_
