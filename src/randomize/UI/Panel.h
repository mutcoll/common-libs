#ifndef _UI_PANEL_H_
#define _UI_PANEL_H_

//#define NUM_MAX_ELEM_PANEL 35
#include "randomize/platform/port.h"
#include <cmath>

#include "UI.h"
#include "Element.h"
#include "randomize/log/log.h"

namespace randomize { namespace UI {

/**
 * TODO: Al hacer: setPos, setTam, etc... que varíe para todos los hijos.
 * 					Sobretodo para funciones de mover y crecer
 * */

template <int NUM_MAX_ELEM_PANEL>
class Panel : public Element
{

	private:
		Element* v_elem	[NUM_MAX_ELEM_PANEL];
		bool v_activo		[NUM_MAX_ELEM_PANEL];
		unsigned int v_identificador	[NUM_MAX_ELEM_PANEL];
		int num_elem;

	public:
	/*
	 * Interactuación con el panel
	 */
		
		Panel():Element(),num_elem(0){
			tam_w = 100;
			tam_h = 100;
		}
	 
		//Necesario para reconstruir el arbol de elementos
		void Limpiar(){num_elem=0;}
		/**
		 * Si las coordenadas pixel coinciden con algún hijo, devuelve
		 * el identificador de este. 
		 * En caso de coincidir con más de un hijo, solo pulsa el más
		 * cercano.
		 * 
		 * @return int: 
		 * 			>=0: Identificador de hijo
		 * 			-1 : Ningún hijo, dentro del panel
		 * 			-2 : Fuera del panel
		 * */
		int CB_Click(UI::Event action, int X, int Y)
		{

			int i;
			int r;
			bool fin = false;
			
			float aux;
			float asp = float(getWinWidth())/float(getWinHeight());
			
			//Coordenadas pixel dextrógiras centradas
			float x =   X - getWinWidth()/2.0; //[0 , getWinWidth()] -> [-getWinWidth()/2 , getWinWidth()/2]
			float y = (-Y)+ getWinHeight()/2.0; //[0 , getWinHeight()] -> [-getWinHeight()/2 , getWinHeight()/2]

			//Deshacer traslacion
			x -= pos_x/(100.0*((asp>1)?asp:1))*getWinWidth();
			y -= pos_y/(100.0/((asp>1)?1:asp))*getWinHeight();
			
			//Deshacer rotacion
			float cos_, sin_;
			sincosf( degree*M_PI/180, &sin_, &cos_);
			aux =  cos_*x + sin_*y;
			y   = -sin_*x + cos_*y;
			x = aux;
			
			//Deshacer escalado
			x/=tam_w/100.0;
			y/=tam_h/100.0;
			
			//Coordenadas pixel levógiras con el centro en la esquina superior izquierda
			x=  x + getWinWidth()/2.0; //[-getWinWidth()/2 , getWinWidth()/2] -> [0 , getWinWidth()]
			y= -y + getWinHeight()/2.0; //[-getWinHeight()/2 , getWinHeight()/2] -> [0 , getWinHeight()]
			
			
			for( i = num_elem-1; i >= 0; i--){
				if(v_activo[i]){
					if(!fin && v_elem[i]->inside(x, y)){
						v_elem[i]->CB_Click(action, x, y);	
						//FIXME : Si el elemento era un panel, se quiere saber qué devuelve el panel, no el identificador de panel!!
						r = i;
						fin = true;
					} else {
						v_elem[i]->CB_NoClick();
					}
				}
			}
			if(fin){
				return v_identificador[r];
			}
			
			if(inside(x, y)){
				return -1;
			} else { 
				return -2;
			}
		}
		
		/**
		 * Cuelga un vector de paneles de este panel
		 * 
		 * @return Identificador del elemento
		 * */
		int addElemento(Element* elem, unsigned int id)
		{
			if(num_elem < NUM_MAX_ELEM_PANEL) {
				v_elem[num_elem] = elem; 
				v_activo[num_elem] = true;
				v_identificador[num_elem] = id;
				num_elem++;
				return num_elem -1;
			} else {
				LOG_ERROR("ERROR: Se intentan añadir más elementos de los que se pueden a un panel!");
				return -1;
			}
		} 
		
		void setActivo(int n, bool act)
		{
			if(n < num_elem)
				v_activo[n] = act;
		}
		
		void Draw()
		{	
			bool configurado = UI::BeginDraw();
			Element::Draw();
			glPushMatrix();
			
			glTranslatef(pos_x,pos_y,0);
			glRotatef(degree, 0,0,1);
			glScalef(tam_w/100.0,tam_h/100.0,1);

			//if(v_elem != NULL)
				for(int i = 0; i < num_elem; i++)
					if(v_activo[i])
						v_elem[i]->Draw();

			glPopMatrix();
			if(configurado){
				UI::EndDraw();
			}
		}
	
		
		
};

}}; //namespace UI

#endif //_UI_PANEL_H_
