//
// Created by jacobo on 28/06/15.
//

#ifndef RANDOMIZE_BOTON_H
#define RANDOMIZE_BOTON_H

#include "Button.h"


namespace randomize { namespace UI {


class [[deprecated("Use \"Button\" instead")]] Boton  : public Button {
  public:

    [[deprecated("Use \"setButtonFrames\" instead")]]
    void setBotones(int f1x, int f1y, int f2x, int f2y) {
        setButtonFrames(f1x, f1y, f2x, f2y);
    }
};
};};

#endif //RANDOMIZE_BOTON_H
