#ifndef _UI_PULSADOR_H_
#define _UI_PULSADOR_H_

#include "Button.h"

namespace randomize { namespace UI {


class Switch : public Button
{
	private:
		bool status;
	
	public:
		
		Switch(): status(false){}
		
		bool getStatus(){
			return status;
		}

		int setStatus(bool p) {
			status = p;
			if(status)
				v_capa[0].setFrame(f2[0],f2[1]);
			else
				v_capa[0].setFrame(f1[0],f1[1]);
			return 0;
		}

		int CB_Click(UI::Event action, int x, int y)
		{
			UI::Element::CB_Click(action, x, y);
			if(UI_EVENT_UP==action)
				status = !status;
			if(status)
				v_capa[0].setFrame(f2[0],f2[1]);
			else
				v_capa[0].setFrame(f1[0],f1[1]);
			return 0;
		}		
		int CB_NoClick()
		{return 0;}
	
};

}}; //namespace UI

#endif //_UI_PULSADOR_H_
