/**
 * @file TestSuite.h
 * @author jmmut 
 * @date 2015-10-10.
 */

#ifndef COMMONLIBS_TESTSUITE_H
#define COMMONLIBS_TESTSUITE_H

#include <vector>
#include <set>
#include <functional>
#include <iostream>
#include <string>
#include <randomize/utils/exception/StackTracedException.h>
#include "randomize/log/log.h"

namespace randomize {

namespace test {

/**
 * to use like this:
```
int main() {
    randomize::test::TestSuite{"test name", {
            []() {
                ASSERT_EQUALS_OR_THROW(1 + 1, 2);
            }, []() {
                ASSERT_EQUALS_OR_THROW(1, 2);
            }
    }};

    return 0;
}
```
 * You can use the name to group unit tests and differentiate TestSuites.
 * To mark a test as failed, throw an exception, which is what the ASSERT_* macros do.
 */
class TestSuite {
public:
    TestSuite(std::string name, std::initializer_list<std::function<void()>>);
    unsigned long countFailed();

private:
    std::set<int> failedTests;
};

#define ASSERT_OR_THROW(expression) ASSERT_OR_THROW_MSG((expression), "")

#define ASSERT_OR_THROW_MSG(expression, message) \
    if (not (expression)) \
        throw randomize::utils::exception::StackTracedException(\
                __FILE__ ":" + std::to_string(__LINE__) + (": assertion ("  #expression  ") failed: ")\
                        + std::to_string(expression) + ". " + (message))

#define ASSERT_EQUALS_OR_THROW(actual, expected) ASSERT_EQUALS_OR_THROW_MSG((actual), (expected), "")

#define ASSERT_EQUALS_OR_THROW_MSG(actual, expected, message) \
    if (not ((actual) == (expected))) \
        throw randomize::utils::exception::StackTracedException( \
                __FILE__ ":" + std::to_string(__LINE__) \
                + (": assertion (" #actual " == " #expected ") failed: ") \
                + std::to_string(actual) + " == " + std::to_string(expected) \
                + ". " + (message))



};  // test
};  // randomize

#endif //COMMONLIBS_TESTSUITE_H
