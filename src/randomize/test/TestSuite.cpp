/**
 * @file TestSuite.cpp
 * @author jmmut 
 * @date 2015-10-10.
 */

#include "TestSuite.h"
#include "randomize/utils/Walkers.h"

namespace randomize {
namespace test {

using std::string;

TestSuite::TestSuite(std::string name, std::initializer_list<std::function<void()>> tests) {
    int i = 0;
    for (auto &func : tests) {
        try {
            func();
        } catch (std::exception &except) {
            LOG_INFO("test [%s][%d]: %s\n", name.c_str(), i, except.what());
            failedTests.insert(i);
        }
        i++;
    }
    LOG_INFO("TestSuite: %lu/%lu [%s] tests failed", failedTests.size(), tests.size(), name.c_str());

    if (not failedTests.empty()) {
        LOG_INFO("failed [%s] tests indices are: %s", name.c_str(),
                randomize::utils::container_to_string_ss(failedTests).c_str());
    }
}

unsigned long TestSuite::countFailed() {
    return failedTests.size();
}

};  // test
};  // randomize

