#include "AutoCallThis.h"

void EventCallThis::autoCallHandler( SDL_Event &event){
	((EventCallThis::wraper*) event.user.data1)->ff();
	delete (EventCallThis::wraper*) event.user.data1;
}

bool EventCallThis::autoCallTester( SDL_Event &event){
	return EventCallThis::eventType == event.type;
}

bool EventCallThis::isActiveAutoCall = false;
int EventCallThis::autoCallEventHandler;

void EventCallThis::requestAutoCallEventHandler( Dispatcher &dispatcher){
	if( !EventCallThis::isActiveAutoCall){
		EventCallThis::autoCallEventHandler = dispatcher.addEventHandler( EventCallThis::autoCallHandler, EventCallThis::autoCallTester);
		EventCallThis::isActiveAutoCall = true;
	}
}



