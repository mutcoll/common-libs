#include "EventCallThis.h"

Uint32 EventCallThis::eventType = (Uint32)-1;

void EventCallThis::pushEvent( std::function< void ()> ff){
	if( EventCallThis::eventType == (Uint32)-1){
		EventCallThis::eventType = SDL_RegisterEvents(1);
	}
	if( EventCallThis::eventType == (Uint32)-1){
		LOG_ERROR( "Error on SDL_RegisterEvents(): %s\n", SDL_GetError());
		exit(1);
	}

	SDL_Event event;
	SDL_memset(&event, 0, sizeof(event)); /* or SDL_zero(event) */
	event.type = EventCallThis::eventType;
	event.user.code = 0;
	event.user.data1 = (void*) new EventCallThis::wraper( ff);
	event.user.data2 = nullptr;
	SDL_PushEvent(&event);
}

