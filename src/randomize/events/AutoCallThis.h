#ifndef _AutoCallThis_H_
#define _AutoCallThis_H_

#include "Dispatcher.h"
#include "EventCallThis.h"

namespace EventCallThis{
	void autoCallHandler( SDL_Event &event);
	bool autoCallTester( SDL_Event &event);

	extern bool isActiveAutoCall;
	extern int autoCallEventHandler;
	void requestAutoCallEventHandler( Dispatcher &dispatcher);
};

#endif//_EventCallThis_H_

