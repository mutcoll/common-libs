#ifndef _Dispatcher_H_
#define _Dispatcher_H_

#include <stdlib.h>
#include <vector>
#include <functional>
#include <memory>
#include "SDL.h"
#include "randomize/log/log.h"

class Dispatcher{
public:
	Dispatcher();
	~Dispatcher();
	int addEventHandler( std::function< void (SDL_Event&)> handler, std::function< bool (SDL_Event&)> tester);
	int addTimerHandler( std::function< Uint32 (Uint32)> handler, Uint32 ms);
	void removeEventHandler( int notifier);
	void removeTimerHandler( int timer);
	void startDispatcher();
	void endDispatcher();
private:
	void loop();
	bool exit_dispatcher;
	struct EventNotifier{
		EventNotifier( std::function< void (SDL_Event&)> h, std::function< bool (SDL_Event&)> t): handler(h), tester(t){};
		std::function< void (SDL_Event&)> handler;
		std::function< bool (SDL_Event&)> tester;
	};
	std::vector< EventNotifier> eventNotifiers;
	struct TimerNotifier{
		static Uint32 myCallBack( Uint32 interval, void *param){ return ((TimerNotifier*)param)->handler( interval); };
		TimerNotifier( std::function< Uint32 (Uint32)> h): handler(h){};
		std::function< Uint32 (Uint32)> handler;
		SDL_TimerID timerID;
	};
	std::vector< std::unique_ptr< TimerNotifier>> timerNotifiers;

};

#endif//_Dispatcher_H_

