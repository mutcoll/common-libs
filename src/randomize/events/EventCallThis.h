#ifndef _EventCallThis_H_
#define _EventCallThis_H_

#include <stdlib.h>
#include <functional>
#include "SDL.h"
#include "randomize/log/log.h"

namespace EventCallThis{
	extern Uint32 eventType;
	struct wraper{
		wraper( std::function< void ()> f):ff(f){};
		std::function< void ()> ff;
	};
	void pushEvent( std::function< void ()> ff);
};

#endif//_EventCallThis_H_

