#include "Dispatcher.h"

Dispatcher::Dispatcher(){
	//Initialize SDL for video output
	if (!SDL_WasInit(SDL_INIT_VIDEO)) {
		if ( SDL_InitSubSystem(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0 ) {
			LOG_ERROR("Unable to initialize SDL: %s\n", SDL_GetError());
			exit(1);
		}
	}//else //this should not occur never, in a proper aplication there's just one dispatcher.

	this->exit_dispatcher = false;
}

Dispatcher::~Dispatcher(){
	for( std::unique_ptr< TimerNotifier> &t: this->timerNotifiers){
		SDL_RemoveTimer( t->timerID);
	}

	if( SDL_WasInit( SDL_INIT_VIDEO)){
		SDL_QuitSubSystem( SDL_INIT_VIDEO);
	}//else //this should not occur never.
}

int Dispatcher::addEventHandler( std::function< void (SDL_Event&)> handler, std::function< bool (SDL_Event&)> tester){
	this->eventNotifiers.emplace_back( handler, tester);
	return this->eventNotifiers.size()-1;
}

int Dispatcher::addTimerHandler( std::function< Uint32 (Uint32)> handler, Uint32 ms){	// c++14 ready
/*	std::unique_ptr< TimerNotifier> t = std::make_unique< TimerNotifiers>( handler);
	t->timerID = SDL_AddTimer( ms, TimerNotifier::myCallBack, (void*) t.get());
	this->timerNotifiers.emplace_back( t);
*/	TimerNotifier *t = new TimerNotifier( handler);
	t->timerID = SDL_AddTimer( ms, TimerNotifier::myCallBack, (void*)t);
	this->timerNotifiers.emplace_back( t);
	return this->timerNotifiers.size()-1;
}

void Dispatcher::removeEventHandler( int notifier){
	this->eventNotifiers.erase( this->eventNotifiers.begin() + notifier);
}

void Dispatcher::removeTimerHandler( int timer){
	std::vector< std::unique_ptr< TimerNotifier>>::iterator it = this->timerNotifiers.begin() + timer;
	SDL_RemoveTimer( (*it)->timerID);
	this->timerNotifiers.erase( it);
}

void Dispatcher::startDispatcher(){
	this->loop();
}

void Dispatcher::endDispatcher(){
	this->exit_dispatcher = true;
}

void Dispatcher::loop(){
	SDL_Event event;
	while( !this->exit_dispatcher){
		if( SDL_WaitEvent( &event) == 1){
			for( EventNotifier &eventNotifier: this->eventNotifiers){ if( eventNotifier.tester(event)){ eventNotifier.handler( event);};};
		}//else // this means an error while waiting the event.
	}
}


