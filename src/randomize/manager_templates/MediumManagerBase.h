#ifndef _MediumManagerBase_H_
#define _MediumManagerBase_H_

#include "SimpleManagerBase.h"

class MediumManagerBase: public SimpleManagerBase{
public:
	explicit MediumManagerBase( Dispatcher &d, std::shared_ptr< Window> w = nullptr);
protected:
	virtual void onEvent( SDL_Event &event) override;
	virtual void onStep( Uint32 ms) override;

	/**
	 * If a key state (pressed or released) changes in this event loop iteration, this method will be called
	 */
	virtual void onKeyChange( SDL_KeyboardEvent &event){};
	/**
	 * If during this event loop iteration, a key is pressed (it was pressed now, or was kept pressed from before),
	 * this method will be called.
	 */
	virtual void onKeyPressed( SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode){};
	virtual void onMouseMotion( SDL_MouseMotionEvent &event){};

	virtual void onMouseButton( SDL_MouseButtonEvent &event){};
	virtual void onMouseWheel(SDL_MouseWheelEvent &event){};

	const Uint8 *keyState;
};

#endif//_MediumManagerBase_H_


