#ifndef _FlyerManagerBase_H_
#define _FlyerManagerBase_H_

#include "MediumManagerBase.h"
#include "randomize/graphics/drawables/Volador.h"

class FlyerManagerBase: public MediumManagerBase{
public:
	explicit FlyerManagerBase( Dispatcher &d, std::shared_ptr< WindowGL> w = nullptr);
protected:
	virtual void onDisplay( Uint32 ms) override;
	virtual void onStep(Uint32 ms) override;

	virtual void onKeyPressed( SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode) override;
	virtual void onMouseMotion(SDL_MouseMotionEvent &event) override;
	virtual void onMouseButton(SDL_MouseButtonEvent &event) override;
	virtual void onMouseWheel(SDL_MouseWheelEvent &event) override;

	Volador vldr;
	Vector3D center;

	Sint32 lastClickX = 0;
	Sint32 lastClickY = 0;
	bool mouseWheelPressed;

	float delta_rotation = 3.0;
	float delta_translation = 0.2;
	float mouse_delta_rotation = 0.0625f * delta_rotation;
	float mouse_delta_translation = 0.00175f;
	float mouse_delta_zoom = 0.1f;
};

#endif//_FlyerManagerBase_H_

