#include "SimpleManagerBase.h"

SimpleManagerBase::SimpleManagerBase( Dispatcher &d, std::shared_ptr< Window> w): dispatcher( d), window (w){
	EventCallThis::requestAutoCallEventHandler( this->dispatcher);
}

SimpleManagerBase::~SimpleManagerBase(){
	this->eraseTimerHandlers();
	if( this->eventsActive){ this->eraseEventHandlers();}
}

void SimpleManagerBase::initializeTimerHandlers(){
    this->startOnStep();
    this->startOnDisplay();
}

void SimpleManagerBase::initializeEventHandlers(){
	this->onEventID = this->dispatcher.addEventHandler(
		[this] (SDL_Event &event){ this->onEvent( event);},
		[this] (SDL_Event &event){ return this->active;});
	this->eventsActive = true;
}

void SimpleManagerBase::eraseTimerHandlers(){
    this->stopOnStep();
    this->stopOnDisplay();
}

void SimpleManagerBase::eraseEventHandlers(){
	this->eventsActive = false;
	this->dispatcher.removeEventHandler( this->onEventID);
}

void SimpleManagerBase::startOnStep(){
    if( !this->stepActive){
	    this->onStepID = this->dispatcher.addTimerHandler( [this] (Uint32 ms){
		    EventCallThis::pushEvent( [this,ms] (){ if( this->active){ this->onStep( ms);};});
		    return 1000 / this->fpsStep;
	    }, 1000 / this->fpsStep);
        this->stepActive = true;
    }
}

void SimpleManagerBase::startOnDisplay(){
    if( !this->displayActive){
	    this->onDisplayID = this->dispatcher.addTimerHandler( [this] (Uint32 ms){
		    EventCallThis::pushEvent( [this,ms] (){
			    if( this->active && !this->window.expired() && this->window.lock()->isOpen()){ this->onDisplay( ms);};
		    });
		    return 1000 / fpsDisplay;
	    }, 1000 / this->fpsDisplay);
        this->displayActive = true;
    }
}

void SimpleManagerBase::stopOnStep(){
	if( this->stepActive) this->dispatcher.removeTimerHandler( this->onStepID);
    this->stepActive = false;
}

void SimpleManagerBase::stopOnDisplay(){
	if( this->displayActive) this->dispatcher.removeTimerHandler( this->onDisplayID);
    this->displayActive = false;
}

void SimpleManagerBase::onStep( Uint32 ms){
}

void SimpleManagerBase::onDisplay( Uint32 ms){
}

void SimpleManagerBase::onEvent( SDL_Event &event){
}



