#ifndef _SimpleManagerBase_H_
#define _SimpleManagerBase_H_

#include <memory>
#include "randomize/events/Dispatcher.h"
#include "randomize/events/EventCallThis.h"
#include "randomize/events/AutoCallThis.h"
#include "randomize/graphics/Window.h"
#include "randomize/UI/UI.h"

class SimpleManagerBase{
public:
	explicit SimpleManagerBase( Dispatcher &d, std::shared_ptr< Window> w = nullptr);
	virtual ~SimpleManagerBase();
	virtual void setWindow( std::shared_ptr< Window> window){ this->window = window;};
	virtual void activate(){ this->active = true; initializeTimerHandlers(); if( !eventsActive){ initializeEventHandlers();};};
	virtual void deActivate(){ eraseTimerHandlers(); this->active = false;};
	void setStepFPS( Uint32 fps){ this->fpsStep = fps;};
	Uint32 getStepFPS(){ return this->fpsStep;};
	void setDisplayFPS( Uint32 fps){ this->fpsDisplay = fps;};
	Uint32 getDisplayFPS(){ return this->fpsDisplay;};
protected:
	std::weak_ptr< Window> window;

	Uint32 fpsDisplay = SimpleManagerBase::DefaultFPSDisplay, fpsStep = SimpleManagerBase::DefaultFPSStep;
	bool stepActive = false, displayActive = false, eventsActive = false, active = false;
	virtual void initializeTimerHandlers();
	virtual void initializeEventHandlers();
	virtual void eraseTimerHandlers();
	virtual void eraseEventHandlers();
    virtual void startOnStep();
    virtual void startOnDisplay();
    virtual void stopOnStep();
    virtual void stopOnDisplay();

	Dispatcher &dispatcher;
	int onEventID;
	virtual void onEvent( SDL_Event &event);

	int onStepID, onDisplayID;
	virtual void onStep( Uint32 ms);
	virtual void onDisplay( Uint32 ms);

private:
	static const Uint32 DefaultFPSDisplay = 30, DefaultFPSStep = 30;
};

#endif//_SimpleManagerBase_H_

