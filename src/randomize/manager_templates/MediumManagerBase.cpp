#include <randomize/utils/exception/StackTracedException.h>
#include "MediumManagerBase.h"

MediumManagerBase::MediumManagerBase( Dispatcher &d, std::shared_ptr< Window> w): SimpleManagerBase( d, w){
	this->keyState = SDL_GetKeyboardState( nullptr);
}

void MediumManagerBase::onEvent( SDL_Event &event){
	Uint32 windowID;
	bool windowReady = !this->window.expired() && this->window.lock()->isOpen();
	if (!windowReady) {
		// This window is not ready, so the event is either for closing this window, or from a different window.
		// Ignore the event, as it can be processed by its window
	} else {
		windowID = this->window.lock()->getWindowID();
		switch (event.type) {
		case SDL_KEYDOWN:
		case SDL_KEYUP:
			if (event.key.windowID == windowID) {
				this->onKeyChange(event.key);
			};
			break;
		case SDL_MOUSEMOTION:
			if (event.motion.windowID == windowID) {
				this->onMouseMotion(event.motion);
			};
			break;
		case SDL_MOUSEBUTTONDOWN:
		case SDL_MOUSEBUTTONUP:
			if (event.button.windowID == windowID) {
				this->onMouseButton(event.button);
			};
			break;
		case SDL_MOUSEWHEEL:
			if (event.wheel.windowID == windowID) {
				this->onMouseWheel(event.wheel);
			}
			break;
		case SDL_WINDOWEVENT:
			if (event.window.event == SDL_WINDOWEVENT_RESIZED) {
				this->window.lock()->resize(static_cast<Uint32>(event.window.data1),
						static_cast<Uint32>(event.window.data2));
			}
		default:
			//let children handle extra events
			break;
		}
	}
}

void MediumManagerBase::onStep( Uint32 ms){
	for( Uint32 scanCode = (SDL_Scancode) 0; scanCode < SDL_NUM_SCANCODES; scanCode++){
		if( this->keyState[ scanCode]){
			this->onKeyPressed( SDL_GetKeyFromScancode( (SDL_Scancode) scanCode), SDL_GetModState(), (SDL_Scancode) scanCode);
		}
	}
}



