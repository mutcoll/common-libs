#include "FlyerManagerBase.h"

FlyerManagerBase::FlyerManagerBase( Dispatcher &d,  std::shared_ptr< WindowGL> w): MediumManagerBase( d, w){
	this->vldr.setPos( Vector3D(1, 2, 10));
	this->vldr.setOrientacion( this->center - vldr.getPos(), Vector3D(0, 1, 0));
	mouseWheelPressed = false;
}

void FlyerManagerBase::onDisplay( Uint32 ms){
	glLoadIdentity();
	this->vldr.Look();
}

void FlyerManagerBase::onStep(Uint32 ms) {
	MediumManagerBase::onStep(ms);

	int xMouse = 0, yMouse = 0;
	auto unusedState = SDL_GetMouseState( &xMouse, &yMouse) & SDL_BUTTON(SDL_BUTTON_MIDDLE);
	if (this->mouseWheelPressed) {
		auto xDistanceSinceLastClick = this->lastClickX - xMouse;
		auto angle = xDistanceSinceLastClick * mouse_delta_rotation * mouse_delta_rotation;
		vldr.externalRotateAround(center, angle, 0, 1, 0);
	}
}

void FlyerManagerBase::onKeyPressed( SDL_Keycode keyCode, SDL_Keymod keyMod, SDL_Scancode scanCode){
	Vector3D aux_vec;
	switch( keyCode){
	case SDLK_UP:
		aux_vec = (vldr.getX() ^ Vector3D{0, 1, 0}) * delta_translation;
		center += aux_vec;
		vldr.setPos( vldr.getPos() + aux_vec);
		break;
	case SDLK_DOWN:
		aux_vec = (vldr.getX() ^ Vector3D{0, 1, 0}) * delta_translation;
		center += aux_vec;
		vldr.setPos( vldr.getPos() - aux_vec);
		break;
	case SDLK_LEFT:
		vldr.rotatef(delta_rotation, 0, 1, 0);
		vldr.setOrientacion(vldr.getDir(), {0, 1, 0});
		break;
	case SDLK_RIGHT:
		vldr.rotatef(-delta_rotation, 0, 1, 0);
		vldr.setOrientacion(vldr.getDir(), {0, 1, 0});
		break;

	case SDLK_u:
		vldr.rotatef(-delta_rotation, 0, 0, 1);
		break;
	case SDLK_o:
		vldr.rotatef(delta_rotation, 0, 0, 1);
		break;
	case SDLK_i:
		vldr.rotatef(delta_rotation, 1, 0, 0);
		center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
		break;
	case SDLK_k:
		vldr.rotatef(-delta_rotation, 1, 0, 0);
		center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
		break;
	case SDLK_l:
		vldr.rotY(-delta_rotation);
		center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
		break;
	case SDLK_j:
		vldr.rotY(delta_rotation);
		center = vldr.getPos() + vldr.getDir()*(vldr.getPos()-center).Modulo();
		break;

	case SDLK_e:
		center += vldr.getDir()*delta_translation;
		vldr.setPos(vldr.getPos() + vldr.getDir()*delta_translation);
		break;
	case SDLK_q:
		center -= vldr.getDir()*delta_translation;
		vldr.setPos(vldr.getPos() - vldr.getDir()*delta_translation);
		break;
	case SDLK_w:
		center += vldr.getUp()*delta_translation;
		vldr.setPos(vldr.getPos() + vldr.getUp()*delta_translation);
		break;
	case SDLK_s:
		center -= vldr.getUp()*delta_translation;
		vldr.setPos(vldr.getPos() - vldr.getUp()*delta_translation);
		break;
	case SDLK_d:
		center -= vldr.getX()*delta_translation;
		vldr.setPos(vldr.getPos() - vldr.getX()*delta_translation);
		break;
	case SDLK_a:
		center += vldr.getX()*delta_translation;
		vldr.setPos(vldr.getPos() + vldr.getX()*delta_translation);
		break;
	}
}

void FlyerManagerBase::onMouseButton(SDL_MouseButtonEvent &event) {
	lastClickX = event.x;
	lastClickY = event.y;
	if (event.button == SDL_BUTTON_MIDDLE) {
		mouseWheelPressed = event.state == SDL_PRESSED;
	}
}

void FlyerManagerBase::onMouseWheel(SDL_MouseWheelEvent &event) {
	MediumManagerBase::onMouseWheel(event);
	vldr.setPos(vldr.getPos() + event.y * mouse_delta_zoom * (center - vldr.getPos()).Modulo() * vldr.getDir());
}

void FlyerManagerBase::onMouseMotion(SDL_MouseMotionEvent &event) {
	auto leftMouseButtonPressed = SDL_GetMouseState( nullptr, nullptr) & SDL_BUTTON(SDL_BUTTON_LEFT);
	if (leftMouseButtonPressed) {
		vldr.rotateAround( center, event.yrel * mouse_delta_rotation, 1, 0, 0);
		vldr.externalRotateAround( center, -event.xrel * mouse_delta_rotation, 0, 1, 0);
	}

	auto rightMouseButtonPressed = SDL_GetMouseState( nullptr, nullptr) & SDL_BUTTON(SDL_BUTTON_RIGHT);
	if (rightMouseButtonPressed) {
		Vector3D projection_y( vldr.getDir());
		float sense = projection_y.getY() <= 0 ? 1 : -1;
		projection_y.setY(0);
		Vector3D projection_x(vldr.getX());
		projection_x.setY(0);
		Vector3D rawTranslation( event.yrel * mouse_delta_translation * projection_y * sense
				+ event.xrel * mouse_delta_translation * projection_x);
		auto makeTranslationProportionalToDistanceFromCameraToCenter = (center - vldr.getPos()).Modulo();
		auto translation = rawTranslation * makeTranslationProportionalToDistanceFromCameraToCenter;
		center += translation;
		vldr.setPos(vldr.getPos() + translation);
	}
}
