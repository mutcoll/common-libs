#ifndef _NETWORK_MANAGER_H_
#define _NETWORK_MANAGER_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <sys/wait.h>
#include <netdb.h> 

#include <errno.h>

/**
 * Version 1.1
 * Ultima modificación: 15-abril-2013
 * Estado : Actualizado
 * */
class NetworkManager
{
	public:
		NetworkManager():conectado(false),sock(-1){}
		
		int Conect(const char ip[16],unsigned short portno);
		int Disconect();
		
		
	protected:
		int Write(const char* c, int n);		
		int Read(const char* c, int n);
		static void Error(int sock,const char *msg);
		
		int sock;
		bool conectado;
		
};

class MyNetworkManager : public NetworkManager
{
	public:
		/**
		 * Comandos
		 * */
		int echo(const char *c);
		int ping();
		int puntuacion(const char *c, int p);
		
		
		int pulsado(int n);
		int soltado(int n);
		
		int estado(int es[4]);
};

class HTTPclient : private NetworkManager
{
	
	public:
		HTTPclient();
		HTTPclient(const char ip[16]);
		HTTPclient(const char ip[16],unsigned short portno);
		
	private:
	
};

#endif
