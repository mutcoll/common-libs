#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <signal.h>

#include <errno.h>


#include "../log/log.h"
#include "randomize/utils/threads/runnable.h"

#include <errno.h>
#include <list>
using std::list;

class ServerConnection : public runnable{
	public:
		ServerConnection(int ID, int socketID):runnable(),id(ID), socketId(socketID){}
		
		int getId(){return id;}
		int getSocketId(){return socketId;}
	protected:
		int id;
		int socketId;
	protected:
		virtual void run(){
			LOG_DEBUG("%d, %d", id, socketId);
		}
};


template<class SC>
class ServerManager : public runnable{
	public:
	protected:
		int sock = -1;
		int tamQueue = 5;
		int lastID = 0;
		unsigned short port;
		list<SC*> connections;
		bool runDaemon;
	private:
	
	public:
		ServerManager(){}
		ServerManager(unsigned short aPort){
			openSocket(aPort, tamQueue);
		}
		ServerManager(unsigned short aPort, int tamQueue):tamQueue(5){
			openSocket(aPort, tamQueue);
		}
		void acceptConnection(){
			struct sockaddr_in s;
			int tam = sizeof(struct sockaddr), connection;
			LOG_INFO("Wait...");
			if((connection=accept(sock,(struct sockaddr *)&s, (socklen_t*)&tam))<0){
				LOG_ERROR("Accept. %d, %d",sock, connection);
				return;
				Error("accept");
			}
			SC* sc= new SC(lastID++,connection);
			configConnection(sc);
			sc->start();
			connections.push_back(sc);	
		}
		void acceptDaemon(){
			runDaemon = true;
			this->start();
		}
		
		void closeConnections(){
			for(auto c : connections){
				close(c->getSocketId());
				c->join();
			}
			#define SIGNAL_PARAR SIGIO
			runDaemon = false;
		//	pthread_kill(self, SIGNAL_PARAR);
			//fcntl(sock, F_SETFL, fcntl(sock, F_GETFL, 0) | O_NONBLOCK);
			
			//LOG_DEBUG("Aqui");
			//pthread_kill(self, SIGINT);
			
			//LOG_DEBUG("AquiNO");
			join();
		}
		
	protected:
		virtual void configConnection(SC* sc){}
	private:
		void run(){
			int i = 0;
			LOG_INFO("Created thread %lu", pthread_self() );

			fcntl(sock, F_SETFL, fcntl(sock, F_GETFL, 0) | O_NONBLOCK);
//			fcntl(sock, F_SETSIG , fcntl(sock, F_GETSIG , 0) | O_ASYNC);
			//signal(SIGNAL_PARAR, [this](int s){LOG_INFO("Recv señal : %d on thread %lu", s, pthread_self() );});
			while(runDaemon){i++;
				LOG_DEBUG("Aceptando... %d", i);
				sleep(2);
				acceptConnection();
			}
			LOG_DEBUG("Salimos");
			close(sock);
		}


		void openSocket(unsigned short aPort, int tamQ){
			int tipo;
			struct sockaddr_in s;
			
			this->port = aPort;
			this->tamQueue = tamQ;
			
			// Crea socket y devuelve descriptor
			if ((sock=socket(AF_INET,SOCK_STREAM,0))<0) 
				Error("socket");

			// Rellena la estructura con la dirección y puerto a usar por el socket
			s.sin_family=AF_INET;
			s.sin_port=htons(aPort);
			s.sin_addr.s_addr=htonl(INADDR_ANY); 

			// Asocia el socket a la direccion y puerto especificado por la estructura
			if (bind(sock,(struct sockaddr *)&s,sizeof(struct sockaddr_in))!=0)
				Error("bind");

			tipo=0;
			// Modifica parametros del socket, en este caso el tipo de conexion.
			// El valor lo pone a 0 para que sea bloqueante.
			if (ioctl(sock,FIONBIO,&tipo)!=0)
				Error("ioctl");

			// Indica que el socket desea aceptar conexiones y limita el numero de las mismas
			if (listen(sock,tamQ)!=0) 
				Error("listen");
			
		}		
		
		void Error(const char *msg)
		{
			int errsv = errno;
			if (sock>=0)
				close(sock);
			LOG_FATAL("%s en socketId=%d :: errno:%d %s",msg,sock, errsv, strerror(errsv));
		}
};
