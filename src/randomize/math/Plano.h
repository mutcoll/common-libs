#ifndef _PLANO_G_H_
#define _PLANO_G_H_
#include "Vector3D.h"
#include "Geom.h"

namespace randomize { namespace math {

class PlanoG : public Quad
{
	public:
		PlanoG(){}
		PlanoG(Vector3D p0, Vector3D p1, Vector3D p2, Vector3D p3);	// en sentido antihorario desde p0
		PlanoG(Vector3D p0, Vector3D p0p1, Vector3D p0p3);	// p0p1 = p1 - p0
		PlanoG(Vector3D p0p1, Vector3D p0p3);	// se asume p0 = {0, 0, 0} para moverlo despues

		void setPos(Vector3D pos);
};

}};
#endif

