#ifndef _Vector3D_H_
#define _Vector3D_H_

#include "randomize/platform/port.h"
#include <cmath>
#ifndef ANDROID
#include <iostream>
#endif
#include <cstring>


class Vector3D
{
	public:
		/*	constructores	*/
		Vector3D ();
		Vector3D (const double&, const double&, const double&);
		Vector3D (const Vector3D&);
		Vector3D (const double f[3])
		{
			memcpy(v, f, sizeof(double)*3);		// realmente vale la pena? si hace un bucle sera peor...
			//v[0] = f[0];
			//v[1] = f[1];
			//v[2] = f[2];
			//faltaria calcular el modulo, si no se es coherente en todo el codigo no sirve de nada
		}
		
		/*	operadores	*/
		Vector3D& operator= (const Vector3D&);
		Vector3D operator+ (const Vector3D&) const;
		Vector3D& operator+= (const Vector3D&);
		Vector3D operator- (const Vector3D&) const;
		Vector3D& operator-= (const Vector3D&);
		Vector3D operator* (const double&) const;		//vector * double
		Vector3D& operator*= (const double& f);
		Vector3D operator/ (const double&) const;
		Vector3D& operator/= (const double& f);
		double operator* (const Vector3D&) const;		// producto escalar de dos vectores
		Vector3D operator^ (const Vector3D&) const;		// producto vectorial de dos vectores
		
		double & operator []( int index )
		{
			return v[index];
		} 
		
		bool operator!= (const Vector3D&) const;
		bool operator== (const Vector3D&) const;
		bool operator> (const Vector3D&) const;
		bool operator>= (const Vector3D&) const;
		bool operator< (const Vector3D&) const;
		bool operator<= (const Vector3D&) const;
		
		/*	amigas	*/
		#ifndef ANDROID
		friend std::ostream& operator<< (std::ostream&, const Vector3D&);
		#endif
		friend Vector3D operator* (const double&, const Vector3D&);		// double * vector
		
		/*	varios	*/
		Vector3D Truncado () const;
		Vector3D Entero () const;		// redondeado
		Vector3D Unit() const;		// misma direccion, modulo 1
		void EsCero ();		// se le asigna 0 a todo
		double Modulo() const;		// get magnitude
		double Determinante(Vector3D, Vector3D) const;
		
		/*	getters y setters	*/
		double getX () const {return x;}
		void setX (const double & x2) {x = x2;}
		
		double getY () const {return y;}
		void setY (const double & y2) {y = y2;}
		
		double getZ () const {return z;}
		void setZ (const double & z2) {z = z2;}
		
		
	//private:
		union{
			struct{
				double x, y, z;
			};
			double v[3];
		};
			
};

#endif

