#ifndef _GEOM_H_
#define _GEOM_H_

#include "Vector3D.h"

namespace randomize {
namespace math {


struct Segment
{
    Vector3D P0;
    Vector3D P1;
};
struct Plane{
    Vector3D V0;
    Vector3D n;
};
struct Triangle{
    Vector3D V0;
    Vector3D V1;
    Vector3D V2;
};

struct Quad{
    Vector3D p[4];
    Vector3D n;
};

typedef Segment Line;
typedef Segment Ray;
typedef Vector3D Point;
typedef Vector3D Vector;


// isLeft(): test if a point is Left|On|Right of an infinite 3D line.
//    Input:  three points P0, P1, and P2
//    Return: >0 for P2 left of the line through P0 to P1
//          =0 for P2 on the line
//          <0 for P2 right of the line
inline int isLeft( Point P0, Point P1, Point P2 ) {
    return P0.Determinante(P1,P2);
}
//===================================================================

int inSegment( Point P, Segment S);
int intersect2D_2SegmentsXY( Segment S1, Segment S2, Point* I0, Point* I1 );
int intersect2D_2SegmentsXZ( Segment S1, Segment S2, Point* I0, Point* I1 );

int intersect3D_RayTriangle( Ray R, Triangle T, Point* I );
int intersect3D_SegmentPlane( Segment S, Plane Pn, Point* I );
int intersect3D_2Planes( Plane Pn1, Plane Pn2, Line* L );
int intersect3D_SegmentTriangle( Segment R, Triangle T, Point* I );

int caeDentroPlano (Quad & p, Vector3D & q, float &dist, Vector3D &interseccion);
int dentroPlano (Quad & p, Vector3D &q);

};  // math
};  // randomize

#endif //_GEOM_H_
