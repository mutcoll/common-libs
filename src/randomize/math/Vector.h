#ifndef _VECTOR_H_
#define _VECTOR_H_

#include "randomize/platform/port.h"
#include <cmath>
#ifndef __CUDACC__
    #define __device__
    #define __host__
    #define __global__
#endif

class Vector{
    public:
        //Constructor
        __device__ __host__ Vector():x(0),y(0),z(0)
            {}
        __device__ __host__ Vector(const float X, const float Y, const float Z):x(X),y(Y),z(Z)
            {}
        __device__ __host__ Vector(const Vector& f):x(f.x),y(f.y),z(f.z)
            {}
        __device__ __host__ Vector(const float f[3]):x(f[0]),y(f[1]),z(f[2])
            {}
        //Access operations
        __device__ __host__ float& operator[](const int i)
            {return v[i];}
        
        //Atirmetic Operations
            //+
        __device__ __host__ Vector operator+ (const Vector& f)const {return Vector(x+f.x,y+f.y, z+f.z);}
        __device__ __host__ Vector operator+= (const Vector& f)     {x+=f.x;y+=f.y;z+=f.z;return *this;}
            //-
        __device__ __host__ Vector operator- ()                     {x=-x;y=-y;z=-z;return *this;}
        __device__ __host__ Vector operator- (const Vector& f) const{return Vector(x-f.x,y-f.y, z-f.z);}
        __device__ __host__ Vector operator-= (const Vector& f)     {x-=f.x;y-=f.y;z-=f.z;return *this;}
            
            // *
        __device__ __host__ Vector operator* (const float& f) const {return Vector(x*f, y*f, z*f);}
        __device__ __host__ Vector& operator*= (const float f)      {x*=f;y*=f;z*=f;return *this;}
        __device__ __host__ Vector& operator*= (const Vector& f)    {x*=f.x;y*=f.y;z*=f.z;return *this;}
        __device__ __host__ friend Vector operator* (const float& f, const Vector& v)
            {return Vector(v.x*f,v.y*f,v.z*f);}
            // /
        __device__ __host__ Vector operator/ (const float& f)       {return Vector(x/f, y/f, z/f);}
        __device__ __host__ Vector& operator/= (const Vector& f)    {x/=f.x;y/=f.y;z/=f.z;return *this;}
        __device__ __host__ Vector& operator/= (const float f)      {x/=f;y/=f;z/=f;return *this;}
        __device__ __host__ friend Vector operator/ (const float& f, const Vector& v)
            {return Vector(f/v.x,f/v.y,f/v.z);}
        
        //Vectorial Operations
        __device__ __host__ float operator* (const Vector& v)        // producto escalar
            {return (x*v.x + y*v.y + z*v.z);}
        __device__ __host__ Vector operator^ (const Vector& v)        // producto vectorial
            {return (Vector (y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x));}
        
        //Others
        __device__ __host__ float Modulo()                          {return sqrt(x*x+y*y+z*z);}
        __device__ __host__ Vector Unit()                           {float m = Modulo();return Vector(x/m, y/m, z/m);}
            
    
    //float m;
    union{
        struct{
            float x, y, z;
        };
        float v[3];
    };
};

typedef Vector Vector3D;


#endif //_VECTOR_H_
