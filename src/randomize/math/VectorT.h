#ifndef _VECTOR_H_
#define _VECTOR_H_

#include "randomize/platform/port.h"
#include <cmath>
#ifndef __CUDACC__
    #define __device__
    #define __host__
    #define __global__
#endif

template <class TYPE>
class VectorT{
	public:
		//Constructor
		__device__ __host__ VectorT():x(0),y(0),z(0)
			{}
		__device__ __host__ VectorT(TYPE X, TYPE Y, TYPE Z):x(X),y(Y),z(Z)
			{}
		__device__ __host__ VectorT(const VectorT& f):x(f.x),y(f.y),z(f.z)
			{}
		//Access operations
		__device__ __host__ TYPE& operator[](const int i)
			{return v[i];}
		
		//Atirmetic Operations
			//+
		__device__ __host__ VectorT operator+ (const VectorT& f) 		{return VectorT(x+f.x,y+f.y, z+f.z);}
		__device__ __host__ VectorT operator+= (const VectorT& f) 	{x+=f.x;y+=f.y;z+=f.z;return *this;}
			//-
		__device__ __host__ VectorT operator- () 					{x=-x;y=-y;z=-z;return *this;}
		__device__ __host__ VectorT operator- (const VectorT& f) 		{return VectorT(x-f.x,y-f.y, z-f.z);}
		__device__ __host__ VectorT operator-= (const VectorT& f) 	{x-=f.x;y-=f.y;z-=f.z;return *this;}
			
			// *
		__device__ __host__ VectorT operator* (const TYPE& f) 		{return VectorT(x*f, y*f, z*f);}
		__device__ __host__ VectorT& operator*= (const TYPE f) 		{x*=f;y*=f;z*=f;return *this;}
		__device__ __host__ VectorT& operator*= (const VectorT& f)	{x*=f.x;y*=f.y;z*=f.z;return *this;}
		__device__ __host__ friend VectorT operator* (const TYPE& f, const VectorT& v)
			{return VectorT(v.x*f,v.y*f,v.z*f);}
			// /
		__device__ __host__ VectorT operator/ (const TYPE& f) 		{return VectorT(x/f, y/f, z/f);}
		__device__ __host__ VectorT& operator/= (const VectorT& f)	{x/=f.x;y/=f.y;z/=f.z;return *this;}
		__device__ __host__ VectorT
		& operator/= (const TYPE f) 		{x/=f;y/=f;z/=f;return *this;}
		__device__ __host__ friend VectorT operator/ (const TYPE& f, const VectorT& v)
			{return VectorT(f/v.x,f/v.y,f/v.z);}
		
		//VectorTial Operations
		__device__ __host__ TYPE operator* (const VectorT& v)		// producto escalar
			{return (x*v.x + y*v.y + z*v.z);}
		__device__ __host__ VectorT operator^ (const VectorT& v)		// producto vectorial
			{return (VectorT (y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x));}
		
		//Others
		__device__ __host__ TYPE Modulo()
			{return sqrt(x*x+y*y+z*z);}
		__device__ __host__ VectorT Unit()
			{TYPE m = Modulo();return VectorT(x/m, y/m, z/m);}
			
			
	union{
		struct{
			TYPE x, y, z;
		};
		TYPE v[3];
	};
};

using Vector   =  VectorT<float>;
using Vector3D =  VectorT<float>;
using Vectorf  =  VectorT<float>;
using Vectord  =  VectorT<double>;
//typedef Vector Vector3D;


#endif //_VECTOR_H_
