 /***************************************
 * Math functions fot matrix
 * Based on MESA's GLU implementation
 * 
 * 		see: http://www.mesa3d.org
 * 
 * Author: Jacobo Coll Moragón
 *
 *
 * Data: 2014-01-15
 * Version: 1.0
 *
 ***************************************/


#ifndef _MATRIX_H_
#define _MATRIX_H_


bool gluInvertMatrixd(const double m[16], double invOut[16]);
bool gluInvertMatrixf(const float m[16], float invOut[16]);
void gluMultMatrixVecd(const double matrix[16], const double in[4], double out[4]);
void gluMultMatrixVecf(const float matrix[16], const float in[4], float out[4]);



#endif //_MATRIX_H_
