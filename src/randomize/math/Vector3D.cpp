#include <stdexcept>
#include <randomize/utils/exception/StackTracedException.h>
#include "Vector3D.h"

Vector3D::Vector3D() : x(0), y(0), z(0) { }

Vector3D::Vector3D(const double& x1, const double& y1, const double& z1) : x(x1), y(y1), z(z1) { }

Vector3D::Vector3D (const Vector3D& v)
{
	*this = v;
}

Vector3D& Vector3D::operator= (const Vector3D& v)
{
	x = v.x;
	y = v.y;
	z = v.z;
	return *this;
}

Vector3D Vector3D::operator+ (const Vector3D& v) const
{
	return Vector3D (x+v.x, y+v.y, z+v.z);
}
Vector3D& Vector3D::operator+= (const Vector3D& v)
{
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
	//return (*this = *this + v);	// que animalada es esta??
}
Vector3D Vector3D::operator- (const Vector3D& v) const
{
	return Vector3D (x-v.x, y-v.y, z-v.z);
}
Vector3D& Vector3D::operator-= (const Vector3D& v)
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
	//return (*this = *this - v);
}

Vector3D Vector3D::operator* (const double& f) const
{
	return Vector3D (x*f, y*f, z*f);
	
	//Vector3D v;
	//v.x = x * f;
	//v.y = y * f;
	//v.z = z * f;
	//if (f < 0)
		//v.m = m * -f; 
	//else
		//v.m = m * f;
	//
	//return v;
}

Vector3D& Vector3D::operator*= (const double& f)
{
	x *= f;
	y *= f;
	z *= f;
	return *this;
}


Vector3D Vector3D::operator/ (const double& f) const
{
	return Vector3D (x/f, y/f, z/f);
	//Vector3D v;
	//v.x = x / f;
	//v.y = y / f;
	//v.z = z / f;
	//if (f < 0)
		//v.m = m / -f; 
	//else
		//v.m = m / f;
	//
	//return v;
}
Vector3D& Vector3D::operator/= (const double& f)
{
	x /= f;
	y /= f;
	z /= f;
	return *this;
}

double Vector3D::operator* (const Vector3D& v) const		// producto escalar
{
	return (x*v.x + y*v.y + z*v.z);
}

Vector3D Vector3D::operator^ (const Vector3D& v) const		// producto vectorial
{
	return Vector3D (y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x);
}

bool Vector3D::operator!= (const Vector3D& v) const
{
	return (x != v.x || y != v.y || z != v.z)? true : false;
	//bool b;
	//
	//if (x != v.x || y != v.y || z != v.z)
		//b = true;
	//else
		//b = false;
	//
	//return b;
}

bool Vector3D::operator== (const Vector3D& v) const
{
	return (x == v.x && y == v.y && z == v.z)? true : false;
	//bool b;
	//
	//if (x == v.x && y == v.y && z == v.z)
		//b = true;
	//else
		//b = false;
	//
	//return b;
}

bool Vector3D::operator> (const Vector3D& v) const
{
	return Modulo() > v.Modulo();
}

bool Vector3D::operator>= (const Vector3D& v) const
{
	return Modulo() >= v.Modulo();
}

bool Vector3D::operator< (const Vector3D& v) const
{
	return Modulo() < v.Modulo();
}

bool Vector3D::operator<= (const Vector3D& v) const
{
	return Modulo() <= v.Modulo();
}

#ifndef ANDROID
std::ostream& operator<< (std::ostream& o, const Vector3D& v)
{
	o << "(" << v.x << ", " << v.y << ", " << v.z << "): " << v.Modulo() << "  ";
	
	return o;
}
#endif

Vector3D operator* (const double& f, const Vector3D& v)		// amiga, double*vector3D
{
	return Vector3D (f*v.x, f*v.y, f*v.z);
	//Vector3D u;
	//
	//u.x = f * v.x;
	//u.y = f * v.y;
	//u.z = f * v.z;
	//if (f < 0)
		//u.m = v.m * -f; 
	//else
		//u.m = v.m * f;
	//
	////u.m = sqrt(u.x*u.x + u.y*u.y);
	//
	//return u;
}

Vector3D Vector3D::Truncado () const
{
	return Vector3D (int(x), int(y), int(z));
}

Vector3D Vector3D::Entero () const		// redondeado
{
	return Vector3D (int(x + 0.5), int(y + 0.5), int(z + 0.5));
}

Vector3D Vector3D::Unit() const
{
	double m = Modulo();
	if (m == 0)
	{
		throw randomize::utils::exception::StackTracedException(
				"cannot do a unitary Vector3D if its magnitude is zero");
	}
	else
	{
		return Vector3D(x / m, y / m, z / m);
	}
}

void Vector3D::EsCero ()
{
	x = 0;
	y = 0;
	z = 0;
}

double Vector3D::Modulo() const
{
	return sqrt(x*x + y*y + z*z);		//actualiza el miembro de la clase
}

double Vector3D::Determinante(Vector3D a, Vector3D b) const
{
	return x*a.y*b.z + y*a.z*b.x + z*a.x*b.y - z*a.y*b.x - y*b.z*a.x - x*b.y*a.z;

}

