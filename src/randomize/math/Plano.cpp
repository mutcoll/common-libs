#include "Plano.h"
#ifdef ANDROID
	#define FACTOR_TEXTURA_MAPA 0.3
#else
	#define FACTOR_TEXTURA_MAPA 0.5
#endif


namespace randomize { namespace math {

PlanoG::PlanoG(Vector3D p0, Vector3D p1, Vector3D p2, Vector3D p3)	// en sentido antihorario desde p0
{
	p[0] = p0;
	p[1] = p1;
	p[2] = p2;
	p[3] = p3;


	n = ((p1-p0)^(p3-p0)).Unit();
}

PlanoG::PlanoG(Vector3D p0, Vector3D p0p1, Vector3D p0p3)	// p0p1 = p1 - p0
{
	p[0] = p0;
	p[1] = p0 + p0p1;
	p[2] = p0 + p0p1 + p0p3;
	p[3] = p0 + p0p3;
	n = (p0p1 ^ p0p3).Unit();
}

PlanoG::PlanoG(Vector3D p0p1, Vector3D p0p3)	// se asume p0 = {0, 0, 0} para moverlo despues
{
	p[0] = Vector3D();	// en {0, 0, 0}
	p[1] = p0p1;
	p[2] = p0p1 + p0p3;
	p[3] = p0p3;
	n = (p0p1 ^ p0p3).Unit();
}


void PlanoG::setPos(Vector3D pos)
{
	for (int i = 0; i < 4; i++)
		p[i] += pos;
}

}};

///////////////////////// suelo ////////////////////////////

//Suelo::Suelo()
//{

//}


