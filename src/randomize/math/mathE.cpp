
#include "mathE.h"

namespace randomize { namespace math { namespace mathE {

#define PI 3.1415926535897932384
#define PI2 6.2831853071795864768

float *mE_SIN, *mE_COS, *mE_TAN;
int mE_TAM = 0;
int mE_P = 1;


void mE_Init(int e)	//Valor estable de 'e' entre 4 y 6.
{
	if(mE_TAM == 0)
	{
		for (int i = 0; i < e; i ++)
		{
			mE_P *= 10;
		}
		mE_TAM = mE_P * PI2;
		mE_SIN = new float[mE_TAM];

		for (int i = 0; i < mE_TAM ; i ++)
		{
			mE_SIN[i] = sin((i/(float)mE_TAM)*PI2);
		}
	
		mE_COS = new float[mE_TAM];
		for (int i = 0; i < mE_TAM ; i ++)
		{
			mE_COS[i] = cos((i/(float)mE_TAM)*PI2);
		}
	
		mE_TAN = new float[mE_TAM];
		for (int i = 0; i < mE_TAM ; i ++)
		{
			mE_TAN[i] = tan((i/(float)mE_TAM)*PI2);
		}
	}

}

void mE_End()
{
	if(mE_TAM > 0)
	{
		delete[] mE_SIN;
		delete[] mE_COS;
		delete[] mE_TAN;
		mE_TAM = 0;
		mE_P = 1;
	}
}

float mE_sin(float ang)
{
	
	if(ang > PI2 || ang < 0)
	{
		int k2pi = ang / PI2;
		if (ang < 0)
		{
			k2pi--;
		}
		ang -= k2pi*PI2;
	}
	int angulo = ang * mE_P;
	return mE_SIN[angulo];
	
}

float mE_cos(float ang)
{
	
	if(ang > PI2 || ang < 0)
	{
		int k2pi = ang / PI2;
		if (ang < 0)
		{
			k2pi--;
		}
		ang -= k2pi*PI2;
	}
	int angulo = ang * mE_P;
	return mE_COS[angulo];
	
}

float mE_tan(float ang)
{
	if(ang > PI2 || ang < 0)
	{
		int k2pi = ang / PI2;
		if (ang < 0)
		{
			k2pi--;
		}
		ang -= k2pi*PI2;
	}
	int angulo = ang * mE_P;
	return mE_TAN[angulo];
	
}

}}}
