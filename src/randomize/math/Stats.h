/**
 * @file Stats.h
 * @author jmmut 
 * @date 2016-04-17.
 */

#ifndef COMMONLIBS_STATS_H
#define COMMONLIBS_STATS_H

#include <algorithm>
#include <numeric>

namespace randomize {
namespace math {

template<class InputIt, class T>
double average(InputIt first, InputIt last, T init) {
    T sum = std::accumulate(first, last, init);
    return static_cast<double>(sum) / (last - first);
};


/**
 * population variance
 */
template<class InputIt>
double variance(InputIt first, InputIt last, double average) {
    double sum = 0;
    double count = last - first;
    while (first != last) {
        sum += (*first - average) * (*first - average);
        ++first;
    }
    return static_cast<double>(sum) / count;
};

template<class InputIt>
double standardDeviation(InputIt first, InputIt last, double average) {
    return std::sqrt(variance(first, last, average));
};



};  // math
};  // randomize

#endif //COMMONLIBS_STATS_H
