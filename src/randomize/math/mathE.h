/*********************************************************
/
/	Nombre Proyecto:           MathE
/
/	Autor/es:      Jacobo Coll Moragón
/	Descripción:   Tablas de asignación para las funciones trigonometricas Sin, Cos y Tan a partir de math.h
/	Fecha:         mar oct 11 22:58:35 CEST 2011
/
/.**********************************************************/

#ifndef _MATHE_H_
#define _MATHE_H_

#include <cmath>


namespace randomize { namespace math { namespace mathE {
	/**
	* Inicializa las tablas de asignación.
	* Las tablas de asignacion tendran un tamaño de 2*PI*10^e
	* Un valor aproximado y correcto para 'e' puede ser entre 4 y 6.
	* 
	* @param e Exponente de precisión.
	* 
	*/
void mE_Init(int e);

	/**
	* Borra las tablas de asignación.
	* No es necesario hacer al finalizar el programa, pero si para volver 
	* a inicializar las tablas.
	* 
	*/
void mE_End();

	/**
	* Devuelve el valor del sinus de ang.
	* 
	* @param ang Valor del angulo en Radianes.
	* 
	* @return El valor del sinus de ang.
	*/
float mE_sin(float ang);

	/**
	* Devuelve el valor del cosinus de ang.
	* 
	* @param ang Valor del angulo en Radianes.
	* 
	* @return El valor del cosinus de ang.
	*/
float mE_cos(float ang);

	/**
	* Devuelve el valor de tangete de ang.
	* 
	* @param ang Valor del angulo en Radianes.
	* 
	* @return El valor de tangente de ang.
	*/
float mE_tan(float ang);

	/**
	* Punteros a las tablas de asignación.
	*/
extern float *mE_SIN, *mE_COS, *mE_TAN;
	/**
	* Tamaño de las tablas de asignación
	* 
	*/
extern int mE_TAM;
	/**
	* Presición de las tablas de asignación
	* 
	*/
extern int mE_P;

}}}

#endif

