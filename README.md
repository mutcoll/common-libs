# Randomize common-libs

**Warning: project under construction**

## Requirements to compile or use in another project

- C++ compiler (`sudo apt-get install build-essential`)
- opengl (`sudo apt-get install libgl1-mesa-dev`)
- cmake (`sudo apt-get install cmake`)
- conan (download [here](https://www.conan.io/downloads), recommended: `sudo apt-get install python-pip; sudo pip install conan`)

## Compiling this project
```
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan --insert
conan remote add commonlibs-repo https://api.bintray.com/conan/jmmut/commonlibs --insert
mkdir build && cd build/
conan install .. --build missing
cmake -G "Unix Makefiles" ..
make
```

## Using this project as a library from your user project

The easiest way to use is with conan. For example, if you use cmake as your build system, 
copy this into a `conanfile.txt` in your project root directory:
```
[requires]
commonlibs/0.2.5@jmmut/testing

[generators]
cmake
```

You'll need a CMakeLists.txt to build your project:
```
project(MyTemplateProject)
cmake_minimum_required(VERSION 2.8)

# if you use clion, you'll have to do `conan install ..` inside the clion build folder.
include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
conan_basic_setup()


set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")
include_directories(src)
include_directories(include)

set(SOURCE_FILES
    ./src/somecode.cpp
    )

# this avoids compiling the same sources twice when you have 2 or more executables
add_library(user_project_lib ${SOURCE_FILES})

set(EXECUTABLE_NAME mytemplate)
add_executable(${EXECUTABLE_NAME} src/main.cpp)
target_link_libraries(${EXECUTABLE_NAME} user_project_lib ${CONAN_LIBS})
```

Then use this commands to install dependencies. First, you'll need to install conan.
Via pip is the easiest way, which is the python package manager.
```
sudo apt-get install pip
sudo pip install conan
conan remote add bincrafters https://api.bintray.com/conan/bincrafters/public-conan --insert
conan remote add commonlibs-repo https://api.bintray.com/conan/jmmut/commonlibs --insert
mkdir build && cd build
conan install .. --build missing
```

Now the libraries should be available, you can use some feature like this:
```
#include "utils/SignalHandler.h"

int main (int argc, char **argv) {
    // when any part of your program receives a segfault, raise an exception with the stacktrace in the exception message
    SigsegvPrinter::activate();
    // ...
}
```
And then use cmake as usual to build your project (in the build directory we created before). 
```
cd build
cmake -G "Unix Makefiles" ..
make
./bin/mytemplate
```


## For commonlibs developers
### How to release version in conan

Change the version in conanfile.py under self.version. Also change this documentation.

Then, do a branch "release/version", where version is like "0.2.5". Push to upstream. Then:
```
VERSION=0.2.5
./update-common-libs-installation.sh $VERSION
conan remote add commonlibs-repo https://api.bintray.com/conan/jmmut/commonlibs
conan upload -r commonlibs-repo --all commonlibs/${VERSION}@jmmut/testing
```
which will ask for your credentials to push to conan.

### Common commands while using commonlibs
#### Adding a small feature in commonlibs and trying it in a user project
```
# in commonlibs
VERSION=0.2.5
git push origin release/$VERSION
./update-common-libs-installation.sh $VERSION

# in user project
conan install . --build missing # or .. instead of . if you are using a build/ directory
# now recompile
```

## Metadependencies

You don't need to know about our dependencies, but it is polite to acknowledge and thank the people who make this possible.

* [SDL](http://libsdl.org/)
* [OpenGL](https://www.opengl.org/)
* [Conan](https://conan.io)
* [LodePNG](http://lodev.org/lodepng/)


## License

[LGPL](https://www.gnu.org/licenses/lgpl.txt)


