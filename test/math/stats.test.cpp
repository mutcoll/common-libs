/**
 * @file stats.test.cpp
 * @author jmmut 
 * @date 2016-04-17.
 */

#include <vector>
#include <cmath>
#include "randomize/utils/test/TestSuite.h"
#include "randomize/math/Stats.h"
const double DELTA_DOUBLE = 0.00001;

int avg(int argc, char ** argv) {
    std::vector<int> elems = {
            1, 2, 3, 4
    };

    return randomize::math::average(elems.begin(), elems.end(), 0) != 2.5;
}

int variance (int argc, char ** argv) {
    std::vector<int> elems = {
            1, 2, 3, 4
    };

    double avg = randomize::math::average(elems.begin(), elems.end(), 0);
    double var = randomize::math::variance(elems.begin(), elems.end(), avg);

    return std::abs(var - 1.25) > DELTA_DOUBLE;
}

int stddev (int argc, char ** argv) {
    std::vector<int> elems = {
            1, 2, 3, 4
    };

    double avg = randomize::math::average(elems.begin(), elems.end(), 0);
    double sdev = randomize::math::standardDeviation(elems.begin(), elems.end(), avg);

    return std::abs(sdev - 1.118033988749895) > DELTA_DOUBLE;
}

int main (int argc, char ** argv) {

    randomize::utils::test::TestSuite suite(argc, argv, {
            avg, variance, stddev
    }, "Stats");

    return suite.countFailed();
}
