/*********************************************************
/
/	Nombre Proyecto:           Eficiencia de accesos
/
/	Autor/es:      Jacobo Coll Moragón
/	Descripción:   Eficiencia de la mathE.h frente a math.h
/	Fecha:         mié oct 12 08:08:05 CEST 2011
/
/.**********************************************************/


/**
* AL COMPILAR USAR -lrt
*/
#include <iostream>
#define depurar
#include "randomize/common-libs/src/math/mathE.h"


using namespace std;
using namespace randomize::math::mathE;

int main (int argc, char const* argv[])
{
	struct timespec ini_time, fin_time;
	
	double time;
	int repeticiones = 1000000;
	
	float a = 2.35234523465;
	float b = 11234523154.1243512345;
	int resul;
	int aa = 123542134;
	int bb = 15436236;
	
	mE_Init(5);


	for(int f = 0; f < 200 ; f++ )
	{
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &ini_time);
		
		
		for(unsigned i = 1; i <= repeticiones; i++)
		{
			//mE_Init(100);
			//delete[] mE_SIN;
		
			resul = aa%bb;
			//cout << resul << endl;
		
		}
		//cout << sin(a) << resul;
		clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &fin_time);
	
		time = (fin_time.tv_sec - ini_time.tv_sec) + (fin_time.tv_nsec * 1.0e-9 - ini_time.tv_nsec * 1.0e-9);
		
		cout << f << " " << time / repeticiones << endl;
	}
	return 0;
}




