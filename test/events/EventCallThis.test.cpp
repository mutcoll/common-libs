#include "randomize/events/EventCallThis.h"
#include <iostream>

int main(int argc, char *argv[]){
	SDL_Event event;
	SDL_Init( SDL_INIT_VIDEO);
	std::string str = "hola\n";

	EventCallThis::pushEvent( [&str] (){ std::cout << str;});
	std::cout << EventCallThis::eventType << std::endl;

	SDL_WaitEvent( &event);
	std::cout << event.type << std::endl;
	if( event.type == EventCallThis::eventType){
		((EventCallThis::wraper*) event.user.data1)->ff();
		delete (EventCallThis::wraper*) event.user.data1;
	}

	SDL_Quit();
	return 0;
}

