#include "randomize/events/Dispatcher.h"

Dispatcher *gd;

void manejador( SDL_Event &event){
	gd->endDispatcher();
}

bool tester( SDL_Event &event){
	return event.type == SDL_QUIT;
}

Uint32 timer( Uint32 interval){
	gd->endDispatcher();
	return interval;
}

struct Manejador;
Manejador *teclSalir;

struct Manejador{
	Dispatcher *dispatcher;
	bool activo;
	Uint32 key;
	void manejador( SDL_Event &event){ if( key == SDLK_q) dispatcher->endDispatcher(); else teclSalir->activo = true;}
	bool tester( SDL_Event &event){ return activo && event.type == SDL_KEYDOWN && event.key.keysym.sym == key;}
	Uint32 timer( Uint32 interval){ teclSalir->activo = true; return interval;}
};

int main(){
	Dispatcher dispatcher;
	Manejador teclado1, tecladoSalir;
	gd = teclado1.dispatcher = tecladoSalir.dispatcher = &dispatcher;
	teclado1.activo = true; tecladoSalir.activo = false;
	teclado1.key = SDLK_g; tecladoSalir.key = SDLK_q;
	teclSalir = &tecladoSalir;
	int timerHandlerLargo;

	SDL_Window *window = SDL_CreateWindow( "Ventana Demo", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

	dispatcher.addEventHandler( manejador, tester);
	dispatcher.addEventHandler( [&teclado1, &timerHandlerLargo, &dispatcher] (SDL_Event &e){
			teclado1.manejador( e); dispatcher.removeTimerHandler( timerHandlerLargo);},
		[&teclado1] (SDL_Event &e){ return teclado1.tester( e);});
	dispatcher.addEventHandler( [&tecladoSalir] (SDL_Event &e){ tecladoSalir.manejador( e);}, [&tecladoSalir] (SDL_Event &e){ return tecladoSalir.tester( e);});
	timerHandlerLargo = dispatcher.addTimerHandler( timer, 30000);
	dispatcher.addTimerHandler( [&tecladoSalir] (Uint32 t){ tecladoSalir.timer( t); return t;}, 10000);
	dispatcher.startDispatcher();

	exit(0);
	return 0;
}
