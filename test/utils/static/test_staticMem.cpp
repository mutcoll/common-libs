

#include <iostream>
//#include "Log_fake.cpp"
using namespace std;
#include "randomize/common-libs/src/utils/static/StaticMem.h"
#include "randomize/common-libs/src/utils/static/StaticVec.h"

void t1()
{
		
	StaticMem<int, 5, 2> mem;
	int id = 1;
	int i = mem.takeMem(id);
	mem.takeMem(id);
	mem.takeMem(id);
	mem.takeMem(id);
	int j =mem.takeMem(id);

	cout << i << " " << j << endl;
	
	mem.freeMem(i, id);
	mem.freeMem(j, id);
	
	j = mem.takeMem(id);
	i = mem.takeMem(id);
	cout << i << " " << j << endl;

}

void t2()
{
		
	StaticMem<int, 5, 2> mem;
	StaticVec<int, 5, 2, 20> vec;
	

	StaticVec<int, 5, 2, 20>::setMem(&mem);
	
	cout << vec.getLength() << endl;
	vec.crece(10);
	
	for(int  i = 0; i < vec.getLength(); i++)
		vec[i] = i;
		
	cout << vec.getLength() << endl;
	vec.decrece();
	
	cout << vec.getLength() << endl;
	vec.decrece();
	
	cout << vec.getLength() << endl;
	vec.decrece();
	cout << vec.getLength() << endl;
	vec.crece();
	cout << vec.getLength() << endl;
	vec.crece();
	cout << vec.getLength() << endl;
	vec.crece();
	cout << vec.getLength() << endl;
	
	for(int  i = 0; i < vec.getLength(); i++)
		cout << vec[i]  << "," ;
	cout << endl;
	
	cout << mem.num_free << endl;
	vec.free();
	cout << mem.num_free << endl;
}

void t3()
{
		
	StaticMem<int, 5, 2> mem;
	int id[5];
	int memid = 2;
	
	for(int i = 0; i < 5; i++)
		id[i]=mem.takeMem(memid);

	/****/
	for(int i = 0; i < 5; i++)
		cout << id[i] << " ";
	cout << endl;
	for(int i = 0; i < 5; i++)
		cout << mem.free[i] << " ";
	cout << " << " << mem.num_free << " " << endl<<endl;
	/****/

	mem.freeMem(3, memid);
	mem.freeMem(2, memid);
	id[3] = 0;
	id[2] = 0;
	/****/
	for(int i = 0; i < 5; i++)
		cout << id[i] << " ";
	cout << endl;
	for(int i = 0; i < 5; i++)
		cout << mem.free[i] << " ";
	cout << " << " << mem.num_free << " " << endl<<endl;
	/****/
	
	id[3] = mem.takeMem(memid);
	id[2] = mem.takeMem(memid);

	/****/
	for(int i = 0; i < 5; i++)
		cout << id[i] << " ";
	cout << endl;
	for(int i = 0; i < 5; i++)
		cout << mem.free[i] << " ";
	cout << " << " << mem.num_free << " " << endl<<endl;
	/****/

}

int main()
{
	//t1();
	//cout << "_____________________" << endl;
	//t2();
	//cout << "_____________________" << endl;
	t3();
}

