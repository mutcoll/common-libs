/**
 * @file StackTracedException.test.cpp
 * @author jmmut 
 * @date 2016-03-26.
 */

#ifdef WIN32
int main(int argc, char** argv) {
	return 0;
}
#else

#include <iostream>
#include "randomize/utils/SignalHandler.h"
#include "randomize/utils/test/TestSuite.h"


using namespace std;

int func_a(int entero) {
    cout << "I'm at func_a. ";
    int a = 42;
    cout << "print numbers to avoid optimizing out: " << a << endl;
    cout << *(int*)0;
    return 0;
}

int func_b(float flotante) {
    cout << "I'm at func_b. ";
    int a = 42;
    cout << "print numbers to avoid optimizing out: " << a << endl;
    func_a(4);
    return 0;
}

int stack (int argc, char ** argv) {
    SigsegvPrinter::activate();
    try {
        func_b(4.5);
    } catch (randomize::utils::exception::StackTracedException se) {
        cout << "caught exception: " << se.what() << endl;
    }
    return 0;
}

int func_c(float flotante) {
    cout << "I'm at func_c. ";
    int  * a = new int(42);
    cout << "print numbers to avoid optimizing out: " << *a << endl;
    delete a;
    delete a;
    return 0;
}

int aborted (int argc, char ** argv) {
    SignalHandler::activate(SIGABRT);
    try {
        func_c(4.5);
    } catch (randomize::utils::exception::StackTracedException se) {
        cout << "caught exception: " << se.what() << endl;
    }
    return 0;
}

/**
 * this is not called in the suite! it does not fit, as it has to finish the program.
 * if you change the SignalHandler, uncomment the line in the main() to test it.
 */
int abortProgram (int argc, char ** argv) {
    cout << "I'm at abortProgram. " << endl;
    SignalHandler::activate(SIGABRT);
    cout << "before calling func_c" << endl;
    func_c(4.5);
    return 0;
}

int main (int argc, char ** argv) {

    randomize::utils::test::TestSuite suite(argc, argv, {
            stack, aborted
//            stack//, aborted
    }, "StackTracedException");

//    abortProgram(argc, argv);

    return suite.countFailed();
}

#endif
