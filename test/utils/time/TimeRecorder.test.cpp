//
// Created by jmmut on 2015-04-09.
//

#include <iostream>
#include <sstream>
#include "randomize/utils/time/TimeRecorder.h"

using std::cout;
using std::endl;
using randomize::utils::time::TimeRecorder;

int main () {
    TimeRecorder timeRecorder;
    timeRecorder.start();
    timeRecorder.stop();
    cout << timeRecorder << endl;

    timeRecorder >> cout << endl;

    std::stringstream ss;
    ss << "using a string stream, ";
    timeRecorder >> ss;
    ss << ", and again, " << timeRecorder;
    cout << ss.str() << endl;

    cout << timeRecorder.toString("<insert your description here>") << endl;

    return 0;
}
