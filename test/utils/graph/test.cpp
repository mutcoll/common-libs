/**************************************************
 *
 * autor: Jose Miguel Mut
 *
 * fecha: 2013/12/19 20:31:59
 *
 * descripcion: timetest.cpp
 *		arriba defines funciones, y pones una llamada en el main real
 *		Utiliza asserts para parar el programa (y ver donde ha parado) 
 *			cuando algo no va como lo habias programado.
 *
 ***************************************************/

#include <cstdio>
#include <iostream>
#include <tuple>
#include "randomize/utils/test/TestSuite.h"
#include "randomize/utils/graph/Digraset.h"
#include "randomize/math/Vector3D.h"


#include "randomize/utils/graph/Undirgravec.h"
#include "randomize/utils/graph/Digraph.h"
#include "randomize/utils/test/Test.h"

using namespace std;
using namespace randomize::utils::test;
using namespace randomize::utils::graph;


int add_edge_vec(int argc, char **argv) {
    Undirgravec<int, float> g;
    float *edge;
    int ok = 0;

    ok += g.addNode(0);
    ok += g.addNode(1);
    ok += g.addNode(2);
    ok += g.addNode(3);
    ok += g.addNode(4);
    ok += g.addEdge(0, 4, 0.3);
    ok += g.addEdge(1, 4, 0.4);
    ok += g.addEdge(2, 4, 0.5);
    ok += g.addEdge(3, 4, 0.6);
    ok += g.addEdge(1, 3, 0.7);
    ok += g.addEdge(1, 2, 0.8);
    ok += g.addEdge(0, 1, 0.9);

    ASSERT_OR_THROW_MSG(ok == 0, "graph fill up went wrong");

    g.printGraph(cout);

    g.getEdge(4, 2, edge);
    ASSERT_OR_THROW_MSG(*edge == 0.5, "the edge should be 0.5");
    ASSERT_OR_THROW(g.getNodesCount() == 5);

    g.clear();

    ASSERT_OR_THROW(g.getNodesCount() == 0);

    return ok;
}


int rem_vec(int argc, char **argv) {
    Undirgravec<int, float> g;
    float e;
    int fails = 0;

    fails += g.addNode(0);
    fails += g.addNode(1);
    fails += g.addNode(2);
    fails += g.addNode(3);
    fails += g.addNode(4);
    fails += g.addEdge(0, 4, 0.3);
    fails += g.addEdge(1, 4, 0.4);
    fails += g.addEdge(2, 4, 0.5);
    fails += g.addEdge(3, 4, 0.6);
    fails += g.addEdge(1, 3, 0.7);
    fails += g.addEdge(1, 2, 0.8);
    fails += g.addEdge(0, 1, 0.9);

    ASSERT_OR_THROW(fails == 0);

    g.printGraph(cout);

    return fails;
}

struct Node {
    int pos[3];
};
struct Edge {
    float pos[3];
};

const int NODE_NUM = 1000;
const int EDGE_NUM = 10000;

Undirgravec<Node, Edge> gv;
int vecNode(int argc, char **argv) {
    Node node{0, 0, 0};

    for (int i = 0; i < NODE_NUM; ++i) {
        gv.addNode(node);
    }
    ASSERT_OR_THROW(gv.getNodesCount() == NODE_NUM);
    return 0;
}
int vecEdge(int argc, char **argv) {
    Edge edge{0, 0, 0};

    int ok = 0;
    int bad_nodes = 0;
    int ret = 0;
    int edge_exists = 0;
    int reflexives = 0;
    for (unsigned int i = 0; i < EDGE_NUM; ++i) {
        ret = gv.addEdge(i*46695%NODE_NUM, i*39415%NODE_NUM, edge);
        ok += ret;
        if (ret == -1) {
            reflexives ++;
        }
        if (ret == -2) {
            bad_nodes ++;
        }
        if (ret == -3) {
            edge_exists ++;
        }
    }
    if (bad_nodes) {
        cout << bad_nodes << " bad nodes" << endl;
    }
    ASSERT_OR_THROW(bad_nodes == 0);
    if (edge_exists) {
        cout << edge_exists << " edges exist" << endl;
    }
    const int EDGE_EXISTS = 9408;
    ASSERT_OR_THROW(edge_exists == EDGE_EXISTS);

    const int EDGE_COUNT = (EDGE_NUM - EDGE_EXISTS - reflexives) * 2;
    ASSERT_OR_THROW_MSG(gv.getEdgesCount() == EDGE_COUNT,
            std::to_string(gv.getEdgesCount()) + " != " + std::to_string(EDGE_COUNT));
    return 0;
}
int vecVisitNode(int argc, char **argv) {
    gv.forEachNode([](Node &node) {
        node.pos[1]++;
    });
    return 0;
}
int vecVisitEdge(int argc, char **argv) {
    gv.forEachEdge([](Edge &edge){
        edge.pos[0]++;
    });
    return 0;
}
int performanceVec(int argc, char **argv) {
    int fails = 0;

    fails += time_test(vecNode, argc, argv);
    fails += time_test(vecEdge, argc, argv);
    fails += time_test(vecVisitNode, argc, argv);
    fails += time_test(vecVisitEdge, argc, argv);

    Node node;
    gv.getNode(0, node);
    ASSERT_OR_THROW(node.pos[1] == 1);
    ASSERT_OR_THROW(fails == 0);
    return fails;
}
/*
Undirgraph<Node, Edge> gm;

int mapNode(int argc, char **argv) {
    Node node{0, 0, 0};

    for (int i = 0; i < NODE_NUM; ++i) {
        gm.addNode(node);
    }
}
int mapEdge(int argc, char **argv) {
    Edge edge{0, 0, 0};

    for (unsigned int i = 0; i < EDGE_NUM; ++i) {
        gm.addEdge(i*46695%NODE_NUM, i*39415%NODE_NUM, edge);
    }
}
int mapVisitNode(int argc, char **argv) {
    gm.forEachNode([](Node &node) {
        node.pos[1]++;
    });
}
int mapVisitEdge(int argc, char **argv) {
    gm.forEachEdge([](Edge &edge){
        edge.pos[0]++;
    });
}
int performanceMap(int argc, char **argv) {
    int ret = 0;

    ret += time_test(mapNode, argc, argv);
    ret += time_test(mapEdge, argc, argv);
    ret += time_test(mapVisitNode, argc, argv);
    ret += time_test(mapVisitEdge, argc, argv);

    return ret;
}
*/

template<typename T>
std::string digrasetHash(Digraset<T> d) {
    std::stringstream accumulation;
    char separator = '_';
    d.walk([&] (typename Digraset<T>::NodeWrapperPtr node) {
        accumulation << node->node << separator;
    }, [&](typename Digraset<T>::NodeWrapperPtr source, typename Digraset<T>::NodeWrapperPtr sink) {
        accumulation << "{" << source->node << separator << sink->node << "}" << separator;
    });
    return accumulation.str();
}

int digraset(int argc, char **argv) {
    Digraset<int> digraset;
    bool ok = true;
    ok &= digraset.addNode(5);
    ok &= digraset.addNode(6);
    ok &= digraset.addNode(7);
    ok &= digraset.addConnection(5, 7);
    ASSERT_OR_THROW(ok);

    auto found5 = digraset.findNode(5);
    auto found6 = digraset.findNode(6);
    auto found7 = digraset.findNode(7);
    ASSERT_OR_THROW(found5.first);
    ASSERT_OR_THROW(found5.second != nullptr);
    ASSERT_OR_THROW(found6.first);
    ASSERT_OR_THROW(found6.second != nullptr);

    ok &= digraset.addConnection(5, 6);
    auto foundConnection = digraset.findConnection(5, 6);
    ASSERT_OR_THROW(foundConnection.first);
    ASSERT_OR_THROW(foundConnection.second.first!= nullptr);
    ASSERT_OR_THROW(foundConnection.second.second != nullptr);
    ASSERT_OR_THROW(found5.second->predecessors.size() == 0);
    ASSERT_OR_THROW(found5.second->successors.size() == 2);
    ASSERT_OR_THROW(found6.second->predecessors.size() == 1);
    ASSERT_OR_THROW(found6.second->successors.size() == 0);
    ASSERT_OR_THROW(found7.second->predecessors.size() == 1);
    ASSERT_OR_THROW(found7.second->successors.size() == 0);

    int visited = 0;
    for (auto node : digraset) {
        visited++;
    }
    ASSERT_OR_THROW(visited == 3);
    digraset.print(cout);

    Digraset<int> other(digraset);
    auto hashFirst = digrasetHash(digraset);
    auto hashSecond = digrasetHash(other);
    cout << "first:" << hashFirst << ", second:" << hashSecond << endl;
    ASSERT_OR_THROW(hashFirst == hashSecond);

    Digraset<int> third;
    third.addNode(2);
    third.addGraph(digraset);
    auto hashThird = digrasetHash(third);
    cout << "first:" << hashFirst << ", third:" << hashThird << endl;
    ASSERT_OR_THROW(hashFirst != hashThird);

    ASSERT_OR_THROW(digraset.removeConnection(found5.second, found6.second));
    ASSERT_OR_THROW(digraset.removeNode(found5.second));
    visited = 0;
    for (auto node : digraset) {
        visited++;
    }
    ASSERT_OR_THROW(visited == 2);
    ASSERT_OR_THROW(not digraset.findConnection(5, 7).first);
    ASSERT_OR_THROW(not digraset.findConnection(5, 6).first);
    ASSERT_OR_THROW(not digraset.findConnection(6, 7).first);

    digraset.print(cout);


    return not ok;
}

//////////////////////////////////////////////



int main(int argc, char **argv) {
    int failed = 0;

    {
        TestSuite suite(argc, argv, {
                add_edge_vec, rem_vec, performanceVec
        }, "undirgraph");
        failed += suite.countFailed();
    }

    {
        TestSuite suite(argc, argv, {
                digraset
        }, "digraph");
        failed += suite.countFailed();
    }
    {

        TestSuite difference(argc, argv, {
                [](int argc, char **argv) -> int {
                    int fails = 0;
                    std::string treeName = "tree";
                    std::string leafName = "leaf";
                    std::string pigName = "pig";
                    Digraset<std::string> tree, tree2, pig, empty;
                    tree.addNode(treeName);
                    pig.addNode(pigName);
                    ASSERT_OR_THROW(tree.size() == 1);
                    ASSERT_OR_THROW(tree.findNode(treeName).first);

                    tree2 = tree - empty;
                    ASSERT_OR_THROW(tree2.size() == 1);
                    ASSERT_OR_THROW(tree2.findNode(treeName).first);
                    tree2.addNode(leafName);

                    ASSERT_OR_THROW((tree - tree).size() == 0);
                    ASSERT_OR_THROW((tree - pig).size() == 1);
                    ASSERT_OR_THROW((tree - pig).findNode(treeName).first);
                    ASSERT_OR_THROW((pig - tree).findNode(pigName).first);

                    ASSERT_OR_THROW(empty < tree);

                    // same set size, "pig" is lexicographically less than "tree"
                    ASSERT_OR_THROW(pig < tree);

                    // operator< transitivity
                    ASSERT_OR_THROW(empty < tree);
                    ASSERT_OR_THROW(tree < tree2);
                    ASSERT_OR_THROW(empty < tree2);

                    // operator< non commutativity
                    ASSERT_OR_THROW((tree - tree2).size() == 0);
                    ASSERT_OR_THROW((tree2 - tree).size() == 1);
                    ASSERT_OR_THROW((tree2 - tree).findNode(leafName).first);

                    // operator^ commutativity
                    ASSERT_OR_THROW((tree2 ^ tree).size() == 1);
                    ASSERT_OR_THROW((tree ^ tree2).size() == 1);
                    ASSERT_OR_THROW((tree2 ^ tree).findNode(treeName).first);


                    return fails;
                }}, "Digraset set operations");
        failed += difference.countFailed();
    }

    return failed;
}

