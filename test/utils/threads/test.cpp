#include <iostream>
#include <mutex>

#include "randomize/common-libs/src/utils/threads/runnable.h"
#include "randomize/common-libs/src/utils/threads/runnable_posix.h"



using namespace std;

class foo
{
	public:
		foo(int A):a(A){}
		
		int a;
		int c[10];
};

std::recursive_mutex m;
int j = 0;
void func(int x,int y) 
{ 
	cout << x << " " << y << endl;
	m.lock();
	j++; // j is now 1, no matter the thread. j is local to this thread.
	cout << j << endl;
	m.unlock();
}
int func2()
{
	cout << "Hola Mundo"<< endl;
	return 4;
}


 int fb(int n)
{
	return n<=2?1:fb(n-1)+fb(n-2);
}
float fb_f(float n)
{
	int i = n > 5 ? 1 : -1 ;
	return n<=2?1:fb_f(n-1)+fb_f(n-2);
}

class C1 : public runnable_posix
{
	public:
		
		C1(int A):a(A)
		{}
		int getA(){return a;}
	private:
		int a;
		void run()
		{
			cout << "Fibonachi entero " << fb(42) << endl;
		}
};
class C2 : public runnable
{
	public:
		~C2()
		{
			detach();
		}
	private:
		void run()
		{
			cout << "Fibonachi flotante " << fb_f(40) << endl;
		}
};
int main(int argc, char *argv[])
{
	C1 c(2);
	c.start();
	c.detach();
	
	C2 c2;
	c2.start();

	//c2.detach();	
	//c2.join();

	c.join();
	
	cout << c.getA() << endl;
	foo f(1);
	foo f2(2);
	foo &ref_f = f;
	foo *pto_f = &f;
	ref_f = f2;
	f2.a = 3;
	cout << sizeof(f) << " " << f.a << endl;
	cout << sizeof(f2) << " " << f2.a << endl;
	cout << sizeof(ref_f) << " " << ref_f.a <<  endl;
	cout << sizeof(pto_f) << "  " << pto_f->a << endl;
	
	//fb_f(42);
	//thread t(func,1,2); // Acceptable.
	//thread t2(func,3,4); // Acceptable.
	//thread t3(func,5,6); // Acceptable.
	//thread t4(func2);
	//t4.detach();

	
	//cout << "ll" << endl;
	//t.join();  
	//t2.join();  
	//t3.join();  

	return 0;
}
//Hola Informer!

//Podrías hacer una encuesta acerca de qué tal fueron las paellas? 
//De parte del AdR queremos conocer qué le pareció a la gente el "nuevo formato" de cara al año que viene, para ver si se repite de esta manera,o hacerlo en el descampado como el año pasado
//Las preguntas que fueran en plan:
 //-Perfectas, hay que repetirlo
 //-Bien planteado, pero falta depurar
 //-Preferiría hacer mi propia paella
 //-O se hace en el descampado, o no se hace!

int laa(int &i, ...)
{
	i = 5;
}
