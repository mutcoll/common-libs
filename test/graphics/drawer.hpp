#include "randomize/events/Dispatcher.h"
#include "randomize/events/EventCallThis.h"
#include "randomize/graphics/Window.h"

class Manager_drawer{
public:
	Manager_drawer( Dispatcher &d);
	virtual ~Manager_drawer();
	void onDisplay( Uint32 ms);
private:
	Dispatcher &dispatcher;
	WindowRenderer window;
	Uint32 windowID;
	SDL_Texture *texture;
	SDL_Surface *surface;
};

Manager_drawer::Manager_drawer( Dispatcher &d): dispatcher(d){
	this->window.open();
	this->windowID = this->window.getWindowID();
	this->texture = this->window.createWindowTexture();
	this->surface = SDL_CreateRGBSurface(0,640,480,32,0,0,0,0);

	this->dispatcher.addTimerHandler( [this] (Uint32 ms){
			EventCallThis::pushEvent( [this,ms] (){ if( this->window.isOpen()){ this->onDisplay( ms);};});
			return ms;
		}, 30);
	this->dispatcher.addEventHandler(
			[this] (SDL_Event &event){ if( this->window.isOpen()){ this->window.close();};},
			[this] (SDL_Event &event){
				return event.type == SDL_WINDOWEVENT && event.window.windowID == this->windowID && event.window.event == SDL_WINDOWEVENT_CLOSE;
			}
	);
}

Manager_drawer::~Manager_drawer(){
	if( this->surface != nullptr){ SDL_FreeSurface( this->surface);};
	if( this->texture != nullptr){ SDL_DestroyTexture( this->texture);};
	if( this->window.isOpen()){ this->window.close();};
}

void Manager_drawer::onDisplay( Uint32 ms){
	// if( ms != 30); // ??
	SDL_Rect rect { 0, 0, this->surface->w, this->surface->h};
	SDL_FillRect( this->surface, &rect, SDL_MapRGB( this->surface->format, 0xff, 0x0, 0x0));
	rect.x = rect.w /= 10;
	rect.y = rect.h /= 10;
	rect.x *= SDL_GetTicks()/100  % 10;
	rect.y *= SDL_GetTicks()/1000 % 10;
	SDL_FillRect( this->surface, &rect, SDL_MapRGB( this->surface->format, 0x0, 0xff, 0x0));

	SDL_UpdateTexture( this->texture, nullptr, this->surface->pixels, this->surface->pitch);
	this->window.renderClear();
	this->window.renderCopy( this->texture);
	this->window.renderPresent();
}

