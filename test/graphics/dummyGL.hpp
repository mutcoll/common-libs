#include "randomize/events/Dispatcher.h"
#include "randomize/graphics/Window.h"
#include "randomize/graphics/drawables/Volador.h"

class Manager_dummyGL{
public:
	Manager_dummyGL( Dispatcher &d);
	~Manager_dummyGL();
	void onDisplay( Uint32 ms);
	bool isEvent( SDL_Event &event);
	void onEvent( SDL_Event &event);
private:
	void drawAxes();
	Dispatcher &dispatcher;
	WindowGL window;
	Uint32 windowID;
	Volador vldr;
    Vector3D center;
};

Manager_dummyGL::Manager_dummyGL( Dispatcher &d): dispatcher(d){
	this->window.open();
	this->windowID = this->window.getWindowID();
	this->window.initGL();
	this->vldr.setPos( Vector3D(1, 2, 10));
	this->vldr.setOrientacion( this->center - vldr.getPos(), Vector3D(0, 1, 0));

	this->dispatcher.addTimerHandler( [this] (Uint32 ms){
			EventCallThis::pushEvent( [this,ms] (){ if( this->window.isOpen()){ this->onDisplay( ms);};});
			return ms;
		}, 30);
	this->dispatcher.addEventHandler(
			[this] (SDL_Event &event){ this->onEvent( event);},
			[this] (SDL_Event &event){ return this->window.isOpen() && this->isEvent( event);});
	this->dispatcher.addEventHandler(
			[this] (SDL_Event &event){ if( this->window.isOpen()){ this->window.close();};},
			[this] (SDL_Event &event){
				return event.type == SDL_WINDOWEVENT && event.window.windowID == this->windowID && event.window.event == SDL_WINDOWEVENT_CLOSE;
			}
	);
}

Manager_dummyGL::~Manager_dummyGL(){
	if( this->window.isOpen()){ this->window.close();};
}

void Manager_dummyGL::drawAxes() {
	glPushMatrix();
	glBegin(GL_LINES);
	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(100, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 100, 0);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 100);
	glEnd();

	glPopMatrix();
}

void Manager_dummyGL::onDisplay( Uint32 ms) {
	this->window.makeCurrent();

	// Clear The Screen And The Depth Buffer
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glLoadIdentity();                // Reset The View
	this->vldr.Look();
	//	glTranslatef( 0, 0, -5.0);

	this->drawAxes();
	glPushMatrix();

	glBegin(GL_POLYGON);
	glColor4f(0, 0.5, 1, 0.2);
	glVertex3f(-1.0, 0.0, 0.0);
	glColor4f(0.5, 1, 0, 0.2);
	glVertex3f(1.0, 0.0, 0.0);
	glColor4f(1, 0, 0.5, 0.2);
	glVertex3f(0.0, 1.0, 0.0);
	glEnd();


	glPopMatrix();
	// swap buffers to display, since we're double buffered.
	this->window.swapWindow();
}

bool Manager_dummyGL::isEvent( SDL_Event &event){
	bool r = false;
	switch( event.type){
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		if( event.key.windowID == this->windowID){ r = true;};
		break;
	case SDL_MOUSEMOTION:
		if( event.motion.windowID == this->windowID){ r = true;};
		break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		if( event.button.windowID == this->windowID){ r = true;};
		break;
	}
	return r;
}

void Manager_dummyGL::onEvent( SDL_Event &event){
	switch( event.type){
	case SDL_KEYDOWN:
		break;
	case SDL_KEYUP:
		break;
	case SDL_MOUSEMOTION:
		break;
	case SDL_MOUSEBUTTONDOWN:
		break;
	case SDL_MOUSEBUTTONUP:
		break;
	}
}

