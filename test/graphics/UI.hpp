#include "randomize/events/Dispatcher.h"
#include "randomize/graphics/Window.h"

#include "randomize/UI/UI.h"
#include "randomize/UI/Element.h"
#include "randomize/UI/Button.h"
#include "randomize/UI/Boton.h"
#include "randomize/UI/Slider.h"
#include "randomize/UI/Switch.h"
#include "randomize/UI/Panel.h"
#include "randomize/UI/Text.h"
#include "randomize/UI/Text_Input.h"

using namespace std;
using namespace randomize;

class Manager_UIdummy{
public:
	Manager_UIdummy( Dispatcher &d, Uint32 w = 640, Uint32 h = 480);
	~Manager_UIdummy();
	void onDisplay( Uint32 ms);
	bool isEvent( SDL_Event &event);
	void onEvent( SDL_Event &event);
private:
	void drawAxes();
	Dispatcher &dispatcher;
	WindowGL window;
	Uint32 windowID;

	void onStep( Uint32 ms);
	void onMouseMotion(SDL_Event &e);
	void onMouseButton(SDL_Event &e);
	void onKeyPressed(SDL_Event &e);
	void onResize(int w, int h);

	UI::Panel<10> panelFullScreen;
	UI::Button botonFullScreen[5];

	UI::Panel<10> panel;
	UI::Button boton[5];

	UI::Text text;
	UI::Text_Input text_input;

	UI::Element slider_back;
	UI::Slider slider;

	unsigned int frames = 16;

	float time;
};

Manager_UIdummy::Manager_UIdummy( Dispatcher &d, Uint32 w, Uint32 h): dispatcher(d), window( w, h){
	this->window.open();
	this->windowID = this->window.getWindowID();
	this->window.initGL();
	glClearColor(0.0f, 1.0f, 0.0f, 0.0f);
	UI::setConfWindow(w,h);

	SDL_StartTextInput();
	GLuint img = LoadPNG( "../test/UI/boton.png");
	GLuint img_chars = LoadPNG( "../test/UI/letras.png");
	std::vector<unsigned char> rainbow( this->frames * 4);
	for (int i = 0; i < this->frames; ++i) {
		rainbow[i * 4 + 0] = 255 / this->frames * i;
		rainbow[i * 4 + 1] = 255 / this->frames * i;
		rainbow[i * 4 + 2] = 255 / this->frames * i;
		rainbow[i * 4 + 3] = 255;
	}
	GLuint img_rainbow = LoadImg( rainbow, this->frames, 1);

	UI::Button::Conf conf_boton1 = {img ,1,5,0,0,0,1,70,15};
	UI::Button::Conf conf_boton2 = {img ,5,5,0,2,1,2,17,17};
	UI::Button::Conf conf_boton3 = {img ,1,5,0,4,0,3,70,17};
	UI::Button::Conf conf_boton_total = {img_chars ,1,1,0,0,0,0,100,20};
	UI::Switch::Conf conf_pulsador={img ,5,5,2,2,3,2,10,10};
	UI::Slider::Conf conf_slider_fondo = {img_rainbow ,1,1,0,0,0,0,10,10};
	UI::Button::Conf conf_boton_rainbow = {img_rainbow ,(int) this->frames,1,0,0,8,0,70,15};

	SpriteChar::setTexture( img_chars);

	panel.setTam( 50, 50);
	UI::setTestBox(true);
	for(int i = 0; i < 5; i++){
		boton[i].setConfig(conf_boton1);
		boton[i].setPos(0,20*i-40);
		boton[i].setListener([](UI::Event a, int x, int y, UI::Element* t, void* ud){
			if(a==UI::UI_EVENT_DOWN){
				LOG_INFO("Pulsado Button: %p", t);
			}
		}, nullptr);
		panel.addElemento(&(boton[i]),i);
	}
	boton[4].setTam(50,50);
	boton[4].setConfig(conf_boton_total);

	boton[0].setTamPixel(200, 50);

	bool *user_data = new bool;
	*user_data = true;
	boton[1].setListener([](UI::Event e, int a, int b, UI::Element *l, void * data) {
		bool * siono = (bool *) data;
		if (e == UI::UI_EVENT_DOWN) {
			*siono = !*siono;
		}
		UI::setTestBox((*siono));
	}, user_data);
	panel.addElemento(&text, 5);
	text.setTamAuto(true);

	text.setFrase("Hola Mundo");

	panel.addElemento(&text_input, 6);
	text_input.setTam(50,50);
	text_input.setPos(0,70);
	text_input.setTamAuto(true);
	text_input.setMaxLength(50);

	slider_back.addCapa(0, img_rainbow, 1, 1);
	slider_back.setTam(100, 10);
	slider_back.setPos(0, -50);
	panel.addElemento(&slider_back, 7);
	slider.setConfig(conf_boton_rainbow);
	slider.setButtonFrames(0, 0, 0, 0);
	slider.setTam(10, 20);
	slider.setFree(true, false);
	slider.setPos(0, -50);
	panel.addElemento(&slider, 8);


	for (int i = 0; i < 5; ++i) {
		botonFullScreen[i].setConfig(conf_pulsador);
		botonFullScreen[i].setTamPixel(30, 30);
		botonFullScreen[i].setPosPixel(i * 50 + 200, 50);
		botonFullScreen[i].setListener([](UI::Event event, int x, int y, UI::Element*, void*) {
			cout << "X: " << x << " Y:" << y << endl;
		}, nullptr);
		panelFullScreen.addElemento(&botonFullScreen[i], i);
	}
	botonFullScreen[0].setPosPixel(0, 0);
	botonFullScreen[1].setPosPixel(w, 0);
	botonFullScreen[4].setPosPixel(w/2, h/2);

	this->dispatcher.addTimerHandler( [this] (Uint32 ms){
			EventCallThis::pushEvent( [this,ms] (){ if( this->window.isOpen()){ this->onDisplay( ms);};});
			return ms;
		}, 30);
	this->dispatcher.addTimerHandler( [this] (Uint32 ms){
			EventCallThis::pushEvent( [this,ms] (){ if( this->window.isOpen()){ this->onStep( ms);};});
			return ms;
		}, 30);
	this->dispatcher.addEventHandler(
			[this] (SDL_Event &event){ this->onEvent( event);},
			[this] (SDL_Event &event){ return this->window.isOpen() && this->isEvent( event);});
	this->dispatcher.addEventHandler(
			[this] (SDL_Event &event){ if( this->window.isOpen()){ this->window.close();};},
			[this] (SDL_Event &event){
				return event.type == SDL_WINDOWEVENT && event.window.windowID == this->windowID && event.window.event == SDL_WINDOWEVENT_CLOSE;
			}
	);
}

Manager_UIdummy::~Manager_UIdummy(){
	if( this->window.isOpen()){ this->window.close();};
}

void Manager_UIdummy::onStep( Uint32 ms){
	time += ms / 1000.0;
}


void Manager_UIdummy::onDisplay( Uint32 ms) {
	this->window.makeCurrent();
	glClearColor(0.0f, 1.0f, 0.0f, 0.0f);

	// Clear The Screen And The Depth Buffer
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	glLoadIdentity();                // Reset The View
	glPushMatrix();
	glPopMatrix();

	//Slider
	int frame = (int) (slider.getX() * (frames - 1));
	slider.setButtonFrames(frame, 0, frame, 0);

	//Una botonera que se mueve
	panel.setPos(100*sin(-time), 0);
	panel.setRotation(-time*10);
	boton[3].setRotation(-time*30);
	panel.setTam(50,30+20*sin(time/2.0));

	//glDisable(GL_TEXTURE_2D);
	panel.Draw();

	panel.setPos(0, 0);
	panel.setRotation(0);
	panel.setTam(50,50);

	//glEnable(GL_TEXTURE_2D);
	panel.Draw();
	panelFullScreen.Draw();

	// swap buffers to display, since we're double buffered.
	this->window.swapWindow();
}

bool Manager_UIdummy::isEvent( SDL_Event &event){
	bool r = false;
	switch( event.type){
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		if( event.key.windowID == this->windowID){ r = true;};
		break;
	case SDL_MOUSEMOTION:
		if( event.motion.windowID == this->windowID){ r = true;};
		break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		if( event.button.windowID == this->windowID){ r = true;};
		break;
	case SDL_WINDOWEVENT:
		if( event.window.windowID == this->windowID && event.window.event == SDL_WINDOWEVENT_RESIZED){ r = true;};
		break;
	case SDL_TEXTINPUT:
		if( event.text.windowID == this->windowID){ r = true;};
	case SDL_TEXTEDITING:
		if( event.edit.windowID == this->windowID){ r = true;};
	}
	return r;
}

void Manager_UIdummy::onEvent( SDL_Event &event){
	switch( event.type){
	case SDL_KEYDOWN:
		this->onKeyPressed( event);
		break;
	case SDL_KEYUP:
		break;
	case SDL_MOUSEMOTION:
		this->onMouseMotion( event);
		break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		this->onMouseButton( event);
		break;
	case SDL_WINDOWEVENT:
		this->onResize( event.window.data1, event.window.data2);
		break;
	case SDL_TEXTINPUT:
		for(int i = 0; i < strlen(event.text.text); i++){
			text_input.addChar(event.text.text[i]);
		}
		break;
	case SDL_TEXTEDITING:
		cout << "_________________________________Text Edit : " << event.edit.text << endl;
		break;
	}
}

void Manager_UIdummy::onMouseMotion(SDL_Event &e){
	panel.CB_Click( UI::UI_EVENT_MOVE, e.button.x,  e.button.y);
}

void Manager_UIdummy::onMouseButton(SDL_Event &e){
	if(e.button.type == SDL_MOUSEBUTTONDOWN) {
        panel.CB_Click(UI::UI_EVENT_DOWN, e.button.x, e.button.y);
        panelFullScreen.CB_Click(UI::UI_EVENT_DOWN, e.button.x, e.button.y);
    } else {
        panel.CB_Click( UI::UI_EVENT_UP, e.button.x,  e.button.y);
        panelFullScreen.CB_Click( UI::UI_EVENT_UP, e.button.x,  e.button.y);
    }
}

void Manager_UIdummy::onKeyPressed(SDL_Event &e){
	if(e.type == SDL_KEYDOWN){
		switch(e.key.keysym.sym){
			case SDLK_BACKSPACE:
				text_input.addChar(SpriteChar::Keycode::BACK);
				break;
		}
	}
}

void Manager_UIdummy::onResize(int w, int h){
	this->window.initGL();
	glClearColor(0.0f, 1.0f, 0.0f, 0.0f);
	UI::setConfWindow(w,h);
	boton[0].setTamPixel(200, 50);
	panelFullScreen.setLayout(UI::Layout::LAYOUT_CENTER);
	botonFullScreen[0].setPosPixel(0, 0);
	botonFullScreen[1].setPosPixel(w, 0);
	botonFullScreen[4].setPosPixel(w/2, h/2);

	botonFullScreen[0].setTamPixel(30, 30);
	botonFullScreen[1].setTamPixel(30, 30);
	botonFullScreen[2].setTamPixel(30, 30);
	botonFullScreen[3].setTamPixel(30, 30);
	botonFullScreen[4].setTamPixel(30, 30);
}

