#include "randomize/events/Dispatcher.h"
#include "randomize/events/AutoCallThis.h"
#include "drawer.hpp"
#include "dummyGL.hpp"
#include "UI.hpp"

int main(){
	Dispatcher dispatcher;
	dispatcher.addEventHandler( EventCallThis::autoCallHandler, EventCallThis::autoCallTester);
	dispatcher.addEventHandler( [&dispatcher] (SDL_Event &event){ dispatcher.endDispatcher();}, [] (SDL_Event &event){ return event.type == SDL_QUIT;});

//	Manager_drawer drawer( dispatcher), drawer2( dispatcher);
	Manager_dummyGL dummyGL( dispatcher), dummy2( dispatcher);
	Manager_UIdummy uiDummy( dispatcher);

	dispatcher.startDispatcher();

	exit(0);
	return 0;
}

