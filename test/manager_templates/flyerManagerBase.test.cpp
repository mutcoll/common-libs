#include "randomize/events/Dispatcher.h"
#include "randomize/events/EventCallThis.h"
#include "randomize/manager_templates/FlyerManagerBase.h"

class TestManager: public FlyerManagerBase{
public:
	TestManager( Dispatcher &d, std::shared_ptr< WindowGL> w): FlyerManagerBase( d, w){};
protected:
	virtual void onDisplay( Uint32 ms){
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glPushMatrix();

		this->FlyerManagerBase::onDisplay( ms);

		glBegin(GL_LINES);
		glColor3f(1, 0, 0);
		glVertex3f(0, 0, 0);
		glVertex3f(100, 0, 0);

		glColor3f(0, 1, 0);
		glVertex3f(0, 0, 0);
		glVertex3f(0, 100, 0);

		glColor3f(0, 0, 1);
		glVertex3f(0, 0, 0);
		glVertex3f(0, 0, 100);

		glColor3f(1, 1, 0);
		glVertex3f(0, 0, 0);
		glVertex3f(1, 1, 0);
		glVertex3f(1, 1, 0);
		glVertex3f(0, -1, 0);
		glVertex3f(0, -1, 0);
		glVertex3f(0, 0, 1);
		glEnd();
		glPopMatrix();
		std::static_pointer_cast< WindowGL>( this->window.lock())->swapWindow();
	}
};

int main(){
	Dispatcher dispatcher;
	dispatcher.addEventHandler( [&dispatcher] (SDL_Event &event){ dispatcher.endDispatcher();}, [] (SDL_Event &event){ return event.type == SDL_QUIT;});

	std::shared_ptr< WindowGL> window = std::make_shared< WindowGL>();
	window->open();
	window->initGL();
	dispatcher.addEventHandler( [&dispatcher,&window] (SDL_Event &event){ window->close();}, [windowID = window->getWindowID()] (SDL_Event &event){
		return event.type == SDL_WINDOWEVENT && event.window.windowID == windowID && event.window.event == SDL_WINDOWEVENT_CLOSE;
	});

	TestManager manager( dispatcher, window);
	manager.activate();

	dispatcher.startDispatcher();

	exit(0);
	return 0;
}

