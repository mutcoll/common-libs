#include "randomize/events/Dispatcher.h"
#include "randomize/events/EventCallThis.h"
#include "randomize/manager_templates/SimpleManagerBase.h"

int main(){
	Dispatcher dispatcher;
	dispatcher.addEventHandler( [&dispatcher] (SDL_Event &event){ dispatcher.endDispatcher();}, [] (SDL_Event &event){ return event.type == SDL_QUIT;});

	std::shared_ptr< WindowGL> window = std::make_shared< WindowGL>(), window2 = std::make_shared< WindowGL>();
	window->open();
	dispatcher.addEventHandler( [&dispatcher,&window] (SDL_Event &event){ window->close();}, [windowID = window->getWindowID()] (SDL_Event &event){
		return event.type == SDL_WINDOWEVENT && event.window.windowID == windowID && event.window.event == SDL_WINDOWEVENT_CLOSE;
	});
	window2->open();
	dispatcher.addEventHandler( [&dispatcher,&window2] (SDL_Event &event){ window2->close();}, [windowID = window2->getWindowID()] (SDL_Event &event){
		return event.type == SDL_WINDOWEVENT && event.window.windowID == windowID && event.window.event == SDL_WINDOWEVENT_CLOSE;
	});

	SimpleManagerBase platformer( dispatcher, window);

	dispatcher.startDispatcher();

	exit(0);
	return 0;
}

