

#include <iostream>
#include "randomize/log/log.h"

#include "randomize/graphics/Sprite.h"
#include "randomize/graphics/SpriteChar.h"
#include "randomize/UI/UI.h"
#include "randomize/UI/Element.h"
#include "randomize/UI/Button.h"
#include "randomize/UI/Boton.h"
#include "randomize/UI/Slider.h"
#include "randomize/UI/Switch.h"
#include "randomize/UI/Panel.h"
#include "randomize/UI/Text.h"
#include "randomize/UI/Text_Input.h"


#include "randomize/vgm/Ventana.h"
#include "randomize/utils/semaforo/Semaforo.h"

#include "randomize/graphics/TextureLoader.h"

#undef main

using namespace randomize;


class Gestor : public Ventana{
	public:
		Gestor(int w, int h):Ventana(w,h){
			onResize(w,h);

            SDL_StartTextInput();
			time = 0;
			setFps(30);
			GLuint img = LoadPNG("randomize/common-libs/test/UI/boton.png");
			GLuint img_chars = LoadPNG("randomize/common-libs/test/UI/letras.png");
			std::vector<unsigned char> rainbow(frames * 4);
			for (int i = 0; i < frames; ++i) {
				rainbow[i * 4 + 0] = 255 / frames * i;
				rainbow[i * 4 + 1] = 255 / frames * i;
				rainbow[i * 4 + 2] = 255 / frames * i;
				rainbow[i * 4 + 3] = 255;
			}
			GLuint img_rainbow = LoadImg(rainbow, frames, 1);

			UI::Button::Conf conf_boton1 = {img ,1,5,0,0,0,1,70,15};
			UI::Button::Conf conf_boton2 = {img ,5,5,0,2,1,2,17,17};
			UI::Button::Conf conf_boton3 = {img ,1,5,0,4,0,3,70,17};
			UI::Button::Conf conf_boton_total = {img_chars ,1,1,0,0,0,0,100,20};
			UI::Switch::Conf conf_pulsador={img ,5,5,2,2,3,2,10,10};
			UI::Slider::Conf conf_slider_fondo = {img_rainbow ,1,1,0,0,0,0,10,10};
			UI::Button::Conf conf_boton_rainbow = {img_rainbow ,(int) frames,1,0,0,8,0,70,15};

			SpriteChar::setTexture(img_chars);

			panel.setTam(50,50);

			UI::setTestBox(true);
			for(int i = 0; i < 5; i++){
				boton[i].setConfig(conf_boton1);
			//	boton[i].setTam(30,30);
				boton[i].setPos(0,20*i-40);
			//	boton[i].setRotation(10*i);
			//	boton[i].setRotation(10);
				boton[i].setListener([](UI::Event a, int x, int y, UI::Element* t, void* ud){
					if(a==UI::UI_EVENT_DOWN){
						LOG_INFO("Pulsado Button: %p", t);
					}
				}, nullptr);
				panel.addElemento(&(boton[i]),i);
			}
			boton[4].setTam(50,50);
			boton[4].setConfig(conf_boton_total);
			//panel.addElemento(&(boton[4]),4);

			boton[0].setTamPixel(200, 50);


            bool *user_data = new bool;
            *user_data = true;
            boton[1].setListener([](UI::Event e, int a, int b, UI::Element *l, void * data) {
                bool * siono = (bool *) data;
                if (e == UI::UI_EVENT_DOWN) {
                    *siono = !*siono;
                }
                UI::setTestBox((*siono));
            }, user_data);
			panel.addElemento(&text, 5);
			text.setTamAuto(true);

			text.setFrase("Hola Mundo");


			panel.addElemento(&text_input, 6);
			text_input.setTam(50,50);
			text_input.setPos(0,70);
			text_input.setTamAuto(true);
			text_input.setMaxLength(50);

			slider_back.addCapa(0, img_rainbow, 1, 1);
			slider_back.setTam(100, 10);
			slider_back.setPos(0, -50);
			panel.addElemento(&slider_back, 7);
			slider.setConfig(conf_boton_rainbow);
            slider.setButtonFrames(0, 0, 0, 0);
			slider.setTam(10, 20);
			slider.setFree(true, false);
			slider.setPos(0, -50);
			panel.addElemento(&slider, 8);


//            panelFullScreen.setTamPixel(h, h);
//            panelFullScreen.setPos(0, 0);
            for (int i = 0; i < 5; ++i) {
                botonFullScreen[i].setConfig(conf_pulsador);
                botonFullScreen[i].setTamPixel(30, 30);
                botonFullScreen[i].setPosPixel(i * 50 + 200, 50);
                botonFullScreen[i].setListener([](UI::Event event, int x, int y, UI::Element*, void*) {
                    cout << "X: " << x << " Y:" << y << endl;
                }, nullptr);
                panelFullScreen.addElemento(&botonFullScreen[i], i);
            }
            botonFullScreen[0].setPosPixel(0, 0);
            botonFullScreen[1].setPosPixel(w, 0);
//            botonFullScreen[2].setPosPixel(0, h);
//            botonFullScreen[3].setPosPixel(w, h);
            botonFullScreen[4].setPosPixel(w/2, h/2);


		}
	private:
		void onStep(float dt){
			time += dt;
			//LOG_EXP(time, "%f");
		}
		void onDisplay(){
			// Clear The Screen And The Depth Buffer
			glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

			//Un triangulo rojo que gira
			glPushMatrix();
                glTranslatef(0,0,-5);
				glRotatef(time*100,0,1,0);
				glColor3f(1,0,0);

				glBegin( GL_TRIANGLES ); // Drawing Using Triangles
				glVertex3f( 0.0f, 1.0f, 0.0f ); // Top
				glVertex3f( -1.0f, -1.0f, 0.0f ); // Bottom Left
				glVertex3f( 1.0f, -1.0f, 0.0f ); // Bottom Right
				glEnd( ); // Finished Drawing The Triangle
			glPopMatrix();

			//Slider
			int frame = (int) (slider.getX() * (frames - 1));
            slider.setButtonFrames(frame, 0, frame, 0);

			//Una botonera que se mueve
			panel.setPos(100*sin(-time), 0);
			panel.setRotation(-time*10);
			boton[3].setRotation(-time*30);
			panel.setTam(50,30+20*sin(time/2.0));

			//glDisable(GL_TEXTURE_2D);
			panel.Draw();

			panel.setPos(0, 0);
			panel.setRotation(0);
			panel.setTam(50,50);

			//glEnable(GL_TEXTURE_2D);
			panel.Draw();
            panelFullScreen.Draw();

			SDL_GL_SwapWindow(window);
		}

		void onMouseMotion(SDL_Event &e){
			panel.CB_Click( UI::UI_EVENT_MOVE, e.button.x,  e.button.y);
		}

		void onMouseButton(SDL_Event &e){
			if(e.button.type == SDL_MOUSEBUTTONDOWN) {
                panel.CB_Click(UI::UI_EVENT_DOWN, e.button.x, e.button.y);
                panelFullScreen.CB_Click(UI::UI_EVENT_DOWN, e.button.x, e.button.y);
            } else {
                panel.CB_Click( UI::UI_EVENT_UP, e.button.x,  e.button.y);
                panelFullScreen.CB_Click( UI::UI_EVENT_UP, e.button.x,  e.button.y);
            }

		}

		void onKeyPressed(SDL_Event &e){
            //e.key.keysym.sym;
       /*     if(e.type == SDL_KEYDOWN)
                text_input.addChar(*SDL_GetKeyName(e.key.keysym.sym));
*/

            if(e.type == SDL_KEYDOWN){
            //printf("Physical %s key acting as %s key\n",  \
                SDL_GetScancodeName(e.key.keysym.scancode), \
                SDL_GetKeyName(e.key.keysym.sym));
                switch(e.key.keysym.sym){
                    case SDLK_BACKSPACE:
                        text_input.addChar(SpriteChar::Keycode::BACK);
                        break;

                }
            }
		}

		void onResize(int w, int h){
			UI::setConfWindow(w,h);
			initGL();
			glDisable(GL_TEXTURE_2D);
			glClearColor(0.0f, 1.0f, 0.0f, 0.0f);
            boton[0].setTamPixel(200, 50);
//            panelFullScreen.setTamPixel(h < w ? h : w, h < w ? h : w);
            panelFullScreen.setLayout(UI::Layout::LAYOUT_CENTER);
            botonFullScreen[0].setPosPixel(0, 0);
            botonFullScreen[1].setPosPixel(w, 0);
//            botonFullScreen[2].setPosPixel(0, h);
//            botonFullScreen[3].setPosPixel(w, h);
            botonFullScreen[4].setPosPixel(w/2, h/2);

            botonFullScreen[0].setTamPixel(30, 30);
            botonFullScreen[1].setTamPixel(30, 30);
            botonFullScreen[2].setTamPixel(30, 30);
            botonFullScreen[3].setTamPixel(30, 30);
            botonFullScreen[4].setTamPixel(30, 30);
		}

		void onEvent(SDL_Event& e){

            switch(e.type){
                case SDL_TEXTINPUT:
                        for(int i = 0; i < strlen(e.text.text); i++){
                            text_input.addChar(e.text.text[i]);
                        }
                    break;
                case SDL_TEXTEDITING:
                    cout << "_________________________________Text Edit : " << e.edit.text << endl;
                    break;
            }

		}


        UI::Panel<10> panelFullScreen;
		UI::Button botonFullScreen[5];

		UI::Panel<10> panel;
		UI::Button boton[5];

		UI::Text text;
		UI::Text_Input text_input;

		UI::Element slider_back;
		UI::Slider slider;

		unsigned int frames = 16;

		float time;

};




int main(){

    Gestor g(800,400);
    g.mainLoop();

	return 0;
}
