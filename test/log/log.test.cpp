/**
 * @file log.test.cpp
 * @author jmmut 
 * @date 2016-03-30.
 */

#include <string>
#include "randomize/utils/test/TestSuite.h"
#include "randomize/log/log.h"

using namespace std;
using randomize::utils::test::TestSuite;
using namespace randomize;


int parse (int argc, char **argv) {
    int fails = 0;
    fails += log::parseLogLevel("fatal") != LOG_FATAL_LEVEL;
    fails += log::parseLogLevel("fatal") != log::FATAL_LEVEL;
    fails += log::parseLogLevel("error") != LOG_ERROR_LEVEL;
    fails += log::parseLogLevel("error") != log::ERROR_LEVEL;
    fails += log::parseLogLevel("warn") != LOG_WARN_LEVEL;
    fails += log::parseLogLevel("warn") != log::WARN_LEVEL;
    fails += log::parseLogLevel("info") != LOG_INFO_LEVEL;
    fails += log::parseLogLevel("info") != log::INFO_LEVEL;
    fails += log::parseLogLevel("debug") != LOG_DEBUG_LEVEL;
    fails += log::parseLogLevel("debug") != log::DEBUG_LEVEL;
    fails += log::parseLogLevel("verbose") != LOG_VERBOSE_LEVEL;
    fails += log::parseLogLevel("verbose") != log::VERBOSE_LEVEL;

    return fails;
}

int listValues (int argc, char **argv) {

    string values;
    for (const auto &entry : log::levelNames) {
        values += entry.second;
        values += ", ";
    }
    string result = "verbose, debug, info, warn, error, fatal, ";

    int fail = strncmp(values.c_str(), result.c_str(), result.size());
    return fail;
}

int failParse (int argc, char **argv) {
    int fail = 1;
    try {
        LOG_LEVEL(log::parseLogLevel("not a log level"));
    } catch (exception e) {
        fail = 0;
    }
    return fail;
}

int main (int argc, char **argv) {
    TestSuite suite(argc, argv, {
            parse, listValues, failParse
    }, "log");
}

